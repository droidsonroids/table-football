module JsonErrors
  class ErrorsList
    def initialize
      @errors = {}
    end

    def add error_args
      error = Error.new error_args
      @errors[error.code] = error
    end

    def list
      @errors.values
    end
  end
end
