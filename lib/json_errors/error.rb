module JsonErrors
  class Error
    def initialize arguments
      @code = arguments[:code]
      @title = arguments[:title]
      @details = arguments[:details]
    end

    attr_reader :code, :title, :details
  end
end
