namespace 'apidoc' do
  desc 'generates apidoc files'
  task 'generate' do
    system "cd #{Rails.root}; apidoc -i app -o public/apidoc" 
  end
end
