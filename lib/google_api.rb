require 'singleton'
require 'google/api_client'

class GoogleApi
  class AuthError < RuntimeError; end

  User = Struct.new :id, :first_name, :last_name, :email
 
  include Singleton

  def initialize
    @client_secrets = Google::APIClient::ClientSecrets.load "#{Rails.root}/config/google_secrets.json"
  end

  def auth_uri redirect_path = nil
    auth_client = @client_secrets.to_authorization
    auth_client.update!(
      :scope => 'email profile https://www.googleapis.com/auth/userinfo.profile',
      :redirect_uri => redirect_uri(redirect_path),
      :access_type => 'offline'
    )
    auth_client.authorization_uri.to_s
  end

  def user_data auth_code, redirect_path = nil
    credentials = get_access_token auth_code, redirect_path
    result = get_user_profile credentials['access_token']
    User.new result.data.id, result.data.name.given_name, result.data.name.family_name, user_email(result)
  rescue Signet::AuthorizationError => error
    raise AuthError.new error.message
  end

  private

  def user_email result
    emails = result.data.emails.select { |hash| hash.type == 'account' }
    if emails.size > 0
      return emails.first.value
    else
      return nil
    end
  end

  def get_access_token auth_code, redirect_path
    auth_client = @client_secrets.to_authorization
    auth_client.update!(
      :scope => 'email profile https://www.googleapis.com/auth/userinfo.profile',
      :redirect_uri => redirect_uri(redirect_path), 
      :access_type => 'offline'
    )
    auth_client.code = auth_code
    auth_client.fetch_access_token!
  end

  def get_user_profile access_token
    client = Google::APIClient.new
    client.authorization.access_token = access_token
    client.execute({
      api_method: plus_api.people.get,
      parameters: { userId: 'me' }
    })
  end

  def plus_api
    unless @plus_api
      client = Google::APIClient.new
      @plus_api = client.discovered_api 'plus'
    end
    @plus_api
  end

  def redirect_uri redirect_path=nil
    redircet_path ||= 'api/v1/omniauth/google'
    if Rails.env.staging?
      "http://krzysztofzielonka.pl/#{redirect_path}"
    else
      "http://localhost:3000/#{redirect_path}"
    end
  end
end

