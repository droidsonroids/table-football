namespace :apidoc do
  desc 'generate apidoc'
  task :generate do
    on roles(:app) do
      execute "cd '#{release_path}'; apidoc -i app -o public/apidoc"
    end
  end
end
