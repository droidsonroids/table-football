namespace :thin do
  desc 'restart thin'
  task :restart do 
    on roles(:app) do
      execute "sudo /etc/init.d/thin restart"
    end
  end
end
