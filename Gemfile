source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.1'
# Use pg as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# token authentication
gem 'devise_token_auth', '~> 0.1.32.beta9'
gem 'omniauth', '~>1.2.2'
gem 'omniauth-google-oauth2', '~>0.2.6'

gem 'thin', '1.5.1'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# migrations
gem 'schema_plus', '2.0.0'
gem 'schema_plus_foreign_keys', '0.1.2'

gem 'valid_email', '0.0.11'

gem 'google-api-client'

gem 'bower-rails', '~> 0.9.2'

gem 'google-id-token', github: 'kzielonka/google-id-token', branch: 'master'

gem 'koala', '~> 2.0'

gem 'sidekiq', '3.4.0', require: nil
gem 'sinatra', '1.4.6'

gem 'apn_sender', branch: 'master'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # Testing
  gem 'rspec-rails', '~>3.2.3'

  gem 'mailcatcher', '~>0.6.1'

  gem 'foreman', '~>0.78.0'

  gem 'knife-solo', '0.4.2'
  gem 'librarian-chef'

  gem 'capistrano', '~> 3.4.0'
  gem 'capistrano-rbenv', '~> 2.0'
  gem 'capistrano-bundler', '~> 1.1.2'
  gem 'capistrano-rails', '~> 1.1'
end

group :test do
  gem 'factory_girl_rails', '~> 4.5'
  gem 'capybara', '~> 2.4.4'
  gem 'capybara-webkit', '~>1.5.2'
end
