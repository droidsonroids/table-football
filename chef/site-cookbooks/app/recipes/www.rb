directory '/var/www' do
  owner 'root'
  group 'root'
  action :create
end

directory '/var/www/table_football' do
  owner 'app'
  group 'app'
  action :create
end

directory '/var/www/table_football/shared' do
  owner 'app'
  group 'app'
  action :create
end

template '/etc/init.d/thin' do
  source 'thin.erb'
  action :create
  mode 0700
  owner 'root'
end

template '/var/www/table_football/shared/thin.yml' do
  source 'thin_config.erb'
  action :create
  mode 0600
  owner 'app'
end

bash 'give app sudo privileges' do
  code <<-EOH
    sed -i '/%app.*/d' /etc/sudoers
    echo '%app ALL=NOPASSWD: ALL' >> /etc/sudoers
  EOH
  not_if "grep -xq '%app ALL=NOPASSWD: ALL' /etc/sudoers"
end

service 'thin' do
  supports start: true, stop: true, restart: true
  action [ :enable, :start]
end
