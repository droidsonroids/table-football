package 'libffi-dev'

include_recipe 'rbenv::default'
include_recipe 'rbenv::ruby_build'

rbenv_ruby '2.2.0' do
  global true
end

rbenv_gem 'bundler' do
  ruby_version '2.2.0'
end

template '/etc/nginx/sites-available/krzysztofzielonka.pl' do
  owner 'root'
  group 'root'
  action :create
  source 'nginx.erb'
end

nginx_site 'default' do
  disable true
end

nginx_site 'krzysztofzielonka.pl' do
  enable true
end

service 'nginx' do
  action :reload
end
