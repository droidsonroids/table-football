template '/etc/init.d/sidekiq' do
  source 'sidekiq.init.d.erb'
  action :create
  mode 0700
  owner 'app'
end

service 'sidekiq' do
  supports start: true, stop: true, restart: true
  action [ :enable, :start]
end


