group node["group"]
group 'app'

user node["user"] do
  gid node["group"]
  home "/home/#{node["user"]}"
  password node["user"]["password"]
  shell "/bin/bash"
  supports manage_home: true
end

user "admin" do
  gid node["group"]
  home "/home/admin"
  password node["user"]["password"]
  shell "/bin/bash"
  supports manage_home: true
end

user "app" do
  gid 'app'
  home "/home/app"
  password node["user"]["password"]
  shell "/bin/bash"
  supports manage_home: true
end

bash "give group sudo privileges" do
  code <<-EOH
    sed -i '/%#{node['group']}.*/d' /etc/sudoers
    echo '%#{node['group']} ALL=(ALL) ALL' >> /etc/sudoers
  EOH
  not_if "grep -xq '%#{node['group']} ALL=(ALL) ALL' /etc/sudoers"
end

directory '/home/app/.ssh' do
  owner 'app'
  group 'app'
end

bash 'add ssh key app user' do
  name = node['ssh_key'].split(' ').last 
  code <<-EOH
    sed -i '/.*#{name}/d' /home/app/.ssh/authorized_keys
    echo '#{node['ssh_key']}' >> /home/app/.ssh/authorized_keys
  EOH
  not_if "grep -xq '#{node['ssh_key']}' /home/app/.ssh/authorized_keys"
end
