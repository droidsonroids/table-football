include_recipe 'monit'

#template '/etc/monit/monitrc.d/thin' do
#  source 'thin_monit.conf.erb'
#  action :create
#  mode 0700
#  owner 'root'
#end

monitrc 'thin_monit' do
  template_cookbook 'app'
end

monitrc 'redis_monit' do
  template_cookbook 'app'
end

monitrc 'sidekiq_monit' do
  template_cookbook 'app'
end
