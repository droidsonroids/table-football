default['app'] = 'football table'

default['rbenv']['group_users'] = ['app', 'admin']
default['rbenv']['install_prefix'] = '/opt/rbenv'
