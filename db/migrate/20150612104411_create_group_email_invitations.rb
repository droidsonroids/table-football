class CreateGroupEmailInvitations < ActiveRecord::Migration
  def change
    create_table :group_email_invitations do |t|
      t.string :email, null: false
      t.integer :group_id, null: false, foreign_key: { references: :groups, on_delete: :cascade }
      t.datetime :last_sent_at
      t.string :token
      t.datetime :created_at, null: false 
    end

    add_index :group_email_invitations, [:email, :group_id], unique: true
    add_index :group_email_invitations, :token, unique: true
  end
end
