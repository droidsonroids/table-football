class AddPointsToMatches < ActiveRecord::Migration
  def change
    execute "CREATE TYPE player_acceptance AS ENUM ('reject', 'none', 'accept')"
    add_column :matches, :points_1, :integer
    add_column :matches, :points_2, :integer
    add_column :matches, :player_1_points_acceptance, :player_acceptance, null: false
    add_column :matches, :player_2_points_acceptance, :player_acceptance, null: false
    add_column :matches, :player_3_points_acceptance, :player_acceptance, null: false
    add_column :matches, :player_4_points_acceptance, :player_acceptance, null: false
  end
end
