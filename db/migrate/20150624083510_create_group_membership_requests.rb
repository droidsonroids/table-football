class CreateGroupMembershipRequests < ActiveRecord::Migration
  def change
    create_table :group_membership_requests do |t|
      t.integer :group_id, null: false, foreign_key: { references: :groups, on_delete: :cascade }
      t.integer :user_id, null: false, foreign_key: { references: :users, on_delete: :cascade }
      t.datetime :created_at, null: false
    end

    add_index :group_membership_requests, [:group_id, :user_id], unique: true
  end
end
