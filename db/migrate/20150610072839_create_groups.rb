class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name, null: false
      t.integer :admin_id, null: false, foreign_key: { references: :users, on_delete: :cascade }
      t.integer :number_of_tables, null: false, default: 0
      t.timestamps null: false
    end

    add_index :groups, :name, unique: true
  end
end
