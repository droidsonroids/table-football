class CreateMatches < ActiveRecord::Migration
  def change
    execute "CREATE TYPE match_type AS ENUM ('1v1', '2v2');"

    create_table :matches do |t|
      t.string :name, null: false
      t.integer :group_id, null: false, foreign_key: { references: :groups, on_delete: :cascade }
      t.integer :admin_id, null: false, foreign_key: { references: :users, on_delete: :nullify }
      t.integer :player_1_id, null: true, foreign_key: { references: :users, on_delete: :nullify }
      t.integer :player_2_id, null: true, foreign_key: { references: :users, on_delete: :nullify }
      t.integer :player_3_id, null: true, foreign_key: { references: :users, on_delete: :nullify }
      t.integer :player_4_id, null: true, foreign_key: { references: :users, on_delete: :nullify }
      t.boolean :private, null: false
      t.integer :duration, null: false
      t.datetime :start_time, null: false
      t.integer :table_number, null: false
      t.timestamps null: false
    end

    execute "ALTER TABLE matches ADD COLUMN match_type match_type NOT NULL;"
  end
end
