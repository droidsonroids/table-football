class CreateGroupsUsers < ActiveRecord::Migration
  def change
    create_table :groups_users do |t|
      t.integer :group_id, foreign_key: { references: :groups, on_delete: :cascade } 
      t.integer :user_id, foreign_key: { references: :users, on_delete: :cascade }
    end

    add_index :groups_users, [:group_id, :user_id], unique: true
  end
end
