class CreateAccessTokens < ActiveRecord::Migration
  def change
    create_table :access_tokens do |t|
      t.integer :user_id, null: false, foreign_key: { references: :users, on_delete: :cascade }
      t.string :client_id, null: false, foreign_key: nil
      t.string :encrypted_access_token, null: false
      t.string :encrypted_refresh_token, null: false
      t.string :apple_bundle_id, foreign_key: nil
      t.string :device_token
      t.datetime :expires_at, null: false
    end

    add_index :access_tokens, :client_id, unique: true
  end
end
