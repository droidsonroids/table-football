class AddPlayerStateToMatches < ActiveRecord::Migration
  def change
    execute "CREATE TYPE match_player_state AS ENUM ('none', 'invited', 'joined')"
    add_column :matches, :player_1_state, :match_player_state, null: false
    add_column :matches, :player_2_state, :match_player_state, null: false
    add_column :matches, :player_3_state, :match_player_state, null: false
    add_column :matches, :player_4_state, :match_player_state, null: false
  end
end
