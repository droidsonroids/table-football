class CreateGroupUserInvitations < ActiveRecord::Migration
  def change
    create_table :group_user_invitations do |t|
      t.integer :user_id, null: false, foreign_key: { references: :users, on_delete: :cascade }
      t.integer :group_id, null: false, foreign_key: { references: :groups, on_delete: :cascade }
      t.datetime :created_at, null: false
    end
    
    add_index :group_user_invitations, [:group_id, :user_id], unique: true
  end
end
