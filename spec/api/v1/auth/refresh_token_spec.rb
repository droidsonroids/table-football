require 'rails_helper'

describe 'POST api/v1/auth/refresh_token', type: :request do
  before :each do
    @headers = {'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json'}
    @headers.merge! create_user(email: 'test@example.com', password: 'PAssWOrd1234', name: 'signed in user')
    @body = {
      refresh_token: @headers.delete('refresh-token')
    }
  end

  it 'should return 400 when json is invalid' do
    post '/api/v1/auth/refresh_token', 'fsadfdsafdas', @headers
    expect( response.status ).to eq 400
  end

  it 'should return 401 with invalid client' do
    @headers['client'] = 'asdfasfas'
    post '/api/v1/auth/refresh_token', @body.to_json, @headers
    expect( response.status ).to eq 401
  end

  it 'should return 401 with invalid uid' do
    @headers['uid'] = 'asdfasfas'
    post '/api/v1/auth/refresh_token', @body.to_json, @headers
    expect( response.status ).to eq 401
  end

  it 'should return 401 if refresh token is not same' do
    @body[:refresh_token] = 'asdfsadf'
    post '/api/v1/auth/refresh_token', @body.to_json, @headers
    expect( response.status ).to eq 401
  end
 
  it 'should return 400 if refresh token is missing' do
    @body = {}
    post '/api/v1/auth/refresh_token', @body.to_json, @headers
    expect( response.status ).to eq 400
  end

  it 'should return 200 and empty body' do
    post '/api/v1/auth/refresh_token', @body.to_json, @headers
    p response.body
    expect( response.status ).to eq 200
  end

  it 'should return new access token' do
    post '/api/v1/auth/refresh_token', @body.to_json, @headers
    expect( response.status ).to eq 200
    expect( response.headers['access-token'] ).to_not be_blank
    expect( response.headers['access-token'] ).to_not eq @headers['access-token']
    data = JSON.parse response.body
    expect( data['auth']['access_token'] ).to_not eq @headers['access-token']
  end

  it 'should return new refresh token' do
    post '/api/v1/auth/refresh_token', @body.to_json, @headers
    expect( response.status ).to eq 200
    expect( response.headers['refresh-token'] ).to_not be_blank
    expect( response.headers['refresh-token'] ).to_not eq @body['refresh-token']
  end

  it 'should return new uid' do
    post '/api/v1/auth/refresh_token', @body.to_json, @headers
    expect( response.status ).to eq 200
    expect( response.headers['uid'] ).to_not be_blank
    expect( response.headers['uid'] ).to eq @headers['uid']
  end

  it 'should return new client' do
    post '/api/v1/auth/refresh_token', @body.to_json, @headers
    p response.body
    expect( response.status ).to eq 200
    expect( response.headers['client'] ).to_not be_blank
    expect( response.headers['client'] ).to eq @headers['client']
  end

  it 'should be possible to create group using new token' do
    group_body = { name: 'name 1', number_of_tables: 5 }
    post '/api/v1/groups', group_body.to_json, @headers
    expect( response.status ).to eq 200

    post '/api/v1/auth/refresh_token', @body.to_json, @headers

    @new_headers = {
      'access-token' => response.headers['access-token']
    }

    group_body = { name: 'name 2', number_of_tables: 5 }
    post '/api/v1/groups', group_body.to_json, @headers
    expect( response.status ).to eq 401

    @headers.merge! @new_headers

    group_body = { name: 'name 3', number_of_tables: 5 }
    post '/api/v1/groups', group_body.to_json, @headers
    expect( response.status ).to eq 200
  end
end 
