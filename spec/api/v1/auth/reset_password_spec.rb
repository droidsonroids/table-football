require 'rails_helper'

describe 'POST api/v1/auth/reset_password', type: :request do
  before :each do
    @headers = {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
    create_user(email: 'test@example.com', password: 'PAssWOrd1234', name: 'signed in user')
  end

  it 'should return 400 when email is missing' do
    @body = {}
    post '/api/v1/auth/reset_password', @body.to_json, @headers
    expect( response.status ).to eq 400
  end

  it 'should return 404 when user does not exists' do
    @body = { email: 'xxxxx@wp.pl' }
    post '/api/v1/auth/reset_password', @body.to_json, @headers
    expect( response.status ).to eq 404
  end

  it 'should return 204 and empty body' do
    @body = { email: 'test@example.com' }
    post '/api/v1/auth/reset_password', @body.to_json, @headers
    expect( response.status ).to eq 204
    expect( response.body ).to be_empty
  end

  it 'should send one email' do
    @body = { email: 'test@example.com' }
    expect{
      post '/api/v1/auth/reset_password', @body.to_json, @headers
    }.to change {
      ActionMailer::Base.deliveries.size
    }.by 1
  end
end
