require 'rails_helper'

describe 'sign in', type: :request do
  before :each do
    @headers = {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
    @body = {
      email: 'test@example.com', 
      password: 'PAssWOrd1234' 
    }
    FacebookAdapters::TestAdapter.raise_auth_error false
  end

  it 'should return 401' do
    FacebookAdapters::TestAdapter.raise_auth_error
    post '/api/v1/auth/facebook', { access_token: 'asfdadsfdsa' }.to_json, @headers
    expect( response.status ).to eq 401
    expect( response.body ).to match /test auth error/
  end 

  it 'should return 200' do
    post '/api/v1/auth/facebook', { access_token: 'asfdadsfdsa' }.to_json, @headers
    expect( response.status ).to eq 200
    data = JSON.parse response.body
    expect( data['email'] ).to eq 'xxxx@example.com'
    expect( data['first_name'] ).to eq 'Imie'
    expect( data['last_name'] ).to eq 'Nazwisko'
  end

  it 'should create new user' do
    @some_user = create_user email: 'admin@example.com', password: 'PAssWOrd1234'
    
    # check number of group members
    get '/api/v1/users', {}.to_json, @headers.merge(@some_user) 
    expect( JSON.parse(response.body).size ).to eq 1

    # sign in 
    post '/api/v1/auth/facebook', { access_token: 'sfasfads'}.to_json, @headers

    # check number of group members
    get '/api/v1/users', {}.to_json, @headers.merge(@some_user)
    expect( JSON.parse(response.body).size ).to eq 2
  end

  it 'should sign in after sign up' do
    post '/api/v1/auth/facebook', { access_token: 'asfdadsfdsa' }.to_json, @headers
    expect( response.status ).to eq 200
    post '/api/v1/auth/facebook', { access_token: 'asfdadsfdsa' }.to_json, @headers
    expect( response.status ).to eq 200
  end
end
 
