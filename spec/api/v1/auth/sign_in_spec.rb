require 'rails_helper'

describe 'sign in', type: :request do
  before :each do
    @headers = {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
    @body = {
      email: 'test@example.com', 
      password: 'PAssWOrd1234' 
    }
    post_auth @body
    accept_last_confirmation
  end
  
  it 'should return 401' do
    @body[:email] = 'abcd@example.com'
    post '/api/v1/auth/sign_in', @body.to_json, @headers
    expect( response.status ).to eq 401
  end

  it 'should return 401' do
    @body[:password] = 'asfasdf'
    post '/api/v1/auth/sign_in', @body.to_json, @headers
    expect( response.status ).to eq 401
  end

  it 'should return 200' do
    post '/api/v1/auth/sign_in', @body.to_json, @headers
    expect( response.status ).to eq 200
  end

  it 'should return auth headers' do
    post '/api/v1/auth/sign_in', @body.to_json, @headers
    expect( response.headers['access-token'] ).to_not be_blank
    expect( response.headers['refresh-token'] ).to_not be_blank
    expect( response.headers['expiry'] ).to_not be_blank
    expect( response.headers['uid'] ).to_not be_blank
    expect( response.headers['client'] ).to_not be_blank
  end

  it 'should be possible to create group using access token' do
    group_body = { name: 'name 1', number_of_tables: 5 }
    post '/api/v1/groups', group_body.to_json, @headers
    expect( response.status ).to eq 401

    post '/api/v1/auth/sign_in', @body.to_json, @headers
    @headers.merge!({
      'access-token' => response.headers['access-token'],
      'uid' => response.headers['uid'],
      'client' => response.headers['client'],
      'token-type' => 'Bearer'
    })
    post '/api/v1/groups', group_body.to_json, @headers
    expect( response.status ).to eq 200
  end

  it 'should return 401 when email and password are correct but user is not confirmed' do
    @body = {
      email: 'other@example.com', 
      password: 'PAssWOrd1234' 
    }
    post_auth @body
    post '/api/v1/auth/sign_in', @body.to_json, @headers
    expect( response.status ).to eq 401
    expect( response.body ).to match /User is not confirmed/
  end

  it 'should return user data' do
    post '/api/v1/auth/sign_in', @body.to_json, @headers
    data = JSON.parse response.body
    expect( data.keys ).to contain_exactly('id', 'email', 'first_name', 'last_name', 'auth', 'groups', 'organizations')
  end

  it 'should reutrn 400' do
    @body.delete :email
    post '/api/v1/auth/sign_in', @body.to_json, @headers
    expect( response.status ).to eq 400
  end

  describe 'ios' do
    it 'should return 400' do
      @body[:apple_bundle_id] = 2134312412
      @body[:device_token] = 'asfsadf'
      post '/api/v1/auth/sign_in', @body.to_json, @headers
      expect( response.status ).to eq 400
    end

    it 'should return 400' do
      @body[:apple_bundle_id] = 'asfasfasd'
      @body[:device_token] = 232342
      post '/api/v1/auth/sign_in', @body.to_json, @headers
      expect( response.status ).to eq 400
    end

    it 'should return 400' do
      @body[:apple_bundle_id] = 'x'*1001
      @body[:device_token] = 'asfasfd'
      post '/api/v1/auth/sign_in', @body.to_json, @headers
      expect( response.status ).to eq 400
    end

    it 'should return 400' do
      @body[:apple_bundle_id] = 'x'
      @body[:device_token] = 'x'*1001
      post '/api/v1/auth/sign_in', @body.to_json, @headers
      expect( response.status ).to eq 400
    end

    it 'should return 400' do
      @body[:apple_bundle_id] = 'asfasfasd'
      @body[:device_token] = nil
      post '/api/v1/auth/sign_in', @body.to_json, @headers
      expect( response.status ).to eq 400
    end

    it 'should return 400' do
      @body[:apple_bundle_id] = nil
      @body[:device_token] = 'dsgsdgfd'
      post '/api/v1/auth/sign_in', @body.to_json, @headers
      expect( response.status ).to eq 400
    end

    it 'should return 200' do
      @body[:apple_bundle_id] = 'asfsafas'
      @body[:device_token] = 'dsgsdgfd'
      post '/api/v1/auth/sign_in', @body.to_json, @headers
      expect( response.status ).to eq 200
    end

    it 'should return 200' do
      @body[:apple_bundle_id] = nil
      @body[:device_token] = nil
      post '/api/v1/auth/sign_in', @body.to_json, @headers
      expect( response.status ).to eq 200
    end
  end
end
