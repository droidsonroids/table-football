require 'rails_helper'

describe 'GET api/v1/auth/register', type: :request do
  it 'should return 204 with blank body' do
    body = {
      email: 'test@example.com',
      password: 'PASSword1234',
      first_name: 'user name',
      last_name: 'user last name'
    }.to_json

    post '/api/v1/auth/register', body, {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
    expect( response.status ).to eq 204
    expect( response.body ).to be_blank
  end

  it 'should return 400' do
    body = {
      email: 'test@example.com',
      password: 'PASSword1234',
      first_name: 'user first name',
      last_name: 'user last name'
    }.to_json
    post '/api/v1/auth/register', body, {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
    post '/api/v1/auth/register', body, {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
    expect( response.status ).to eq 204
    expect( response.body ).to be_blank
  end

  it 'should send confirmation email' do
    body = {
      email: 'test@example.com',
      password: 'PASSword1234',
      first_name: 'user first name',
      last_name: 'last_name'
    }.to_json
    expect{ 
      post '/api/v1/auth/register', body, {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
    }.to change{ ActionMailer::Base.deliveries.size }.by 1
  end

  describe 'validation' do
    it 'should return 400 with missing name' do
      body = {
        email: 'test@example.com',
        password: 'PASSword1234',
        first_name: nil,
        last_name: 'sadfs'
      }.to_json
      post '/api/v1/auth/register', body, {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
      expect( response.status ).to eq 400
      expect( response.body ).to match /First name can't be blank./
    end

    it 'should return 400 with missing email' do
      body = {
        password: 'PASSword1234',
        first_name: 'name',
        last_name: 'sadfasdf'
      }.to_json
      post '/api/v1/auth/register', body, {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
      expect( response.status ).to eq 400
      expect( response.body ).to match /Email can't be blank./
    end

    it 'should return 400 with not unique email' do
      create_user email: 'xxx@wp.pl'
      body = {
        email: 'xxx@wp.pl',
        password: 'PASSword1234',
        first_name: 'name',
        last_name: 'sadfasdf'
      }.to_json
      post '/api/v1/auth/register', body, {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
      expect( response.status ).to eq 400
      expect( response.body ).to match /this email address is already in use/
    end
  end
end
