require 'rails_helper'

describe 'GET /api/v1/users/unique_email', type: :request do
  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  it 'should return 400' do
    get '/api/v1/users/unique_email', {}, @headers
    expect( response.status ).to eq 400
    expect( response.body ).to match /Email is required/
  end 

  it 'should return 400' do
    get '/api/v1/users/unique_email', { email: 'asfasfdas' }, @headers
    expect( response.status ).to eq 400
    expect( response.body ).to match /Email is not valid/
  end 

  it 'should return 200 with message that email is unique' do
    get '/api/v1/users/unique_email', { email: 'test@example.com' }, @headers
    expect( response.status ).to eq 200
    expect( response.body ).to eq({ unique: true }.to_json)
  end

  it 'should return 200 with message that email is not unique' do
    create_user email: 'test@example.com'
    get '/api/v1/users/unique_email', { email: 'test@example.com' }, @headers
    expect( response.status ).to eq 200
    expect( response.body ).to eq({ unique: false }.to_json)
  end
end
