require 'rails_helper'

describe 'PUT api/v1/users/me', type: :request do
  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      get '/api/v1/users/me', {}, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @current_user = create_user email: 'test@example.com', password: 'PAssWOrd1234', first_name: 'Marek', last_name: 'Pazdzioch'
      @headers.merge! @current_user
      @body = {
        first_name: 'Arnold',
        last_name: 'Boczek'
      }
    end

    it 'should return corret user' do
      put '/api/v1/users/me', @body.to_json, @headers
      expect( response.status ).to eq 200
      expect( JSON.parse( response.body )['id'] ).to eq @current_user['id']
    end

    it 'should update user first name' do
      put '/api/v1/users/me', @body.to_json, @headers
      expect( response.status ).to eq 200
      expect( JSON.parse( response.body )['first_name'] ).to eq @body[:first_name]
 
      get '/api/v1/users/me', @body.to_json, @headers
      expect( response.status ).to eq 200
      expect( JSON.parse( response.body )['first_name'] ).to eq @body[:first_name]
    end 

    it 'should update last name' do
      put '/api/v1/users/me', @body.to_json, @headers
      expect( response.status ).to eq 200
      expect( JSON.parse( response.body )['first_name'] ).to eq @body[:first_name]
 
      get '/api/v1/users/me', @body.to_json, @headers
      expect( response.status ).to eq 200
      expect( JSON.parse( response.body )['first_name'] ).to eq @body[:first_name]
    end

    [
      [:first_name, '', "First name can't be blank"],
      [:first_name, 'x', 'First name is too short'],
      [:first_name, 'x'*1001, 'First name is too long'],
      [:last_name, '', "Last name can't be blank"],
      [:last_name, 'x', 'Last name is too short'],
      [:last_name, 'x'*1001, 'Last name is too long']
    ].each do |key, value, message|
      it "should return 400 when '#{key}' is invalid with message '#{message}'" do
        @body[key] = value
        put '/api/v1/users/me', @body.to_json, @headers
        expect( response.status ).to eq 400
        expect( JSON.parse( response.body )['errors'] ).to match /#{message}/
      end
    end
  end
end
