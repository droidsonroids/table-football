require 'rails_helper'

describe 'GET api/v1/users', type: :request do
  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      get '/api/v1/users', {}, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @auth_headers = create_user email: 'test@example.com', password: 'PAssWOrd1234', first_name: 'signed in user', last_name: 'vvvvvvvvvvvvvvv'
      @headers.merge! @auth_headers
    end
 
    it 'should return 200' do
      get '/api/v1/users', {}, @headers
      expect( response.status ).to eq 200
    end

    it 'should return two users' do
      create_user email: 'u1@example.com'
      create_user email: 'u2@example.com'
      get '/api/v1/users', {}, @headers
      expect( JSON.parse(response.body).size ).to eq 3
    end

    it 'should return only user id and name' do
      create_user email: 'u1@example.com'
      get '/api/v1/users', {}, @headers
      expect( JSON.parse(response.body).first.keys ).to eq ['id', 'first_name', 'last_name']
    end

    it 'should show only users matching query' do
      create_user email: 'u1@example.com', first_name: 'Marek', last_name: 'Banbor'
      create_user email: 'u2@example.com', first_name: 'Jacek', last_name: 'Kotek'

      create_user email: 'u3@example.com', first_name: 'Maciek', last_name: 'Banan'
      create_user email: 'u4@example.com', first_name: 'Jacek', last_name: 'Piesek'
      
      get '/api/v1/users', { query: 'ban' }, @headers
      expect( response.status ).to eq 200
      data = JSON.parse response.body
      data.map!{ |user| "#{user['first_name']} #{user['last_name']}" }
      expect( data ).to contain_exactly('Marek Banbor', 'Maciek Banan')
    end

    it 'should return 200 and match one user (replacment % with \%)' do
      create_user email: 'u1@example.com', first_name: 'xxx', last_name: 'ccccccc'
      create_user email: 'u2@example.com', first_name: 'x%x', last_name: 'ccccccc'

      get '/api/v1/users', { query: 'x%x' }, @headers
      expect( response.status ).to eq 200
      data = JSON.parse response.body
      expect( data.size ).to eq 1
      expect( data.first['first_name'] ).to eq 'x%x'
    end

    it 'should return 200 and match one user (replacment \s with %)' do
      create_user email: 'u1@example.com', first_name: 'Marek', last_name: 'Ksiask'
      create_user email: 'u2@example.com', first_name: 'Marek Marcel', last_name: 'Banan'

      get '/api/v1/users', { query: 'Marek Banan' }, @headers
      expect( response.status ).to eq 200
      data = JSON.parse response.body
      expect( data.size ).to eq 1
      expect( "#{data.first['first_name']} #{data.first['last_name']}" ).to eq 'Marek Marcel Banan'
    end

    it 'should return 200 and match one user (replacment _ with \_)' do
      create_user email: 'u1@example.com', first_name: 'xyx', last_name: 'ccccccc'
      create_user email: 'u2@example.com', first_name: 'x_x', last_name: 'ccccccc'

      get '/api/v1/users', { query: 'x_x' }, @headers
      expect( response.status ).to eq 200
      data = JSON.parse response.body
      expect( data.size ).to eq 1
      expect( data.first['first_name'] ).to eq 'x_x'
    end

    it 'should return 200 and match one user' do
      create_user email: 'u1@example.com', first_name: %q(xyx), last_name: 'gggggg'
      create_user email: 'u2@example.com', first_name: %q(x\_x), last_name: 'gggggg'
      get '/api/v1/users', { query: %q(x\_x) }, @headers
      expect( response.status ).to eq 200
      data = JSON.parse response.body
      expect( data.size ).to eq 1
      expect( data.first['first_name'] ).to eq %q(x\_x)
    end

    it 'should return 200 and match one user' do
      create_user email: 'u1@example.com', first_name: %q(x\\\\_x), last_name: 'cccccc'
      create_user email: 'u2@example.com', first_name: %q(x\_x), last_name: 'cccccc'
      get '/api/v1/users', { query: %q(x\\_x) }, @headers
      expect( response.status ).to eq 200
      data = JSON.parse response.body
      expect( data.size ).to eq 1
      expect( data.first['first_name'] ).to eq %q(x\_x)
    end

    describe 'without_me param' do
      it 'should return 200 and all users' do
        create_user email: 'u1@example.com', first_name: 'other user', last_name: 'ffffffffff'
        get '/api/v1/users', { without_me: 'no' }, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data.size ).to eq 2
      end

      it 'should return 200 and not include signed in user in results' do
        create_user email: 'u1@example.com', first_name: 'other user'
        get '/api/v1/users', { without_me: 'yes' }, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data.size ).to eq 1
        expect( data.first['first_name'] ).to eq 'other user'
      end
    end

    describe 'group_id param' do
      before :each do
        @admin_1 = create_user email: 'admin1@example.com', first_name: 'admin 1'
        @admin_2 = create_user email: 'admin2@example.com', first_name: 'admin 2'
        @group_1 = create_group({ name: 'group 1' }, @admin_1)
        @group_2 = create_group({ name: 'group 2' }, @admin_2)

        @users_group_1 = (1..4).map { |i| create_user email: "user_group1_#{i}@example.com", first_name: "user_group1_#{i}" }
        @users_group_1.each{ |user| add_user_to_group @group_1, user, @admin_1 }

        @users_group_2 = (1..2).map { |i| create_user email: "user_group2_#{i}@example.com", first_name: "user_group2_#{i}" }
        @users_group_2.each{ |user| add_user_to_group @group_2, user, @admin_2 }

        @users_common = (1..2).map { |i| create_user email: "user_common_#{i}@example.com", first_name: "user_common_#{i}" }
        @users_common.each{ |user| add_user_to_group @group_1, user, @admin_1 }
        @users_common.each{ |user| add_user_to_group @group_2, user, @admin_2 }
      end
  
      it 'should return 200 and all group 1 users' do
        get '/api/v1/users', { group_id: @group_1['id'] }, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expected_ids = (@users_group_1 + @users_common + [@admin_1]).map { |user| user['id'] }
        expect( data.map{ |user| user['id'] } ).to contain_exactly *expected_ids
      end 

      it 'should return 200 and all group 2 users' do
        get '/api/v1/users', { group_id: @group_2['id'] }, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expected_ids = (@users_group_2 + @users_common + [@admin_2]).map { |user| user['id'] }
        expect( data.map{ |user| user['id'] } ).to contain_exactly *expected_ids
      end 
    end

    describe 'query and group_id' do
      before :each do
        @admin_1 = create_user email: 'admin1@example.com', first_name: 'admin 1'
        @admin_2 = create_user email: 'admin2@example.com', first_name: 'admin 2'
        @group_1 = create_group({ name: 'group 1' }, @admin_1)
        @group_2 = create_group({ name: 'group 2' }, @admin_2)

        add_user_to_group @group_1, @headers, @admin_1

        @user_1 = create_user email: 'user_group_1_1@example.com', first_name: 'Marcin', last_name: 'Xyz1'
        add_user_to_group @group_1, @user_1, @admin_1
        @user_2 = create_user email: 'user_group_1_2@example.com', first_name: 'Magda', last_name: 'Xyz2'
        add_user_to_group @group_1, @user_2, @admin_1
        @user_3 = create_user email: 'user_group_1_3@example.com', first_name: 'Bogdan', last_name: 'Abc3'
        add_user_to_group @group_1, @user_3, @admin_1
        @user_4 = create_user email: 'user_group_1_4@example.com', first_name: 'Janusz', last_name: 'Abc4'
        add_user_to_group @group_1, @user_4, @admin_1

        @users_group_2 = (1..2).map { |i| create_user email: "user_group2_#{i}@example.com", first_name: "user_group2_#{i}" }
        @users_group_2.each{ |user| add_user_to_group @group_2, user, @admin_2 }
      end

      it 'should return first two users' do
        get '/api/v1/users', { group_id: @group_1['id'], query: 'Xyz' }, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data.size ).to eq 2
        expect( data.map{ |user| user['id'] } ).to contain_exactly @user_1['id'], @user_2['id']
      end

      it 'should return last two users' do
        get '/api/v1/users', { group_id: @group_1['id'], query: 'Abc' }, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data.size ).to eq 2
        expect( data.map{ |user| user['id'] } ).to contain_exactly @user_3['id'], @user_4['id']
      end
    end

    describe 'pagination' do
      before :each do
        (0..9).each do |i|
          create_user email: "u#{i}@example.com", first_name: "user #{i}"
        end
      end

      it 'should return first 3 results' do
        get '/api/v1/users', { page: 1, per_page: 3 }, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data.size ).to eq 3
        expect( data.map{ |user| user['first_name'] } ).to eq [
          'signed in user',
          'user 0',
          'user 1'
        ]
      end

      it 'should return second page and 4 results' do
        get '/api/v1/users', { page: 2, per_page: 4 }, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data.size ).to eq 4
        expect( data.map{ |user| user['first_name'] } ).to eq [
          'user 3',
          'user 4',
          'user 5',
          'user 6'
        ]
      end

      it 'should return second page and 4 results' do
        get '/api/v1/users', { page: 4, per_page: 3 }, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data.size ).to eq 2
        expect( data.map{ |user| user['first_name'] } ).to eq [
          'user 8',
          'user 9'
        ]
      end

      it 'should return second page and 0 results' do
        get '/api/v1/users', { page: 10, per_page: 5 }, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data.size ).to eq 0
      end
    end

    describe 'validation' do
      [
        [:query, 'x'*1001, 'Query is too long'],
        [:without_me, 'safsasd', 'Without me is not included'], 
        [:group_id, 'asfads', 'Group is not a number'],
        [:group_id, '2.4', 'Group must be an integer'],
        [:group_id, '-1', 'Group must be greater than or equal to 1'],
        [:group_id, '0',  'Group must be greater than or equal to 1'],
        [:page, 'asfads', 'Page is not a number'],
        [:page, '2.4', 'Page must be an integer'],
        [:page, '-1', 'Page must be greater than or equal to 1'],
        [:page, '0',  'Page must be greater than or equal to 1'],
        [:per_page, 'asfads', 'Per page is not a number'],
        [:per_page, '2.4', 'Per page must be an integer'],
        [:per_page, '-1', 'Per page must be greater than or equal to 1'],
        [:per_page, '0',  'Per page must be greater than or equal to 1']
      ].each do |param, value, error_message|
        it "should return 400 if param '#{param}' is set to '#{value}'" do
          get '/api/v1/users', { param => value }, @headers
          expect( response.status ).to eq 400
          expect( response.body ).to match /#{error_message}/
        end
      end
    end
  end
end
