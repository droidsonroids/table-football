require 'rails_helper'

describe 'GET api/v1/users/me', type: :request do
  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      get '/api/v1/users/me', {}, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @current_user = create_user email: 'test@example.com', password: 'PAssWOrd1234'
      @headers.merge! @current_user
    end

    it 'should return corret user' do
      create_user email: 'u1@example.com'
      user = create_user email: 'u2@example.com'
      create_user email: 'u3@example.com'
      get '/api/v1/users/me', {}, @headers
      expect( response.status ).to eq 200
      expect( JSON.parse( response.body )['id'] ).to eq @current_user['id']
    end

    it 'should return users groups' do
      user_1 = create_user email: 'u1@example.com', password: 'PAssword1'
      user_2 = create_user email: 'u2@example.com', password: 'PAswrod2'
      group_1 = create_group({name: 'group1', number_of_tables: 1}, user_1)
      group_2 = create_group({name: 'group2', number_of_tables: 2}, @current_user)
      group_3 = create_group({name: 'group3', number_of_tables: 3}, @current_user)
      get '/api/v1/users/me', {}, @headers
      data = JSON.parse response.body 
      expect( data ).to have_key 'groups'
      expect( data['groups'].size ).to eq 2
      expect( data['groups'].map{ |group| group['id'] } ).to eq [group_2['id'], group_3['id']]
    end
  end
end
