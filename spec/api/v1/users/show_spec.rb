require 'rails_helper'

describe 'GET api/v1/users/:id', type: :request do
  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      get '/api/v1/users/1234', {}, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @auth_headers = create_user email: 'test@example.com', password: 'PAssWOrd1234'
      @headers.merge! @auth_headers
    end

    it 'should return 404' do
      get '/api/v1/users/1234', {}, @headers
      expect( response.status ).to eq 404
    end

    it 'should return 403' do
      user = post_auth email: 'other@example.com'
      last_user_id = User.last.id
      get "/api/v1/users/#{last_user_id}", {}, @headers
      expect( response.status ).to eq 403
    end

    it 'should return corret user' do
      create_user email: 'u1@example.com'
      user = create_user email: 'u2@example.com'
      create_user email: 'u3@example.com'
      get "/api/v1/users/#{user['id']}", {}, @headers
      expect( JSON.parse( response.body )['id'] ).to eq user['id']
    end

    it 'should return users groups' do
      user_1 = create_user email: 'u1@example.com', password: 'PAssword1'
      user_2 = create_user email: 'u2@example.com', password: 'PAswrod2'
      group_1 = create_group({name: 'group1', number_of_tables: 1}, user_1)
      group_2 = create_group({name: 'group2', number_of_tables: 2}, user_2)
      group_3 = create_group({name: 'group3', number_of_tables: 3}, user_2)
      get "/api/v1/users/#{user_2['id']}", {}, @headers
      data = JSON.parse response.body 
      expect( data ).to have_key 'groups'
      expect( data['groups'].size ).to eq 2
      expect( data['groups'].map{ |group| group['id'] } ).to eq [group_2['id'], group_3['id']]
    end
  end
end
