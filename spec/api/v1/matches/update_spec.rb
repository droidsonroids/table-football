require 'rails_helper'

describe 'POST /api/v1/matches/:id', type: :request do
  include ActiveSupport::Testing::TimeHelpers

  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
    @other_user = create_user email: 'other_user@example.com'
    @group = create_group({name: 'group'}, @headers.merge(@other_user))
    
    @player_1 = create_user email: 'player1@example.com'
    @player_2 = create_user email: 'player2@example.com'
    add_user_to_group @group, @player_1, @other_user
    add_user_to_group @group, @player_2, @other_user

    @body = {
      name: 'Match name',
      type: '1v1',
      player_1: @other_user['id'],
      player_2: @player_2['id'],
      start_time: DateTime.now.rfc3339,
      duration: 15,
      table_number: 1,
      private: true
    }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      put '/api/v1/matches/1234', @body.to_json, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @current_user = create_user email: 'test@example.com', first_name: 'Marek', last_name: 'Banan'
      @headers.merge! @current_user
      add_user_to_group @group, @current_user, @other_user
    end

    it 'should return 404' do
      put "/api/v1/matches/1234", @body.to_json, @headers
      expect( response.status ).to eq 404
    end

    it 'should return 403' do
      post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers.merge(@other_user)
      expect( response.status ).to eq 200
      match = JSON.parse response.body
      put "/api/v1/matches/#{match['id']}", @body.to_json, @headers
      expect( response.status ).to eq 403
    end
 
    it 'should not be possible to update match after its start_time' do
      match = nil

      @body[:player_1] = @current_user['id']
      travel_to DateTime.parse('2000-01-01T10:00:00.00+00:00') do
        @body[:start_time] = '2000-01-01T12:00:00.00+00:00'
        post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
        expect( response.status ).to eq 200
        match = JSON.parse response.body
      end

      travel_to DateTime.parse('2000-01-01T12:15:00.00+00:00') do
        @body[:start_time] = '2000-01-01T13:00:00.00+00:00'
        put "/api/v1/matches/#{match['id']}", @body.to_json, @headers
        expect( response.status ).to eq 400
        expect( response.body ).to match "update/after start time"
      end
    end

    it 'should update match' do
      @body[:player_1] = @current_user['id']
      @body[:start_time] = 2.days.from_now.to_datetime.rfc3339
      post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
      expect( response.status ).to eq 200
      match = JSON.parse response.body
      put "/api/v1/matches/#{match['id']}", @body.to_json, @headers
      expect( response.status ).to eq 200
    end
  end
end
