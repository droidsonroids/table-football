require 'rails_helper' 

describe 'GET /api/v1/matches', type: :request do
  include ActiveSupport::Testing::TimeHelpers

  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      get '/api/v1/matches'
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @current_user = create_user email: 'test@example.com', first_name: 'Marek', last_name: 'Banan'
      @headers.merge! @current_user
    end

    context 'user is not assigned to any group' do
      it 'should return 404' do
        get '/api/v1/matches', {}, @headers
        expect( response.status ).to eq 200
        expect( JSON.parse(response.body)['matches'] ).to be_empty
      end
    end
 
    context 'user assigned to two groups' do
      before :each do
        @group_admin_1 = create_user email: 'group_admin_1@example.com'
        @group_1 = create_group({name: 'group 1'}, @headers.merge(@group_admin_1))

        @group_admin_2 = create_user email: 'group_admin_2@example.com'
        @group_2 = create_group({name: 'group 2'}, @headers.merge(@group_admin_2))

        @group_admin_3 = create_user email: 'group_admin_3@example.com'
        @group_3 = create_group({name: 'group 3'}, @headers.merge(@group_admin_3))


        add_user_to_group @group_1, @current_user, @group_admin_1
        add_user_to_group @group_2, @current_user, @group_admin_2
      end

      describe 'validation' do
        it 'should return 400 when from_time is greater then to_time' do
          get '/api/v1/matches', { 
            from_time: '2000-01-01T12:00:00.00+00:00',
            to_time: '2000-01-01T10:00:00.00+00:00'
          }, @headers
          expect( response.status ).to eq 400
          expect( response.body ).to match /Param from_time can not be after to_time/
        end

        it 'should return 400 when user tries to access other group' do
          get '/api/v1/matches', { group_id: @group_3['id'] }, @headers
          expect( response.status ).to eq 400
          expect( response.body ).to match /You can not access this group/
        end

        it 'should return 200' do
          get '/api/v1/matches', { group_id: @group_2['id'] }, @headers
          expect( response.status ).to eq 200
        end
      end

      it 'should return two matches' do
        match_1 = create_match @group_1['id'], {
          player_1: @group_admin_1['id'].to_i,
          player_2: nil,
          start_time: 2.hours.from_now.to_datetime.rfc3339,
          duration: 15,
          type: '1v1',
          private: false
        }, @group_admin_1

        match_2 = create_match @group_2['id'], {
          player_1: @group_admin_2['id'].to_i,
          player_2: nil,
          start_time: 2.hours.from_now.to_datetime.rfc3339,
          duration: 15,
          type: '1v1',
          private: false
        }, @group_admin_2

        match_3 = create_match @group_3['id'], {
          player_1: @group_admin_3['id'].to_i,
          player_2: nil,
          start_time: 2.hours.from_now.to_datetime.rfc3339,
          duration: 15,
          type: '1v1',
          private: false
        }, @group_admin_3

        get '/api/v1/matches', {}, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data['matches'].size ).to eq 2
        expect( data['matches'].map { |match| match['id'] } ).to contain_exactly match_1['id'], match_2['id']
      end

      describe 'group_id param' do
        before :each do
          @match_1 = create_match @group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: nil,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false
          }, @group_admin_1

          @match_2 = create_match @group_2['id'], {
            player_1: @group_admin_2['id'].to_i,
            player_2: nil,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false
          }, @group_admin_2

          @match_3 = create_match @group_3['id'], {
            player_1: @group_admin_3['id'].to_i,
            player_2: nil,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false
          }, @group_admin_3
        end

        it 'should return first match' do
          get '/api/v1/matches', { group_id: @group_1['id'] }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 1
          expect( JSON.parse(response.body)['matches'][0]['id'] ).to eq @match_1['id']

        end

        it 'should return last match' do
          get '/api/v1/matches', { group_id: @group_2['id'] }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 1
          expect( JSON.parse(response.body)['matches'][0]['id'] ).to eq @match_2['id']
        end

        it 'should return 400' do
          get '/api/v1/matches', { group_id: @group_3['id'] }, @headers
          expect( response.status ).to eq 400
        end
      end

      describe 'type param' do
        before :each do
          @match_1 = create_match @group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: nil,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false
          }, @group_admin_1

          @match_2 = create_match @group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: nil,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false
          }, @group_admin_1

          @match_3 = create_match @group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: nil,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '2v2',
            private: false
          }, @group_admin_1
        end

        it 'should return 3 matches' do
          get '/api/v1/matches', { type:  nil }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 3
        end

        it 'should return 3 matches' do
          get '/api/v1/matches', { type: 'all' }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 3
        end

        it 'should return 2 matches' do
          get '/api/v1/matches', { type: '1v1' }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 2
          expect( JSON.parse(response.body)['matches'].map{ |match| match['id'] } ).to contain_exactly @match_1['id'], @match_2['id']
        end

        it 'should return 1 matches' do
          get '/api/v1/matches', { type: '2v2' }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 1
          expect( JSON.parse(response.body)['matches'].map{ |match| match['id'] } ).to contain_exactly @match_3['id']
        end
      end

      describe 'private param' do
        before :each do
          @match_1 = create_match @group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: nil,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false
          }, @group_admin_1

          @match_2 = create_match @group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: @current_user['id'].to_i,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: true
          }, @group_admin_1
        end

        it 'should return two matches' do
          get '/api/v1/matches', { private: nil }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 2
        end 
      
        it 'should return two matches' do
          get '/api/v1/matches', { private: 'all' }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 2
        end 

        it 'should return two matches' do
          get '/api/v1/matches', { private: 'private' }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 1
          expect( JSON.parse(response.body)['matches'][0]['id'] ).to eq @match_2['id']
        end

        it 'should return two matches' do
          get '/api/v1/matches', { private: 'public' }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 1
          expect( JSON.parse(response.body)['matches'][0]['id'] ).to eq @match_1['id']
        end 
      end

      describe 'admin_id param' do
        before :each do
          @match_1 = create_match @group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: nil,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false
          }, @group_admin_1

          @match_2 = create_match @group_2['id'], {
            player_1: @group_admin_2['id'].to_i,
            player_2: nil,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false
          }, @group_admin_2
        end

        it 'should return two matches' do
          get '/api/v1/matches', { admin_id: nil }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 2
        end 
      
        it 'should return two matches' do
          get '/api/v1/matches', { admin_id: '*' }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 2
        end 

        it 'should return two matches' do
          get '/api/v1/matches', { admin_id: @group_admin_1['id'] }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 1
          expect( JSON.parse(response.body)['matches'][0]['id'] ).to eq @match_1['id']
        end

        it 'should return two matches' do
          get '/api/v1/matches', { admin_id: @group_admin_2['id'] }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 1
          expect( JSON.parse(response.body)['matches'][0]['id'] ).to eq @match_2['id']
        end 
      end

      describe 'player_id param' do
        before :each do
          @other_user = create_user email: 'other_user@xxxx.com'
          add_user_to_group @group_1, @other_user, @group_admin_1

           travel_to DateTime.parse('2000-01-01T10:00:00.00+00:00').to_time do
            @match_1 = create_match @group_1['id'], {
              player_1: @group_admin_1['id'].to_i,
              player_2: @current_user['id'].to_i,
              start_time: '2000-01-01T12:00:00.00+00:00',
              duration: 15,
              type: '1v1',
              private: false
            }, @group_admin_1

            @match_2 = create_match @group_1['id'], {
              player_1: @group_admin_1['id'].to_i,
              player_2: @other_user['id'].to_i,
              start_time: '2000-01-01T13:00:00.00+00:00',
              duration: 15,
              type: '1v1',
              private: false
            }, @group_admin_1
          end
        end

        it 'should return two matches' do
          get '/api/v1/matches', { player_id: nil }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 2
        end 
      
        it 'should return two matches' do
          get '/api/v1/matches', { player_id: '*' }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 2
        end 

        it 'should return two matches' do
          get '/api/v1/matches', { player_id: @current_user['id'] }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 1
          expect( JSON.parse(response.body)['matches'][0]['id'] ).to eq @match_1['id']
        end

        it 'should return two matches' do
          get '/api/v1/matches', { player_id: @other_user['id'] }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 1
          expect( JSON.parse(response.body)['matches'][0]['id'] ).to eq @match_2['id']
        end 

        it 'should return zero matches' do
          get '/api/v1/matches', { player_id: User.all.collect(&:id).max + 1 }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 0
        end 
      end

      describe 'table_number' do
        before :each do
          @match_1 = create_match @group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: nil,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false,
            table_number: 1
          }, @group_admin_1

         @match_2 = create_match @group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: nil,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false,
            table_number: 2
          }, @group_admin_1
        end

        it 'should return both matches' do
          get '/api/v1/matches', { table_number: nil }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 2
        end

        it 'should return both matches' do
          get '/api/v1/matches', { table_number: '*' }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 2
        end

        it 'should return first match' do
          get '/api/v1/matches', { table_number: 1 }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 1
          expect( JSON.parse(response.body)['matches'][0]['id'] ).to eq @match_1['id']
        end

        it 'should return last match' do
          get '/api/v1/matches', { table_number: 2 }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 1
          expect( JSON.parse(response.body)['matches'][0]['id'] ).to eq @match_2['id']
        end

        it 'should return zero matches' do
          get '/api/v1/matches', { table_number: 3 }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 0
        end
      end

      describe 'time params' do
        before :each do
          travel_to DateTime.parse('2000-01-01T10:00:00.00+00:00').to_time do
            @match_1 = create_match @group_1['id'], {
              player_1: @group_admin_1['id'].to_i,
              player_2: nil,
              start_time: '2000-01-01T10:00:00.00+00:00',
              duration: 15,
              type: '1v1',
              private: false
            }, @group_admin_1

            @match_2 = create_match @group_1['id'], {
              player_1: @group_admin_1['id'].to_i,
              player_2: nil,
              start_time: '2000-01-01T11:00:00.00+00:00',
              duration: 15,
              type: '1v1',
              private: false
            }, @group_admin_1

            @match_3 = create_match @group_1['id'], {
              player_1: @group_admin_1['id'].to_i,
              player_2: nil,
              start_time: '2000-01-01T12:00:00.00+00:00',
              duration: 15,
              type: '2v2',
              private: false
            }, @group_admin_1
          end
        end

        describe 'to_time' do
          it 'should return 3 matches' do
            get '/api/v1/matches', { from_time: nil }, @headers
            expect( response.status ).to eq 200
            expect( JSON.parse(response.body)['matches'].size ).to eq 3
          end

          it 'should return 3 matches' do
            get '/api/v1/matches', { from_time: '2000-01-01T10:00:00.00+00:00' }, @headers
            expect( response.status ).to eq 200
            expect( JSON.parse(response.body)['matches'].size ).to eq 3
          end

          it 'should return 2 matches' do
            get '/api/v1/matches', { from_time: '2000-01-01T10:30:00.00+00:00' }, @headers
            expect( response.status ).to eq 200
            expect( JSON.parse(response.body)['matches'].size ).to eq 2
            expect( JSON.parse(response.body)['matches'].map{ |match| match['id'] } ).to contain_exactly @match_2['id'], @match_3['id']
          end

          it 'should return 1 matche' do
            get '/api/v1/matches', { from_time: '2000-01-01T11:30:00.00+00:00' }, @headers
            expect( response.status ).to eq 200
            expect( JSON.parse(response.body)['matches'].size ).to eq 1
            expect( JSON.parse(response.body)['matches'].map{ |match| match['id'] } ).to contain_exactly @match_3['id']
          end

          it 'should return 0 matche' do
            get '/api/v1/matches', { from_time: '2000-01-01T12:30:00.00+00:00' }, @headers
            expect( response.status ).to eq 200
            expect( JSON.parse(response.body)['matches'].size ).to eq 0
          end
        end

        describe 'to_time param' do
          it 'should return 3 matches' do
            get '/api/v1/matches', { to_time: nil }, @headers
            expect( response.status ).to eq 200
            expect( JSON.parse(response.body)['matches'].size ).to eq 3
          end

          it 'should return 3 matches' do
            get '/api/v1/matches', { to_time: '2000-01-01T13:00:00.00+00:00' }, @headers
            expect( response.status ).to eq 200
            expect( JSON.parse(response.body)['matches'].size ).to eq 3
          end

          it 'should return 2 matches' do
            get '/api/v1/matches', { to_time: '2000-01-01T11:30:00.00+00:00' }, @headers
            expect( response.status ).to eq 200
            expect( JSON.parse(response.body)['matches'].size ).to eq 2
            expect( JSON.parse(response.body)['matches'].map{ |match| match['id'] } ).to contain_exactly @match_1['id'], @match_2['id']
          end

          it 'should return 1 matche' do
            get '/api/v1/matches', { to_time: '2000-01-01T10:51:00.00+00:00' }, @headers
            expect( response.status ).to eq 200
            expect( JSON.parse(response.body)['matches'].size ).to eq 1
            expect( JSON.parse(response.body)['matches'].map{ |match| match['id'] } ).to contain_exactly @match_1['id']
          end

          it 'should return 0 matche' do
            get '/api/v1/matches', { to_time: '2000-01-01T9:45:00.00+00:00' }, @headers
            expect( response.status ).to eq 200
            expect( JSON.parse(response.body)['matches'].size ).to eq 0
          end
        end
      end

      describe 'pagination' do
        before :each do
          @matches = []
          15.times do |i|
            @matches << create_match(@group_1['id'], {
              player_1: @group_admin_1['id'].to_i,
              player_2: nil,
              start_time: (i+1).hours.from_now.to_datetime.rfc3339,
              duration: 15,
              type: '1v1',
              private: false
            }, @group_admin_1)
          end
        end

        it 'should return first 5 results' do
          get '/api/v1/matches', { per_page: 5 }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 5
          expect( JSON.parse(response.body)['matches'].map { |match|
            match['id']
          }).to contain_exactly @matches[14]['id'], @matches[13]['id'], @matches[12]['id'], @matches[11]['id'], @matches[10]['id']
        end

        it 'should return pagination data' do
          get '/api/v1/matches', { per_page: 5 }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['pagination'] ).to eq({
            'page' => 1,
            'per_page' => 5,
            'total' => 15
          })  
        end

        it 'should return second 5 results' do
          get '/api/v1/matches', { page: 2, per_page: 5 }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 5
          expect( JSON.parse(response.body)['matches'].map { |match|
            match['id']
          }).to contain_exactly @matches[9]['id'], @matches[8]['id'], @matches[7]['id'], @matches[6]['id'], @matches[5]['id']
        end

        it 'should return last 3 results' do
          get '/api/v1/matches', { page: 3, per_page: 6 }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 3
          expect( JSON.parse(response.body)['matches'].map { |match|
            match['id']
          }).to contain_exactly @matches[2]['id'], @matches[1]['id'], @matches[0]['id']
        end

        it 'should return pagination data' do
          get '/api/v1/matches', { page: 3, per_page: 6 }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['pagination'] ).to eq({
            'page' => 3,
            'per_page' => 6,
            'total' => 15
          })  
        end
      end

      describe 'ordering' do
        it 'should return 2 results in correct order' do
          match_1 = create_match(@group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: nil,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false
          }, @group_admin_1)

          match_2 = create_match(@group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: nil,
            start_time: 3.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false
          }, @group_admin_1)
      
          get '/api/v1/matches', { }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 2
          expect( JSON.parse(response.body)['matches'].map { |match|
            match['id']
          }).to eq [match_2['id'], match_1['id']]
        end

        it 'should return 2 results in correct order' do
          match_1 = create_match(@group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: nil,
            start_time: 3.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false
          }, @group_admin_1)

          match_2 = create_match(@group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: nil,
            start_time: 1.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false
          }, @group_admin_1)

          match_3 = create_match(@group_1['id'], {
            player_1: @group_admin_1['id'].to_i,
            player_2: nil,
            start_time: 2.hours.from_now.to_datetime.rfc3339,
            duration: 15,
            type: '1v1',
            private: false
          }, @group_admin_1)

          get '/api/v1/matches', { }, @headers
          expect( response.status ).to eq 200
          expect( JSON.parse(response.body)['matches'].size ).to eq 3
          expect( JSON.parse(response.body)['matches'].map { |match|
            match['id']
          }).to eq [match_1['id'], match_3['id'], match_2['id']]
        end
      end
    end
  end
end
