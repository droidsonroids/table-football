require 'rails_helper'

describe 'GET api/v1/groups/:id', type: :request do
  before :each do 
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      get '/api/v1/groups/1234', {}, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @auth_headers = create_user email: 'test@example.com', password: 'PAssWOrd1234'
      @headers.merge! @auth_headers
    end

    it 'should return 404' do
      get '/api/v1/groups/1234', {}, @headers
      expect( response.status ).to eq 404
    end

    it 'should return 403' do
      create_user email: 'other@example.com', password: 'PAssWOrd1234'
      group = create_group email: 'other@example.com', password: 'PAssWOrd1234', name: 'gorup name', number_of_tables: 500
      get "/api/v1/groups/#{group['id']}", {}, @headers
      expect( response.status ).to eq 403
    end

    it 'should return 200 and group' do
      group = create_group email: 'test@example.com', password: 'PAssWOrd1234', name: 'group name', number_of_tables: 1000
      get "/api/v1/groups/#{group['id']}", {}, @headers
      expect( response.status ).to eq 200
      data = JSON.parse response.body
      expect( data.keys ).to eq(['id', 'name', 'number_of_tables', 'admin', 'users'])
      expect( data['id'] ).to eq group['id']
      expect( data['name'] ).to eq 'group name'
      expect( data['number_of_tables'] ).to eq 1000
    end
  end
end
