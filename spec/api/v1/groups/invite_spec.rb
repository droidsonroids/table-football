require 'rails_helper'

describe 'POST api/v1/groups/:id/invite', type: :request do
  include ActiveSupport::Testing::TimeHelpers

  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      body = {
        name: 'test',
        number_of_tables: 5
      }.to_json
      post '/api/v1/groups/1234/invite', body, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @auth_headers = create_user email: 'test@example.com', password: 'PAssWOrd1234'
      @headers.merge! @auth_headers

      @group = create_group({ name: 'test', number_of_tables: 1 }, @auth_headers)

      @body = {
        emails: ['test@example.com']
      }
    end

    it 'should return 404' do
      post '/api/v1/groups/1234/invite', @body.to_json, @headers
      expect( response.status ).to eq 404
    end

    it 'should return 403' do
      other_group = create_group({ name: 'other group', number_of_tables: 3},  create_user(email: 'xxxx@ex.com'))
      post "/api/v1/groups/#{other_group['id']}/invite", @body.to_json, @headers
      expect( response.status ).to eq 403
    end

    it 'should return one email invitation' do
      @body[:emails] = ['x1@ex.com']
      post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
      expect( response.status ).to eq 200
      data = JSON.parse response.body
      expect( data['emails']['x1@ex.com']['status'] ).to eq 'queued_send'
    end

    it 'should return two email invitations' do
      @body[:emails] = ['x1@ex.com', 'x2@ex.com']
      post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
      expect( response.status ).to eq 200
      data = JSON.parse response.body
      expect( data['emails']['x1@ex.com']['status'] ).to eq 'queued_send'
      expect( data['emails']['x2@ex.com']['status'] ).to eq 'queued_send'
    end 

    it 'should send invitation email' do
      @body[:emails] = ['x1@ex.com']
      expect{
        post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
      }.to change{
        ActionMailer::Base.deliveries.size
      }.by 1
    end

    it 'should send invitation email' do
      @body[:emails] = ['x1@ex.com', 'x2@ex.com']
      expect{
        post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
      }.to change{
        ActionMailer::Base.deliveries.size
      }.by 2
    end

    it 'should send only two email' do
      # send first two mail
      # the frist one was send
      # the second not
      @body[:emails] = ['x0@ex.com', 'x1@ex.com']
      post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers

      GroupEmailInvitation.find_by_email('x1@ex.com').update_attribute :last_sent_at, nil
      
      # send next two email
      travel 30.minutes do
        @body[:emails] = ['x2@ex.com', 'x3@ex.com']
        post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
        GroupEmailInvitation.find_by_email('x2@ex.com').update_attribute :last_sent_at, Time.current
      end
      travel 35.minutes do
        @body[:emails] = ['x0@ex.com', 'x1@ex.com', 'x2@ex.com', 'x3@ex.com', 'x4@ex.com']
        expect{
          post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
        }.to change{
          ActionMailer::Base.deliveries.size
        }.by 2
        data = JSON.parse response.body
        expect( data['emails'].keys ).to contain_exactly('x0@ex.com', 'x1@ex.com', 'x2@ex.com', 'x3@ex.com', 'x4@ex.com')
        expect( data['emails']['x4@ex.com']['status'] ).to eq 'queued_send'
        expect( data['emails']['x3@ex.com']['status'] ).to eq 'not_queued'
        expect( data['emails']['x2@ex.com']['status'] ).to eq 'not_queued'
        expect( data['emails']['x1@ex.com']['status'] ).to eq 'not_queued'
        expect( data['emails']['x0@ex.com']['status'] ).to eq 'queued_resend'
      end
    end
    
    describe 'users invitation' do 
      it 'should create new user invitation' do
        user_to_invite = create_user email: 'xxx@example.com'

        get '/api/v1/groups/invitations', {}.to_json, @headers.merge(user_to_invite)
        expect( JSON.parse(response.body) ).to eq []

        @body = {
          users: [user_to_invite['id'].to_i],
          emails: []
        }
        post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
        expect( response.status ).to eq 200

        get '/api/v1/groups/invitations', {}.to_json, @headers.merge(user_to_invite)
        expect( JSON.parse(response.body).size ).to eq 1
        expect( JSON.parse(response.body).first['group_id'] ).to eq @group['id']
        expect( JSON.parse(response.body).first['group_name'] ).to eq 'test'
      end

      it 'should return info that invitation has been created' do
        @body = {
          users: [1234],
          emails: []
        }
        post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
        expect( response.status ).to eq 200
        users = JSON.parse(response.body)['users']
        expect( users.keys ).to contain_exactly('1234')
        expect( users['1234']['status'] ).to eq 'user not found'
      end

      it 'should return info that invitation has been created' do
        user_to_invite = create_user email: 'xxx@example.com'
        @body = {
          users: [user_to_invite['id'].to_i],
          emails: []
        }
        post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
        expect( response.status ).to eq 200
        users = JSON.parse(response.body)['users']
        expect( users.keys ).to contain_exactly(user_to_invite['id'].to_s)
        expect( users[user_to_invite['id'].to_s]['status'] ).to eq 'ok'
      end

      it 'should return info that user has already been invited' do
        user_to_invite = create_user email: 'xxx@example.com'
        @body = {
          users: [user_to_invite['id'].to_i],
          emails: []
        }
        post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
        expect( response.status ).to eq 200
        post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
        expect( response.status ).to eq 200
        users = JSON.parse(response.body)['users']
        expect( users.keys ).to contain_exactly(user_to_invite['id'].to_s)
        expect( users[user_to_invite['id'].to_s]['status'] ).to eq 'already invited'
      end

      it 'should accept user request' do
        user_to_invite = create_user email: 'xxx@example.com'
 
        # user sends requests
        post "/api/v1/groups/#{@group['id']}/join", @body.to_json, @headers.merge(user_to_invite)
        expect( response.status ).to eq 204

        # check number of users
        get "/api/v1/users", { group_id: @group['id'] }, @headers
        expect( response.status ).to eq 200
        expect( JSON.parse(response.body).size ).to eq 1

        # invite
        @body = {
          emails: [],
          users: [user_to_invite['id'].to_i]
        }
        post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data['users'][user_to_invite['id'].to_s]['status'] ).to eq 'accepted request'

        # check number of users
        get "/api/v1/users", { group_id: @group['id'] }, @headers
        expect( response.status ).to eq 200
        expect( JSON.parse(response.body).size ).to eq 2
      end

      it 'should remove membership request' do
        user_to_invite = create_user email: 'xxx@example.com'
 
        # user sends requests
        post "/api/v1/groups/#{@group['id']}/join", @body.to_json, @headers.merge(user_to_invite)
        expect( response.status ).to eq 204

        # check number of requests
        get "/api/v1/groups/#{@group['id']}/requests", { group_id: @group['id'] }, @headers
        expect( response.status ).to eq 200
        expect( JSON.parse(response.body).size ).to eq 1

        # invite
        @body = {
          emails: [],
          users: [user_to_invite['id'].to_i]
        }
        post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
        expect( response.status ).to eq 200

        # check number of requests
        get "/api/v1/groups/#{@group['id']}/requests", { group_id: @group['id'] }, @headers
        expect( response.status ).to eq 200
        expect( JSON.parse(response.body).size ).to eq 0
      end
      
      it 'should not be possible to invite admin' do
        @body = {
          emails: [],
          users: [@headers['id']]
        }
        post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
        expect( response.status ).to eq 200
        expect( JSON.parse(response.body)['users'][@headers['id'].to_s]['status'] ).to eq 'is member'
      end

      it 'should not create invitation for user who is member' do
        member = create_user email: 'member@example.com'
        add_user_to_group @group, member, @headers
        @body = {
          emails: [],
          users: [member['id']]
        }
        post "/api/v1/groups/#{@group['id']}/invite", @body.to_json, @headers
        expect( response.status ).to eq 200
        expect( JSON.parse(response.body)['users'][member['id'].to_s]['status'] ).to eq 'is member'
       
        get '/api/v1/groups/invitations', {}, @headers.merge(member)
        expect( response.status ).to eq 200
        expect( JSON.parse response.body ).to be_empty
      end
 
      it 'should return info that user already is member of this group'
    end
  end
end
