require 'rails_helper'

describe 'POST api/v1/groups', type: :request do
  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      body = {
        name: 'test',
        number_of_tables: 5
      }.to_json
      post '/api/v1/groups', body, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @auth_headers = create_user email: 'test@example.com', password: 'PAssWOrd1234'
      @headers.merge! @auth_headers
    end

    context 'with invalid params' do
      it 'should return 400 if name is invalid' do
        @body = {
          name: 'x',
          number_of_tables: 3
        }
        post '/api/v1/groups', @body.to_json, @headers
        expect( response.status ).to eq 400
        expect( response.body ).to match /Name is too short/
      end

      it 'should return 400 if name is not unique' do
        @body = {
          name: 'name',
          number_of_tables: 3
        }
        post '/api/v1/groups', @body.to_json, @headers
        expect( response.status ).to eq 200
        post '/api/v1/groups', @body.to_json, @headers
        expect( response.status ).to eq 400
        expect( response.body ).to match /Name is not unique/
      end

      it 'should return 400 if number_of_tables is invalid' do
        @body = {
          name: 'xxxx',
          number_of_tables: -5
        }
        post '/api/v1/groups', @body.to_json, @headers
        expect( response.status ).to eq 400 
      end
    end

    context 'with valid params' do
      before :each do
        @body = {
          name: 'test',
          number_of_tables: 3
        }
      end

      it 'should return 200' do	
        post '/api/v1/groups', @body.to_json, @headers
        expect( response.status ).to eq 200  
      end

      it 'should return created group' do
        post '/api/v1/groups', @body.to_json, @headers
        data = JSON.parse response.body
        expect( data.keys ).to contain_exactly('id', 'name', 'number_of_tables', 'admin', 'users')
        expect( data['id'] ).to be_a Fixnum
        expect( data['name'] ).to eq 'test'
        expect( data['number_of_tables'] ).to eq 3
      end

      it 'should add admin user to members' do
        post '/api/v1/groups', @body.to_json, @headers
        id = JSON.parse(response.body)['id']
        get '/api/v1/users', { group_id: id }, @headers
        expect( JSON.parse(response.body).size ).to eq 1
        expect( JSON.parse(response.body)[0]['id'] ).to eq @headers['id']

      end
    end
  end
end
