require 'rails_helper'

describe 'POST api/v1/groups/:id/join', type: :request do
  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      body = {
        name: 'test',
        number_of_tables: 5
      }.to_json
      post '/api/v1/groups/1234/join', body, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @auth_headers = create_user email: 'test@example.com', password: 'PAssWOrd1234'
      @headers.merge! @auth_headers

      @group_admin = create_user email: 'admin@example.com', password: 'PAssWOrd1234'
      @group = create_group({ name: 'group', number_of_tables: 1 }, @headers.merge(@group_admin))
    end

    it 'should return 404' do
      post '/api/v1/groups/1234/join', @body.to_json, @headers
      expect( response.status ).to eq 404
      expect( response.body ).to match /Not found/
    end

    it 'should return 204 if user is not invited' do
      post "/api/v1/groups/#{@group['id']}/join", @body.to_json, @headers
      expect( response.status ).to eq 204
    end

    it 'should accept invitation' do
      # invite user
      post "/api/v1/groups/#{@group['id']}/invite", { users: [@auth_headers['id'].to_i] }.to_json, @headers.merge(@group_admin)
    
      # check number of invitations
      get '/api/v1/groups/invitations', {}.to_json, @headers
      expect( JSON.parse(response.body).size ).to eq 1

      # check number of group members
      get "/api/v1/users?group_id=#{@group['id']}", {}.to_json, @headers 
      expect( JSON.parse(response.body).size ).to eq 1
  
      # perform join   
      post "/api/v1/groups/#{@group['id']}/join", @body.to_json, @headers
      expect( response.status ).to eq 204

      # check number of invitations
      get '/api/v1/groups/invitations', {}.to_json, @headers
      expect( JSON.parse(response.body).size ).to eq 0
  
      # check number of  group members
      get "/api/v1/users?group_id=#{@group['id']}", {}.to_json, @headers 
      expect( JSON.parse(response.body).size ).to eq 2
    end

    it 'should not be possible to join two times even with invitation' do
      # invite user
      post "/api/v1/groups/#{@group['id']}/invite", { users: [@auth_headers['id'].to_i] }.to_json, @headers.merge(@group_admin)
    
      # check number of invitations
      get '/api/v1/groups/invitations', {}.to_json, @headers
      expect( JSON.parse(response.body).size ).to eq 1

      # check number of group members
      get "/api/v1/users?group_id=#{@group['id']}", {}.to_json, @headers 
      expect( JSON.parse(response.body).size ).to eq 1

      # due to some error user become member of gorup 
      Group.find(@group['id']).users << User.find(@auth_headers['id'])

      # perform join   
      post "/api/v1/groups/#{@group['id']}/join", @body.to_json, @headers
      expect( response.status ).to eq 409
    end

    it 'should create membership request' do
      # check number of membership requests
      get "/api/v1/groups/#{@group['id']}/requests", {}, @headers.merge(@group_admin)
      expect( JSON.parse(response.body).size ).to eq 0

      # perform join   
      post "/api/v1/groups/#{@group['id']}/join", @body.to_json, @headers
      expect( response.status ).to eq 204

      # check number of membership requests
      get "/api/v1/groups/#{@group['id']}/requests", {}, @headers.merge(@group_admin)
      expect( JSON.parse(response.body).size ).to eq 1
    end
  end
end
