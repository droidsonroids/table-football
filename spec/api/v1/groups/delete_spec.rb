require 'rails_helper'

describe 'DELETE api/v1/groups/:id', type: :request do
  before :each do 
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      delete '/api/v1/groups/1234', {}, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @auth_headers = create_user email: 'test@example.com', password: 'PAssWOrd1234'
      @headers.merge! @auth_headers
    end

    it 'should return 404' do
      delete '/api/v1/groups/1234', {}, @headers
      expect( response.status ).to eq 404
    end

    it 'should return 403' do
      create_user email: 'other@example.com', password: 'PAssWOrd1234'
      group = create_group email: 'other@example.com', password: 'PAssWOrd1234', name: 'gorup name', number_of_tables: '1234'
      delete "/api/v1/groups/#{group['id']}", {}, @headers
      expect( response.status ).to eq 403
    end

    it 'should return 204 and empty body' do
      group = create_group email: 'test@example.com', password: 'PAssWOrd1234', name: 'gorup name', number_of_tables: '1234'
      delete "/api/v1/groups/#{group['id']}", {}, @headers
      expect( response.status ).to eq 204
      expect( response.body ).to be_blank
    end
  end
end
