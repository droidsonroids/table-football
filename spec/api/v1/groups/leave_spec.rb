require 'rails_helper'

describe 'POST /api/v1/groups/:id/leave', type: :request do
  before :each do 
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      delete '/api/v1/groups/1234/leave', {}, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @auth_headers = create_user email: 'test@example.com', password: 'PAssWOrd1234'
      @headers.merge! @auth_headers
      @group_admin = create_user email: 'user@example.com'
      @group = create_group({ name: 'group' }, @group_admin)
    end

    it 'should not be possible to remove admin' do
      my_group = create_group({ name: 'my group' }, @headers)
      delete "/api/v1/groups/#{my_group['id']}/leave", {}, @headers
      p response.body
      expect( response.status ).to eq 400
      expect( response.body ).to match /You are admin of this group/
    end

    it 'should return 404' do
      delete '/api/v1/groups/1234/leave', {}, @headers
      expect( response.status ).to eq 404
    end

    it 'should return 400' do
      delete "/api/v1/groups/#{@group['id']}/leave", {}, @headers
      expect( response.status ).to eq 400
    end

    context 'with one user' do
      before :each do
        add_user_to_group @group, @auth_headers, @group_admin
      end

      it 'should return 204 and empty body' do
        delete "/api/v1/groups/#{@group['id']}/leave", {}, @headers
        expect( response.status ).to eq 204
        expect( response.body ).to be_blank
      end

      it 'should remove user from group' do
        get '/api/v1/users', { group_id: @group['id'] }, @headers
        expect( JSON.parse(response.body).map{ |user| user['id'] } ).to contain_exactly @group_admin['id'], @auth_headers['id']

        delete "/api/v1/groups/#{@group['id']}/leave", {}, @headers
        expect( response.status ).to eq 204

        get '/api/v1/users', { group_id: @group['id'] }, @headers
        expect( JSON.parse(response.body).map{ |user| user['id'] } ).to contain_exactly @group_admin['id']

      end
    end

    context 'with one invited user' do
      before :each do
        post "/api/v1/groups/#{@group['id']}/invite", { 
          emails: [], users: [@auth_headers['id'].to_i] 
        }.to_json, @headers.merge(@group_admin)
      end

      it 'should return 204 and empty body' do
        delete "/api/v1/groups/#{@group['id']}/leave", {}, @headers
        expect( response.status ).to eq 204
        expect( response.body ).to be_blank
      end

      it 'should remove invitation' do
        get '/api/v1/groups/invitations', {}.to_json, @headers
        expect( JSON.parse(response.body).size ).to eq 1

        delete "/api/v1/groups/#{@group['id']}/leave", {}, @headers
        expect( response.status ).to eq 204

        get '/api/v1/groups/invitations', {}.to_json, @headers
        expect( JSON.parse(response.body).size ).to eq 0
      end
    end
  end
end
