require 'rails_helper'

describe 'POST /api/v1/groups/:id/matches', type: :request do
  include ActiveSupport::Testing::TimeHelpers

  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
    @other_user = create_user email: 'other_user@example.com'
    @group = create_group({name: 'group'}, @headers.merge(@other_user))
    
    @player_1 = create_user email: 'player1@example.com'
    @player_2 = create_user email: 'player2@example.com'
    add_user_to_group @group, @player_1, @other_user
    add_user_to_group @group, @player_2, @other_user

    @body = {
      name: 'Match name',
      type: '1v1',
      player_1: @player_1['id'],
      player_2: @player_2['id'],
      start_time: DateTime.now.rfc3339,
      duration: 15,
      table_number: 1,
      private: true
    }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      post '/api/v1/groups/1234/matches', @body.to_json, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @current_user = create_user email: 'test@example.com', first_name: 'Marek', last_name: 'Banan'
      @headers.merge! @current_user

      add_user_to_group @group, @current_user, @other_user
      
      @body[:player_1] = @current_user['id']
    end

    it 'should return 404' do
      post '/api/v1/groups/1234/matches', @body.to_json, @headers
      expect( response.status ).to eq 404
    end

    it 'should return 403' do
      other_user = create_user email: 'other@example.com'
      group = create_group({name: 'other group', number_of_tables: 5}, @headers.merge(other_user))
      post "/api/v1/groups/#{group['id']}/matches", @body.to_json, @headers
      expect( response.status ).to eq 403
    end 

    it 'should return 200' do
      @body[:private] = false
      @body[:player_2] = nil
      post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
      expect( response.status ).to eq 200
      data = JSON.parse response.body
      expect( data['id'] ).to_not be_nil
      expect( data['name'] ).to eq @body[:name]
      expect( data['table_number'] ).to eq @body[:table_number]
      expect( data['duration'] ).to eq @body[:duration]
      expect( data['admin'] ).to_not be_nil
      expect( data['admin']['id'] ).to eq @current_user['id']
      expect( data['admin']['first_name'] ).to eq 'Marek'
      expect( data['admin']['last_name'] ).to eq 'Banan'
      expect( data['player_1'] ).to_not be_nil
      expect( data['player_1']['id'] ).to eq @current_user['id']
      expect( data['player_1']['first_name'] ).to eq 'Marek'
      expect( data['player_1']['last_name'] ).to eq 'Banan'
      expect( data['player_2'] ).to eq nil
      expect( data ).to_not have_key 'player_3'
      expect( data ).to_not have_key 'player_4'
    end

    describe 'setting poitns' do
      it 'should return 400 when user tries to set points' do
        @body[:points_1] = 2
        @body[:points_2] = 1
        post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
        expect( response.status ).to eq 400
      end
  
      it 'should return 200 when points are nil' do
        @body[:points_1] = nil
        @body[:points_2] = nil
        post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
        expect( response.status ).to eq 200
      end
    end

    describe 'automatic table selection' do
      before :each do
        @body[:start_time] = DateTime.now.rfc3339
      end

      context 'group with one table' do
        before :each do
          @group = create_group({name: 'name', number_of_tables: 1}, @headers)
          add_user_to_group @group, @player_2, @current_user

        end

        it 'should choose first table' do
          @body[:table_number] = nil
          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 200 
          expect( JSON.parse(response.body)['table_number'] ).to eq 1
        end

        it 'should return 400' do
          @body[:table_number] = nil
          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 200 
          expect( JSON.parse(response.body)['table_number'] ).to eq 1

          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 400 
        end
      end

      describe 'automatic name' do
        before :each do
          @body[:name] = nil
        end
 
        it 'should create simple name' do
          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 200 
          expect( JSON.parse(response.body)['name'] ).to eq 'Match #1'
        end

        it 'should create simple name' do
          @body[:table_number] = 1
          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 200 

          @body[:table_number] = 2
          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 200 

          expect( JSON.parse(response.body)['name'] ).to eq 'Match #2'
        end
      end     
 
      context 'group with 5 tables' do
        before :each do
          @group = create_group({name: 'name', number_of_tables: 5}, @headers)
          add_user_to_group @group, @player_2, @current_user
        end

        it 'should choose first table' do
          @body[:table_number] = nil
          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 200 
          expect( JSON.parse(response.body)['table_number'] ).to eq 1
        end

        it 'should choose second table' do
          @body[:table_number] = nil
          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 200 

          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 200 
          expect( JSON.parse(response.body)['table_number'] ).to eq 2
        end

        it 'should select 4th table' do
          @body[:table_number] = 1
          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 200 

          @body[:table_number] = 3
          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 200 

          @body[:table_number] = 2
          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 200 

          @body[:table_number] = 5
          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 200 

          @body[:table_number] = nil
          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 200 
          expect( JSON.parse(response.body)['table_number'] ).to eq 4
        end

      end
    end

    describe 'setting players' do
      it 'should return that admin joined to match' do
        @body[:player_1] = @current_user['id']
        @body[:player_2] = nil
        @body[:private] = false
        post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data['player_1_state'] ).to eq 'joined'  
        expect( data['player_2_state'] ).to eq 'none'
        expect( data['player_3_state'] ).to eq nil
        expect( data['player_4_state'] ).to eq nil
      end

      it 'should return that admin joined and other player is invited' do
        @body[:player_1] = @current_user['id']
        @body[:player_2] = @player_2['id']
        @body[:private] = false
        post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data['player_1_state'] ).to eq 'joined'  
        expect( data['player_2_state'] ).to eq 'invited'
        expect( data['player_3_state'] ).to eq nil
        expect( data['player_4_state'] ).to eq nil
      end

      it 'should return that admin joined and other players are invited' do
        @body[:type] = '2v2'
        @body[:player_1] = nil
        @body[:player_2] = @current_user['id']
        @body[:player_3] = @player_1['id']
        @body[:player_4] = @player_2['id']
        @body[:private] = false
        post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data['player_1_state'] ).to eq 'none' 
        expect( data['player_2_state'] ).to eq 'joined'
        expect( data['player_3_state'] ).to eq 'invited'
        expect( data['player_4_state'] ).to eq 'invited'
      end 

    end

    describe 'validation' do
      it 'should return 400 when current user is not a player' do
        @body[:player_1] = @player_1['id']
        @body[:player_2] = @player_2['id']
        post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
        expect( response.status ).to eq 400
        expect( response.body ).to match /"admin_is_not_player":true/
      end

      it 'should return 400 when one of players is not gorup member' do
        other_user = create_user email: 'not_group_member@example.ecom'
        @body[:player_2] = other_user['id']
        post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
        expect( response.status ).to eq 400
        expect( JSON.parse(response.body)['errors']['player_2']['not_in_group'] ).to be_truthy
      end

      describe 'start time' do
        it 'should return 400 when start_time is in the past' do
          @body[:start_time] = '2000-01-01T12:00:00.00+02:00'
          travel_to DateTime.parse('2000-01-01T12:05:00.00+02:00').to_time do
            post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
            expect( response.status ).to eq 400
            expect( response.body ).to match /"past":true/
          end
        end
    
        it 'should return 200 when start_time is one minute in past' do
          @body[:start_time] = '2000-01-01T12:04:01.00+02:00'
          travel_to DateTime.parse('2000-01-01T12:05:00.00+02:00').to_time do
            post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
            expect( response.status ).to eq 200
            expect( response.body ).to_not match /"past":true/
          end
        end
      end

      describe 'number of table' do
        it 'should return 400 when number of table is greater then number of group tables' do
          @body[:table_number] = 6
          post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
          expect( response.status ).to eq 400
          expect( response.body ).to match /"table_does_not_exist":true/
        end
      end

      describe 'start time' do
        it 'should return 400 when there exist match in the same time' do
          travel_to DateTime.parse('2000-01-01T12:05:00.00+02:00').to_time do
            @body[:start_time] =  '2000-01-01T12:05:00.00+02:00'
            @body[:duration] = 5
            post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
            expect( response.status ).to eq 200
            post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
            expect( response.status ).to eq 400
            expect( response.body ).to match  /\"collision\":true/
          end
        end

        it 'should return 400 when tehre are intersection with two matches' do
          @body[:table_number] = 1
 
          travel_to DateTime.parse('2000-01-01T10:00:00.00+02:00').to_time do
            @body[:start_time] =  '2000-01-01T12:00:00.00+02:00'
            @body[:duration] = 15
            post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
            expect( response.status ).to eq 200

            @body[:start_time] =  '2000-01-01T12:30:00.00+02:00'
            @body[:duration] = 30
            post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
            expect( response.status ).to eq 200

            @body[:start_time] =  '2000-01-01T12:10:00.00+02:00'
            @body[:duration] = 40
            post "/api/v1/groups/#{@group['id']}/matches", @body.to_json, @headers
            expect( response.status ).to eq 400
            expect( response.body ).to match  /\"collision\":true/
          end
        end
      end
    end
  end 
end
