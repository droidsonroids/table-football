require 'rails_helper'

describe 'GET api/v1/groups/unique_name', type: :request do
  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do
      body = { name: 'name' }.to_json
      get '/api/v1/groups/unique_name', body, @headers
      expect( response.status ).to eq 401
    end
  end

  context 'sign in user' do
    before :each do
      post_auth email: 'test@example.com', password: 'PAssWOrd1234'
      accept_last_confirmation
      @auth_headers = post_auth_sign_in email: 'test@example.com', password: 'PAssWOrd1234'
      @headers.merge! @auth_headers
    end

    context 'invalid params' do
      it 'should return 400 and error that name is missing' do
        get '/api/v1/groups/unique_name', { name: '' }, @headers
        expect( response.status ).to eq 400
        expect( response.body ).to match /Name can't be blank./
      end

      it 'should return 400 and error that name is missing' do
        get '/api/v1/groups/unique_name', {}, @headers
        expect( response.status ).to eq 400
        expect( response.body ).to match /Keys name are missing./
      end

      it 'should return 400 and error that name is missing' do
        get '/api/v1/groups/unique_name', { name: 'xxxx', test: 'test' }, @headers
        expect( response.status ).to eq 400
        expect( response.body ).to match /Keys test should be removed./
      end
    end

    context 'with unique name' do
      before :each do
        get '/api/v1/groups/unique_name', { name: 'unique name' }, @headers
      end
  
      it 'should return 200' do
        expect( response.status ).to eq 200
      end
 
      it 'should return unique message' do 
        expect( response.body ).to eq({ unique: true }.to_json)
      end
    end

    context 'with not unique name' do
      before :each do
        body = {
          name: 'not unique name',
          number_of_tables: 3
        }.to_json
        post '/api/v1/groups', body, @headers
        get '/api/v1/groups/unique_name', { name: 'not unique name' }, @headers
      end
  
      it 'should return 200' do
        expect( response.status ).to eq 200
      end
 
      it 'should return unique message' do 
        expect( response.body ).to eq({ unique: false }.to_json)
      end
    end
  end
end
