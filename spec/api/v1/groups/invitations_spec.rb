require 'rails_helper'

describe 'GET api/v1/groups/invitations', type: :request do
  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      get '/api/v1/groups/invitations', {}.to_json, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @auth_headers = create_user email: 'test@example.com', password: 'PAssWOrd1234'
      @headers.merge! @auth_headers

      @other_user = create_user email: 'other@example.com', password: 'PAssWOrd1234'

      @group_admin_1 = create_user email: 'test_1@example.com', password: 'PAssWOrd1234'
      @group_1 = create_group({ name: 'group 1', number_of_tables: 1 }, @group_admin_1)

      @group_admin_2 = create_user email: 'test_2@example.com', password: 'PAssWOrd1234'
      @group_2 = create_group({ name: 'group 2', number_of_tables: 1 }, @group_admin_2)
    end

    it 'should return 200' do
      get '/api/v1/groups/invitations', {}.to_json, @headers
      expect( response.status ).to eq 200
    end

    it 'should return empty array' do
      get '/api/v1/groups/invitations', {}.to_json, @headers
      expect( JSON.parse(response.body) ).to eq []
    end

    it 'should show two invitations' do
      # invite other user
      post "/api/v1/groups/#{@group_1['id']}/invite", { users: [@other_user['id'].to_i] }.to_json, @headers.merge(@group_admin_1)
      # invite current user
      post "/api/v1/groups/#{@group_1['id']}/invite", { users: [@auth_headers['id'].to_i] }.to_json, @headers.merge(@group_admin_1)
      post "/api/v1/groups/#{@group_2['id']}/invite", { users: [@auth_headers['id'].to_i] }.to_json, @headers.merge(@group_admin_2)
      # get invitations
      get '/api/v1/groups/invitations', {}.to_json, @headers
      response_data =  JSON.parse(response.body)
      expect( response_data.size ).to eq 2
    end
  end
end
