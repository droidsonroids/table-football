require 'rails_helper'

describe 'GET api/v1/groups', type: :request do
  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      body = {
        name: 'test',
        number_of_tables: 5
      }.to_json
      get '/api/v1/groups', body, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @auth_headers = create_user email: 'test@example.com', password: 'PAssWOrd1234'
      @headers.merge! @auth_headers
    end

    it 'should return 200' do
      get '/api/v1/groups', {}, @headers
      expect( response.status ).to eq 200
    end

    it 'should return empty array' do
      get '/api/v1/groups', {}, @headers
      expect( JSON.parse(response.body) ).to be_instance_of Array
      expect( JSON.parse(response.body) ).to be_empty
    end

    it 'should return two groups' do
      create_user email: 'user1@example.com', password: 'PAssWOrd1234'
      group = create_group email: 'user1@example.com', password: 'PAssWOrd1234', name: 'gorup name 1', number_of_tables: '1234'
      create_user email: 'user2@example.com', password: 'PAssWOrd1234'
      group = create_group email: 'user2@example.com', password: 'PAssWOrd1234', name: 'gorup name 2', number_of_tables: '1234'
      get '/api/v1/groups', {}, @headers
      expect( JSON.parse(response.body) ).to be_instance_of Array
      expect( JSON.parse(response.body).size ).to eq 2
    end
  end
end
