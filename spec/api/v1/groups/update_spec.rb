require 'rails_helper'

describe 'PUT api/v1/groups/:id', type: :request do
  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401 for not existing group' do
      body = {
        name: 'test',
        number_of_tables: 5
      }.to_json
      put '/api/v1/groups/1234', body, @headers
      expect( response.status ).to eq 401
    end
 
    it 'should return 401' do
      auth_headers = create_user email: 'test@example.com', password: 'PAssWOrd1234'
      group = create_group email: 'test@example.com', password: 'PAssWOrd1234', name: 'group 1', number_of_tables: 3

      body = {
        name: 'test2',
        number_of_tables: 5
      }.to_json
      put "/api/v1/groups/#{group['id']}", body, @headers
      expect( response.status ).to eq 401
    end
  end

  context 'sign in user' do
    before :each do
      @auth_headers = create_user email: 'test@example.com', password: 'PAssWOrd1234'
      @headers.merge! @auth_headers
      @group = create_group email: 'test@example.com', password: 'PAssWOrd1234', name: 'group', number_of_tables: '5'
    end
   
    it 'should return 403 on updating not self group' do
      create_user email: 'test2@example.com', password: 'PAssWOrd1234'
      other_group = create_group email: 'test2@example.com', password: 'PAssWOrd1234', name: 'other group name', number_of_tables: 2
      body = {
        name: 'test',
        number_of_tables: 2
      }
      put "/api/v1/groups/#{other_group['id']}", body.to_json, @headers
      expect( response.status ).to eq 403
    end

    it 'should return 404 for not exsiting group' do
      body = {
        name: 'test',
        number_of_tables: 2
      }
      put "/api/v1/groups/1234", body.to_json, @headers
      expect( response.status ).to eq 404
    end 

    context 'with invalid params' do
      it 'should return 400 if name is invalid' do
        body = {
          name: 's',
          number_of_tables: 3
        }
        put "/api/v1/groups/#{@group['id']}", body.to_json, @headers
        expect( response.status ).to eq 400
        expect( response.body ).to match /Name is too short/
      end

      it 'should return 400 if name is not unique' do
        create_user email: 'test2@example.com', password: 'PAssWOrd1234'
        create_group email: 'test2@example.com', password: 'PAssWOrd1234', name: 'other group name', number_of_tables: 2
        body = {
          name: 'other group name',
          number_of_tables: 4
        }
        put "/api/v1/groups/#{@group['id']}", body.to_json, @headers
        expect( response.status ).to eq 400
        expect( response.body ).to match /Name is not unique/
      end

      it 'should return 400 if number_of_tables is invalid' do
        body = {
          name: 'xxxx',
          number_of_tables: 5.2
        }
        put "/api/v1/groups/#{@group['id']}", body.to_json, @headers
        expect( response.status ).to eq 400 
      end
    end

    context 'with valid params' do
      it 'should return 200' do
        body = {
          name: 'new group name',
          number_of_tables: 10432
        }
        put "/api/v1/groups/#{@group['id']}", body.to_json, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data.keys ).to eq(['id', 'name', 'number_of_tables', 'admin', 'users'])
        expect( data['id'] ).to eq @group['id']
        expect( data['name'] ).to eq 'new group name'
        expect( data['number_of_tables'] ).to eq 10432
      end

      it "should reutrn updated group when name doesn't change" do
        body = {
          name: @group['name'],
          number_of_tables: 20432
        }
        put "/api/v1/groups/#{@group['id']}", body.to_json, @headers
        expect( response.status ).to eq 200
        data = JSON.parse response.body
        expect( data.keys ).to eq(['id', 'name', 'number_of_tables', 'admin', 'users'])
        expect( data['id'] ).to eq @group['id']
        expect( data['name'] ).to eq @group['name']
        expect( data['number_of_tables'] ).to eq 20432
      end
    end
  end
end
