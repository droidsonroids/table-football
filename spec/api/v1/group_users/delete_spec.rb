require 'rails_helper'

describe 'DELETE /api/v1/groups/:group_id/users/:id', type: :request do
  before :each do 
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      delete '/api/v1/groups/1234/users/1234', {}, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @user = create_user email: 'test1@example.com'
      @other_user = create_user email: 'test2@example.com'
      @group_member = create_user email: 'test3@example.com', password: 'PAssWOrd1234'


      @headers.merge! @user
      @group = create_group({ name: 'group 1', number_of_tables: 1}, @headers)
      @other_group = create_group({name: 'group 2', number_of_tables: 1}, @headers.merge(@other_user))

      post "/api/v1/groups/#{@group['id']}/invite", { emails: ['user3@example.com'] }.to_json, @headers

      token = GroupEmailInvitation.last.token
      post "/group_invitations/#{token}", commit: 'Sign in', email: 'test3@example.com', password: 'PAssWOrd1234'
    end
  
    it 'should return 404' do
      delete '/api/v1/groups/1234/users/1234', {}, @headers
      expect( response.status ).to eq 404
    end

    it 'should reurn 403' do
      delete "/api/v1/groups/#{@other_group['id']}/users/1234", {}, @headers
      expect( response.status ).to eq 403
    end

    it 'should return 404' do
      delete "/api/v1/groups/#{@group['id']}/users/1234", {}, @headers
      expect( response.status ).to eq 404
    end

    it 'should return 404' do
      delete "/api/v1/groups/#{@group['id']}/users/#{@other_user['id']}", {}, @headers
      expect( response.status ).to eq 404
    end

    it 'should return 400' do
      delete "/api/v1/groups/#{@group['id']}/users/#{@user['id']}", {}, @headers
      expect( response.status ).to eq 400
    end

    it 'should return 204' do
      delete "/api/v1/groups/#{@group['id']}/users/#{@group_member['id']}", {}, @headers
      expect( response.status ).to eq 204
      expect( response.body ).to be_blank
    end

    it 'should remove user' do
      user_id = Proc.new { |user| user['id'] }

      get "/api/v1/groups/#{@group['id']}", {}, @headers
      expect( JSON.parse(response.body)['users'].collect(&user_id) ).to contain_exactly(@user['id'], @group_member['id'])

      delete "/api/v1/groups/#{@group['id']}/users/#{@group_member['id']}", {}, @headers
      
      get "/api/v1/groups/#{@group['id']}", {}, @headers
      expect( JSON.parse(response.body)['users'].collect(&user_id) ).to contain_exactly(@user['id'])
    end
  end
end
