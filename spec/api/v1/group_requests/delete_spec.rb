require 'rails_helper'

describe 'DELETE /api/v1/groups/:group_id/requests/:id' do
  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      delete '/api/v1/groups/1234/requests/1234/reject', {}, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @current_user = create_user email: 'current_user@exmaple.com'
      @headers.merge! @current_user
    end 

    it 'should return 404' do
      delete '/api/v1/groups/1234/requests/1234/reject', {}, @headers
      expect( response.status ).to eq 404
    end
 
    it 'should return 403' do
      other_user = create_user email: 'other_user@example.com'
      group = create_group({name: 'group 1'}, @headers.merge(other_user))
      delete "/api/v1/groups/#{group['id']}/requests/#{other_user['id'] }/reject", @headers
    end
 
    it 'should return 404' do
      other_user = create_user email: 'other_user@example.com'
      group = create_group({name: 'group 1'}, @headers)

      delete "/api/v1/groups/#{group['id']}/requests/#{other_user['id']}/reject", nil, @headers
      expect( response.status ).to eq 404
    end

    it 'should remove request' do
      user_1 = create_user email: 'user_1@example.com'
      user_2 = create_user email: 'user_2@example.com'
      group = create_group({ name: 'group' }, @headers)
       
      post "/api/v1/groups/#{group['id']}/join", {}.to_json, @headers.merge(user_1)
      post "/api/v1/groups/#{group['id']}/join", {}.to_json, @headers.merge(user_2)

      # check number of requests
      get "/api/v1/groups/#{group['id']}/requests", {}, @headers
      expect( response.status ).to eq 200
      expect( JSON.parse(response.body).size ).to eq 2

      # remove requests     
      delete "/api/v1/groups/#{group['id']}/requests/#{user_2['id']}/reject", nil, @headers
      expect( response.status ).to eq 204 
 
      # check number of requests
      get "/api/v1/groups/#{group['id']}/requests", {}, @headers
      expect( response.status ).to eq 200
      expect( JSON.parse(response.body).size ).to eq 1
      expect( JSON.parse(response.body)[0]['id'] ).to eq user_1['id']
    end
  end
end
