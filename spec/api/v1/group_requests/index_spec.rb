require 'rails_helper'

describe 'GET api/v1/groups/:id/requests', type: :request do
  before :each do
    @headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
  end

  context 'not sign in user' do
    it 'should return 401' do	
      get '/api/v1/groups/1234/requests', {}, @headers
      expect( response.status ).to eq 401
    end 
  end

  context 'sign in user' do
    before :each do
      @current_user = create_user email: 'current_user@example.com'
      @other_user = create_user email: 'other_user@example.com'
      @headers.merge! @current_user
      @group = create_group({ name: 'group 1', number_of_tables: 1}, @headers)
      @other_group = create_group({name: 'group 2', number_of_tables: 1}, @headers.merge(@other_user))
    end

    it 'should return 404' do
      get '/api/v1/groups/1234/requests', {}, @headers
      expect( response.status ).to eq 404
    end

    it 'should return 403' do
      get "/api/v1/groups/#{@other_group['id']}/requests", {}, @headers
      expect( response.status ).to eq 403
    end

    it 'should return 200 and empty list' do
      get "/api/v1/groups/#{@group['id']}/requests", {}, @headers
      expect( response.status ).to eq 200
      expect( response.body ).to eq [].to_json
    end

    it 'should return two requests' do
      @user_1 = create_user email: 'user1@example.com'
      @user_2 = create_user email: 'user2@example.com'
      post "/api/v1/groups/#{@group['id']}/join", {}.to_json, @headers.merge(@user_1)
      post "/api/v1/groups/#{@group['id']}/join", {}.to_json, @headers.merge(@user_2)
      get "/api/v1/groups/#{@group['id']}/requests", {}, @headers
      expect( response.status ).to eq 200
      data = JSON.parse response.body
      expect( data.size ).to eq 2
      expect( data.map{ |entry| entry['id'] } ).to contain_exactly(@user_1['id'], @user_2['id'] )
    end
  end
end
