require 'rails_helper'

describe 'GET /api/v1/sdfasfasasdfas' do
  it 'should return 404 not found in json' do
    get '/api/v1/sfdasfasdf'
    expect( response.status ).to eq 404
    expect{ JSON.parse response.body }.to_not raise_error
  end
end
