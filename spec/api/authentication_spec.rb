require 'rails_helper'

describe 'user/password authentication', type: :request do
  it 'should return 401 error and return message invalid login credentials' do
    body = { email: 'test@example.com', password: 'PAssWOrd1234' }.to_json
    post '/auth/sign_in', body, {'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json'}
    expect( JSON.parse(response.body)['errors'].first ).to match /Invalid login credentials/
    expect( response.status ).to eq 401
  end

  it 'should return 401 and message that confirmation email was sent' do
    post_auth email: 'test@example.com', password: 'PAssWOrd1234'
    
    body = { email: 'test@example.com', password: 'PAssWOrd1234' }.to_json
    post '/auth/sign_in', body, {'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json'}
    expect( JSON.parse(response.body)['errors'].first ).to match /confirmation email was sent/
    expect( response.status ).to eq 401
  end

  it 'should authenticate user' do
    post_auth email: 'test@example.com', password: 'PAssWOrd1234'

    accept_last_confirmation
    
    body = { email: 'test@example.com', password: 'PAssWOrd1234' }.to_json
    post '/auth/sign_in', body, {'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json'}

    expect( response.status ).to eq 200
    %w(token-type access-token client expiry uid).each do |key|
      expect( response.headers ).to have_key key
    end
  end
end
