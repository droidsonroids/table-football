require 'rails_helper'

describe 'user/password registration', type: :request do
  it 'should register new user' do
    body = {
      email: 'test@example.com',
      password: 'PASSword1234'
    }.to_json
    post '/auth', body, {'CONTENT_TYPE' => "application/json", 'ACCEPT' => 'application/json'}
    expect( response.status ).to eq 200
  end
end
