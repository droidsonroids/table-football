require 'rails_helper'

describe DbRepository::MatchRepository, type: :request do
  describe '#find_intersection' do
    it 'should return empty list' do
      expect( subject.find_intersection 1, DateTime.now, 10 ).to eq []
    end

    describe 'with with matches' do
      before :each do
        @group = FactoryGirl.create :group
        @matches = []
        @base_time = DateTime.parse '2000-01-01T12:00:00.00+02:00'
        5.times do |i|
          start_time = @base_time + i.hours
          @matches << FactoryGirl.create(:match, group: @group, start_time: start_time, duration: 15)
        end
      end
 
      it 'should return one intersection' do
        expect( subject.find_intersection @group.id, @base_time, 6*60 ).to contain_exactly *@matches
      end
 
      it 'should return nothing for other group' do
        expect( subject.find_intersection @group.id + 1, @base_time, 6*60 ).to be_empty
      end

      it 'should return one match' do
        expect( subject.find_intersection @group.id, @base_time + 125.minutes, 5 ).to eq [@matches[2]]
      end

      it 'should return two matches' do
        expect( subject.find_intersection @group.id, @base_time + 125.minutes, 65 ).to contain_exactly @matches[2], @matches[3]
      end
    end

    describe 'with two matches' do
      before :each do
        @group = FactoryGirl.create :group
        @base_time = DateTime.parse '2000-01-01T12:00:00.00+02:00'
        @match_1 = FactoryGirl.create :match, group: @group, start_time: @base_time + 5.minutes, duration: 30, table_number: 1
        @match_2 = FactoryGirl.create :match, group: @group, start_time: @base_time + 15.minutes, duration: 30, table_number: 2
      end

      it 'should return two matches' do
        expect( subject.find_intersection @group.id, @base_time, 120, 1 ).to contain_exactly @match_1
      end

      it 'should return two matches' do
        expect( subject.find_intersection @group.id, @base_time, 120, 2 ).to contain_exactly @match_2
      end

      it 'should return first match' do
        expect( subject.find_intersection @group.id, @base_time, 10 ).to contain_exactly @match_1

      end

      it 'should return first match' do
        expect( subject.find_intersection @group.id, @base_time + 40.minutes, 30 ).to contain_exactly @match_2
      end

      it 'should return empty list' do
        expect( subject.find_intersection @group.id, @base_time + 240.minutes, 30 ).to eq []
      end
    end
  end
end
