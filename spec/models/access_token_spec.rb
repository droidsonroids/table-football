require 'rails_helper'

describe AccessToken, type: :model do
  let(:access_token) { FactoryGirl.create :access_token }
 
  it 'should be cascade deleted with group' do
    expect{
      access_token.user.destroy
    }.to change{
      AccessToken.exists? access_token.id
    }
  end

  it 'should not be possible to create duplicated client id' do
    user = FactoryGirl.create :user
    expect{
      AccessToken.create!({
        user: user,
        client_id: access_token.client_id,
        encrypted_access_token: 'asfdadsf',
        encrypted_refresh_token: 'sgfsdgfsdg',
        expires_at: Time.now 
      })
    }.to raise_error ActiveRecord::RecordNotUnique
  end

  it 'should generate client id' do
    expect{
      access_token.generate_client_id
    }.to change{ access_token.client_id }
  end

  it 'should generate access token' do
    token = access_token.generate_access_token
    expect( BCrypt::Password.new(access_token.encrypted_access_token) == token ).to be_truthy
  end

  describe '#valid_access_token?' do
    it 'should return true' do
      token = access_token.generate_access_token
      expect( access_token.valid_access_token? token ).to be_truthy
    end

    it 'should return false' do
      token1 = access_token.generate_access_token
      token2 = access_token.generate_access_token
      expect( access_token.valid_access_token? token1 ).to be_falsey
    end

    it 'should return false' do
      expect( access_token.valid_access_token? 'sdgsfdg' ).to be_falsey
    end
  end

  describe '#valid_refresh_token?' do
    it 'should return true' do
      token = access_token.generate_refresh_token
      expect( access_token.valid_refresh_token? token ).to be_truthy
    end

    it 'should return false' do
      token1 = access_token.generate_refresh_token
      token2 = access_token.generate_refresh_token
      expect( access_token.valid_refresh_token? token1 ).to be_falsey
    end

    it 'should return false' do
      expect( access_token.valid_refresh_token? 'sdgsfdg' ).to be_falsey
    end
  end

  describe '#expires?' do
    it 'should return false' do
      access_token.update_attribute :expires_at, 1.day.from_now
      expect( access_token.expired? ).to be_falsey
    end

    it 'should return true' do
      access_token.update_attribute :expires_at, 1.day.ago
      expect( access_token.expired? ).to be_truthy
    end
  end
end
