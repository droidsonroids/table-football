require 'rails_helper'

describe Match, type: :model do
  let(:match) { FactoryGirl.create :match }

  it 'should be cascade deleted with group' do
    expect{
      match.group.destroy
    }.to change{
      Match.exists? match.id
    }
  end

  [1, 2, 3, 4].each do |i|
    it "should set null whene player_#{i} is deleted" do
      expect{
        match.send("player_#{i}").destroy
      }.to change{
        match.reload.send("player_#{i}_id")
      }.to nil
    end
  end
end
