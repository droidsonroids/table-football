require 'rails_helper'

describe GroupUserInvitation do
  let!(:group_user_invitation) { FactoryGirl.create :group_user_invitation }

  it 'should be cascade deleted with group' do
    expect{
      group_user_invitation.group.destroy
    }.to change{
      GroupUserInvitation.exists? group_user_invitation.id
    }
  end

  it 'should be cascade deleted with user' do
    expect{
      group_user_invitation.user.destroy
    }.to change{
      GroupUserInvitation.exists? group_user_invitation.id
    }
  end

  it 'should not be possible to created duplicated entry' do
    expect{
      GroupUserInvitation.create!({
        user: group_user_invitation.user,
        group: group_user_invitation.group,
        created_at: Time.now
      })
    }.to raise_error ActiveRecord::RecordNotUnique
  end 
end
