require 'rails_helper'

describe GroupMembershipRequest, type: :model do
  let(:group_membership_request) { FactoryGirl.create :group_membership_request }
  
  it 'should be cascade deleted with group' do
    expect{
      group_membership_request.group.destroy
    }.to change{
      GroupMembershipRequest.exists? group_membership_request.id
    }
  end

  it 'should be cascade deleted with user' do
    expect{
      group_membership_request.user.destroy
    }.to change{
      GroupMembershipRequest.exists? group_membership_request.id
    }
  end

  it 'should not be possible to create two requests' do
    expect{
      GroupMembershipRequest.create group_membership_request.attributes
    }.to raise_error ActiveRecord::RecordNotUnique
  end
end
