require 'rails_helper'

describe Group do
  let(:group) { FactoryGirl.create :group }

  it 'should be cascaded deleted with user' do
    expect{
      group.admin.destroy
    }.to change{ 
      Group.exists? group.id
    }.from(true).to false
  end

  it 'should not be possible to create two groups with same name' do
    expect{
      Group.create group.attributes
    }.to raise_error ActiveRecord::RecordNotUnique
  end
end
