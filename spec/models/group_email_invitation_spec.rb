require 'rails_helper'

describe GroupEmailInvitation, type: :model do
  let(:group_email_invitation) { FactoryGirl.create :group_email_invitation }

  it 'should be cascaded deleted with group' do
    expect{
      group_email_invitation.group.destroy
    }.to change{
      GroupEmailInvitation.exists? group_email_invitation.id
    }
  end

  it 'should be not possible to create two invitations for same group and email' do
    expect{
      GroupEmailInvitation.create!({
        email: group_email_invitation.email,
        group: group_email_invitation.group,
        token: 'asfadsdfads'
      })
    }.to raise_error ActiveRecord::RecordNotUnique
  end

  it 'should be not possible to create two invitataions with same token' do
    other_group = FactoryGirl.create :group
    expect{
      GroupEmailInvitation.create!({
        email: 'other@example.com',
        group: other_group,
        token: group_email_invitation.token
      })
    }.to raise_error ActiveRecord::RecordNotUnique
  end

  it 'should generate token' do
    invitation = FactoryGirl.create :group_email_invitation
    expect{
      invitation.generate_token
    }.to change{ invitation.token }
    expect( invitation.token ).to_not be_nil
  end
end

