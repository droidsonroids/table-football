require 'rails_helper'

describe 'group invitations', type: :feature do

  let!(:invitation) { FactoryGirl.create :group_email_invitation }

  it 'should show 404 page' do
    visit '/group_invitations/234243'
    expect( page ).to have_content 'Not found'
  end

  it 'should show sign in/sign up page' do
    visit "/group_invitations/#{invitation[:token]}"
    expect( page ).to have_content 'Sign in' 
    expect( page ).to have_content 'Sign up' 
  end

  describe 'accept by sign in' do
    it 'should show meessage that email or password are not correct' do
      visit "/group_invitations/#{invitation[:token]}"
      within '.sign-in-form' do
        fill_in 'Email', with: 'test@example.com'
        fill_in 'Password', with: 'safasfasdfs'
        click_on 'Sign in'
      end

      expect( page ).to have_content 'Email or password are incorrect'
    end

    it 'should accept invitation' do
      FactoryGirl.create :user, email: 'test@example.com', provider: 'email', uid: 'test@example.com', password: 'PAssWOrd1234'

      visit "/group_invitations/#{invitation[:token]}"
      within '.sign-in-form' do
        fill_in 'Email', with: 'test@example.com'
        fill_in 'Password', with: 'PAssWOrd1234'
        click_on 'Sign in'
      end
      expect( page ).to have_content 'Success!'
    end
   
    it 'should show info that you have been already added to that group' do
      user = FactoryGirl.create :user, email: 'test@example.com', provider: 'email', uid: 'test@example.com', password: 'PAssWOrd1234'
      invitation.group.users << user

      visit "/group_invitations/#{invitation[:token]}"
      within '.sign-in-form' do
        fill_in 'Email', with: 'test@example.com'
        fill_in 'Password', with: 'PAssWOrd1234'
        click_on 'Sign in'
      end
      expect( page ).to have_content 'You are group member!'
    end
  end

  describe 'accept by sign up' do
    it 'should show errors when all fields are blank' do
      visit "/group_invitations/#{invitation[:token]}"
      within '.sign-up-form' do
        click_on 'Sign up'
      end
      expect( page ).to have_content "Email can't be blank"
      expect( page ).to have_content "Password can't be blank"
      expect( page ).to have_content "Password confirmation can't be blank"
      expect( page ).to have_content "First name can't be blank"
      expect( page ).to have_content "Last name can't be blank"
    end

    it 'should show errors email is not unique' do
      FactoryGirl.create :user, email: 'test@example.com', confirmed_at: Time.now, provider: 'email', uid: 'test@example.com'
      visit "/group_invitations/#{invitation[:token]}"
      within '.sign-up-form' do
        fill_in 'Email', with: 'test@example.com'
        click_on 'Sign up'
      end
      expect( page ).to have_content 'Email this email address is already in use'
    end

    it 'should show password confirmation error' do
      FactoryGirl.create :user, email: 'test@example.com', confirmed_at: Time.now, provider: 'email', uid: 'test@example.com'
      visit "/group_invitations/#{invitation[:token]}"
      within '.sign-up-form' do
        fill_in 'Password', with: 'sfdasddfasdfads'
        fill_in 'Password confirmation', with: 'dsfgsdgfsdgsfdfgsdfgfsd'
        click_on 'Sign up'
      end
      expect( page ).to have_content "Password confirmation doesn't match Password"
    end

    it 'should show success message' do
      visit "/group_invitations/#{invitation[:token]}"
      within '.sign-up-form' do
        fill_in 'Email', with: 'test@example.com'
        fill_in 'Password', with: 'PAssWOrd1234'
        fill_in 'Password confirmation', with: 'PAssWOrd1234'
        fill_in 'First name', with: 'PAssWOrd1234'
        fill_in 'Last name', with: 'PAssWOrd1234'
        click_on 'Sign up'
      end
      expect( page ).to have_content 'Success!'
    end
  end
end
