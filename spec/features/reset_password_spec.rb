require 'rails_helper'

describe 'reset password', type: :feature do
  let!(:user) { FactoryGirl.create :user, reset_password_token: 'asfsadfadsdfdsa' }

  before :each do
    user.update_attribute :reset_password_token, 'safasfads'
    user.update_attribute :reset_password_sent_at, Time.now
  end

  it 'should show 404 page' do
    visit '/reset_password/fwafasfsdfasd'
    expect( page ).to have_content 'Not found'
  end

  it 'should show reset password page' do
    visit "/reset_password/#{user.reset_password_token}"
    expect( page ).to have_content 'Reset your password!'
  end

  it 'should show error' do
    visit "/reset_password/#{user.reset_password_token}"
    expect( page ).to have_content 'Reset your password!'
    fill_in 'Password', with: 'XYZ1234abcd'
    fill_in 'Password confirmation', with: 'sdgsdf'
    click_on 'Submit'
    expect( page ).to have_content "Password confirmation doesn't match Password"
  end

  it 'should reset user password' do
    visit "/reset_password/#{user.reset_password_token}"
    expect( page ).to have_content 'Reset your password!'
    fill_in 'Password', with: 'XYZ1234abcd'
    fill_in 'Password confirmation', with: 'XYZ1234abcd'
    click_on 'Submit'
    expect( page ).to have_content 'Success!'
  end

  it 'should not allow to reset two times' do
    visit "/reset_password/#{user.reset_password_token}"
    expect( page ).to have_content 'Reset your password!'
    fill_in 'Password', with: 'XYZ1234abcd'
    fill_in 'Password confirmation', with: 'XYZ1234abcd'
    click_on 'Submit'
    expect( page ).to have_content 'Success!'

    visit "/reset_password/#{user.reset_password_token}"
    expect( page ).to have_content 'Not found'
  end

  it 'should show not found' do
    user.update_attribute :reset_password_sent_at, 1.year.ago
    visit "/reset_password/#{user.reset_password_token}"
    expect( page ).to have_content 'Not found'
 end
end
