require 'rails_helper'

describe AuthServices::GoogleIdKeyService, type: :service do
  subject{ AuthServices::GoogleIdKeyService.new @params, @dependencies }
  let(:google_adapter) { GoogleIdTokenAdapters::TestAdapter.new }
  let(:user_repository) { TestRepository::UserRepository.new }
  let(:access_token_repository) { TestRepository::AccessTokenRepository.new }
 
  before :each do
    @params = {
      id_token: '1234',
      email: 'email@example.com',
      first_name: 'First name', 
      last_name: 'Last name'
    }
    @dependencies = {
      google_adapter: google_adapter,
      user_repository: user_repository,
      access_token_repository: access_token_repository
    }
    user_repository.load_by_uid_return = nil
  end

  it 'should return result with user and auth hash' do
    result = subject.authenticate
    expect( result ).to respond_to :user
    expect( result ).to respond_to :tokens
  end

  context 'without user' do
    it 'should create one user' do
      expect{
        subject.authenticate
      }.to change{ user_repository.created_users.size }.from(0).to 1
      expect( user_repository.created_users.last[:provider] ).to eq 'google'
      expect( user_repository.created_users.last[:uid] ).to eq google_adapter.sub
      expect( user_repository.created_users.last[:email] ).to eq 'email@example.com'
      expect( user_repository.created_users.last[:first_name] ).to eq 'First name'
      expect( user_repository.created_users.last[:last_name] ).to eq 'Last name'
      expect( user_repository.created_users.last[:password] ).to_not be_nil
      expect( user_repository.created_users.last[:confirmed_at] ).to_not be_nil
    end

    it 'should raise error when email is invalid' do
      @params[:email] = 'emailsadfsadfsad'
      expect{
        subject.authenticate
      }.to raise_error ServicesErrors::ValidationError
    end
  end

  context 'with user' do
    it 'should not create user if one exists' do
      user_repository.load_by_uid_return = User.new provider: 'google', 'uid': google_adapter.sub
      expect{
        subject.authenticate
      }.to_not change{ user_repository.created_users.size }
    end
 
    it 'should not raise error when email is invalid and user has already exist' do
      user_repository.load_by_uid_return = User.new provider: 'google', 'uid': google_adapter.sub
      @params[:email] = 'emailsadfsadfsad'
      expect{
        subject.authenticate
      }.to_not raise_error
    end
  end 
end
