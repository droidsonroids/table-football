require 'rails_helper'

describe GroupServices::InviteService, type: :service do
  subject{ GroupServices::InviteService.new @user, @params, @dependencies }
  let(:group_repository) { TestRepository::GroupRepository.new true }
  let(:group_email_invitation_repository) { TestRepository::GroupEmailInvitationRepository.new }
  let(:group_membership_requests_repository) { TestRepository::GroupMembershipRequestsRepository.new }
  let(:user_repository) { TestRepository::UserRepository.new }
  let(:email_adapter) { GroupInvitationAdapters::TestEmailAdapter.new }

  before :each do
    @user = FactoryGirl.build :user
    @dependencies = {
      group_repository: group_repository, 
      group_email_invitation_repository: group_email_invitation_repository,
      group_membership_requests_repository: group_membership_requests_repository,
      user_repository: user_repository,
      email_adapter: email_adapter
    }
    @params = {
      group_id: 1,
      emails: []
    }
    @group = FactoryGirl.build :group, admin: @user
    group_repository.find_group = @group
  end

  it 'should raise not found error' do
    group_repository.stub(:find).and_raise RepositoriesErrors::NotFoundError
    expect{
      subject.invite
    }.to raise_error ServicesErrors::NotFoundError
  end 

  it 'should raise access denied error' do
    group_repository.find_group = FactoryGirl.build :group
    expect{
      subject.invite
    }.to raise_error ServicesErrors::AccessDeniedError
  end 

  it 'should create one email notification' do
    group_email_invitation_repository.load_groups = []
    @params[:emails] = ['e1@ex.com']
    subject.invite
    expect( group_email_invitation_repository.create_args ).to eq [[@group, 'e1@ex.com']]
  end

  it 'should create two email notifications' do
    group_email_invitation_repository.load_groups = []
    @params[:emails] = ['e1@ex.com', 'es2@ex.com']
    subject.invite
    expect( group_email_invitation_repository.create_args ).to eq [[@group, 'e1@ex.com'], [@group, 'es2@ex.com']]
  end

  it 'should not create new email notification' do
    group_email_invitation_repository.load_groups = [FactoryGirl.build(:group_email_invitation, email: 'es1@ex.com')]
    @params[:emails] = ['es1@ex.com']
    subject.invite
    expect( group_email_invitation_repository.create_args ).to eq []
  end

  it 'should create new email notification' do
    group_email_invitation_repository.load_groups = [FactoryGirl.build(:group_email_invitation, email: 'es1@ex.com', last_sent_at: 1.hour.ago)]
    @params[:emails] = ['es1@ex.com', 'es2@ex.com']
    subject.invite
    expect( group_email_invitation_repository.create_args ).to eq [[@group, 'es2@ex.com']]
  end

  it 'should send two email invitations' do
    group_email_invitation_repository.load_groups = [FactoryGirl.build(:group_email_invitation, email: 'es1@ex.com', last_sent_at: 1.hour.ago)]
    @params[:emails] = ['es1@ex.com', 'es2@ex.com']
    subject.invite
    expect( email_adapter.send_invitation_email_args.collect(&:email) ).to contain_exactly('es1@ex.com', 'es2@ex.com')
  end

  it 'should prevent spam invitations' do
    group_email_invitation_repository.load_groups = [
      FactoryGirl.build(:group_email_invitation, email: 'es1@ex.com', last_sent_at: 10.minutes.ago),
      FactoryGirl.build(:group_email_invitation, email: 'es2@ex.com', last_sent_at: 1.day.ago),
      FactoryGirl.build(:group_email_invitation, email: 'es3@ex.com', last_sent_at: nil),

    ]
    @params[:emails] = ['es1@ex.com', 'es2@ex.com', 'es3@ex.com']
    subject.invite
    expect( email_adapter.send_invitation_email_args.collect(&:email) ).to contain_exactly('es2@ex.com')
  end
end
