require 'rails_helper'

describe GroupServices::CreateService, type: :service do
  subject{ GroupServices::CreateService.new @user, @params, @dependencies }
  let(:group_repository) { TestRepository::GroupRepository.new true }

  before :each do
    @user = FactoryGirl.build :user
    @dependencies = {
      group_repository: group_repository
    }
    @params = {
      name: 'Group name',
      number_of_tables: 5
    }
  end

  context 'invalid params' do
    it 'should raise error when name is not unique' do
      @dependencies[:group_repository] = TestRepository::GroupRepository.new false
      expect{
        subject.create_group
      }.to raise_error ServicesErrors::ValidationError
    end

    it 'should raise error when name is too short' do
      @params[:name] = 'xx'
      expect{
        subject.create_group
      }.to raise_error ServicesErrors::ValidationError
    end
  end

  context 'with valid params' do
    it 'should create group with correct user' do
      expect{
        subject.create_group
      }.to change{
        group_repository.last_created_group_user
      }.from(nil).to @user
    end

    it 'should create group with correct attributes' do
      expect{
        subject.create_group
      }.to change{
        group_repository.last_created_group_attributes
      }.from(nil).to @params.merge(admin: @user)
    end
  end
end
