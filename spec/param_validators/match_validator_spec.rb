require 'rails_helper'

describe MatchValidator, type: :validator do
  subject{ MatchValidator.new @params, @admin_id, @number_of_tables, @allow_points, @player_in_group_proc, @time_past_proc, @time_collision_proc }

  before :each do
    @params = {
      name: 'Match name', 
      type: '2v2',
      private: false,
      player_1: 1,
      player_2: 2,
      player_3: 3,
      player_4: 4,
      start_time: '1937-01-01T12:00:27.87+00:20',
      duration: 15,
      table_number: 2
    }
    @admin_id = 1
    @number_of_tables = 10
    @player_in_group_proc = Proc.new { |_| true }
    @time_past_proc = Proc.new { |_| false }
    @time_collision_proc = Proc.new { |_| false }
    @allow_points = false
  end

  it 'should be valid' do
    result = subject.validate
    pp result.errors
    expect( result.valid ).to be_truthy
  end

  describe 'points_1, points_2' do
    [:points_1, :points_2].each do |key|
      it "should be valid with not set #{key}" do
        @params.delete key
        expect( subject.validate.valid ).to be_truthy
      end

      it "should be valid with '#{key}' = nil" do
        @params[key] = nil
        expect( subject.validate.valid ).to be_truthy
      end

      it "should be valid with '#{key}' = 5" do
        @params[key] = nil
        expect( subject.validate.valid ).to be_truthy
      end

      it "should be valid" do
        @allow_points = true
        @params[key] = 5
        expect( subject.validate.valid ).to be_truthy
      end

      it "should be invalid with '#{key}' = 'safasdfads'" do
        @params[key] = 'safsadf'
        expect( subject.validate.valid ).to be_falsey
      end

      it "should be invalid with '#{key}' = -5" do
        @params[key] = 'safsadf'
        expect( subject.validate.valid ).to be_falsey
      end

      it "should be invalid with '#{key}' = 2.2" do
        @params[key] = 'safsadf'
        expect( subject.validate.valid ).to be_falsey
      end

      it "should be invalid" do
        @allow_points = false
        @params[key] = 5
        expect( subject.validate.valid ).to be_falsey
      end
    end
  end

  describe 'name' do
    it 'should be valid with empty name' do
      @params[:name] = nil
      result = subject.validate
      expect( result.valid ).to be_truthy
    end
 
    it 'should be not valid with not string name' do
      @params[:name] = 1234
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:name][:not_string] ).to eq true
      expect( result.errors[:name][:too_long] ).to eq false
      expect( result.errors[:name][:too_short] ).to eq false
    end
 
    it 'should be not valid with too long name' do
      @params[:name] = 'x'*101
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:name][:not_string] ).to eq false
      expect( result.errors[:name][:too_long] ).to eq true
      expect( result.errors[:name][:too_short] ).to eq false
    end

    it 'should be not valid with too long name' do
      @params[:name] = 'xx'
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:name][:not_string] ).to eq false
      expect( result.errors[:name][:too_long] ).to eq false
      expect( result.errors[:name][:too_short] ).to eq true
    end
  end

  describe 'type' do
    it "should be valid with type '1v1'" do
      @params[:type] = '1v1'
      @params[:player_3] = nil
      @params[:player_4] = nil
      result = subject.validate
      expect( result.valid ).to be_truthy
    end

    it "should be valid with type '2v2'" do
      @params[:type] = '2v2'
      result = subject.validate
      expect( result.valid ).to be_truthy
    end

    it 'should be not valid when nil' do
      @params[:type] = nil
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:type][:missing] ).to eq true
      expect( result.errors[:type][:invalid] ).to eq false
    end

    it 'should be not valid when nil' do
      @params[:type] = 'asfads'
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:type][:missing] ).to eq false
      expect( result.errors[:type][:invalid] ).to eq true
    end
  end

  describe 'private' do
    it 'should be valid when true' do
      @params[:private] = true
      result = subject.validate
      expect( result.valid ).to be_truthy
    end

    it 'should be valid when false' do
      @params[:type] = '2v2'
      result = subject.validate
      expect( result.valid ).to be_truthy
    end

    it 'should be not valid when nil' do
      @params[:private] = nil
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:private][:missing] ).to eq true
      expect( result.errors[:private][:not_boolean] ).to eq false
    end

    it 'should be not valid when nil' do
      @params[:private] = 'asfads'
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:private][:missing] ).to eq false
      expect( result.errors[:private][:not_boolean] ).to eq true
    end
  end

  describe 'start_time' do
    it 'should be valid' do
      @params[:start_time] = '1937-01-01T12:00:27.87+00:20'
      result = subject.validate
      expect( result.valid ).to be_truthy
      expect( result.errors[:start_time][:missing] ).to eq false
      expect( result.errors[:start_time][:not_string] ).to eq false
      expect( result.errors[:start_time][:not_rfc3339] ).to eq false
      expect( result.errors[:start_time][:past] ).to eq false
      expect( result.errors[:start_time][:collision] ).to eq false
    end

    it 'should be invliad with nil start time' do
      @params[:start_time] = nil
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:start_time][:missing] ).to eq true
      expect( result.errors[:start_time][:not_string] ).to eq false
      expect( result.errors[:start_time][:not_rfc3339] ).to eq false
      expect( result.errors[:start_time][:past] ).to eq false
      expect( result.errors[:start_time][:collision] ).to eq false
    end

    it 'should be invliad with nil start time' do
      @params[:start_time] = 5234542352
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:start_time][:missing] ).to eq false
      expect( result.errors[:start_time][:not_string] ).to eq true
      expect( result.errors[:start_time][:not_rfc3339] ).to eq false
      expect( result.errors[:start_time][:past] ).to eq false
      expect( result.errors[:start_time][:collision] ).to eq false
    end

    it 'should be invliad with nil start time' do
      @params[:start_time] = 'sdgfsdgsfd'
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:start_time][:missing] ).to eq false
      expect( result.errors[:start_time][:not_string] ).to eq false
      expect( result.errors[:start_time][:not_rfc3339] ).to eq true
      expect( result.errors[:start_time][:past] ).to eq false
      expect( result.errors[:start_time][:collision] ).to eq false
    end

    it 'should be invalid if start time is in the past' do
      @time_past_proc = Proc.new { |_| true }
      @params[:start_time] = '1937-01-01T12:00:27.87+00:20'
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:start_time][:missing] ).to eq false
      expect( result.errors[:start_time][:not_string] ).to eq false
      expect( result.errors[:start_time][:not_rfc3339] ).to eq false
      expect( result.errors[:start_time][:past] ).to eq true
      expect( result.errors[:start_time][:collision] ).to eq false
    end

    it 'should be not valid when it colidates with other match' do
      @time_collision_proc = Proc.new { |_| true }
      @params[:start_time] = '1937-01-01T12:00:27.87+00:20'
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:start_time][:missing] ).to eq false
      expect( result.errors[:start_time][:not_string] ).to eq false
      expect( result.errors[:start_time][:not_rfc3339] ).to eq false
      expect( result.errors[:start_time][:past] ).to eq false
      expect( result.errors[:start_time][:collision] ).to eq true
    end
  end

  describe 'duration' do
    it 'should be valid' do
      @params[:duration] = 60
      result = subject.validate
      expect( result.valid ).to be_truthy
      expect( result.errors[:duration][:missing] ).to eq false
      expect( result.errors[:duration][:not_integer] ).to eq false
      expect( result.errors[:duration][:not_positive] ).to eq false
      expect( result.errors[:duration][:too_large] ).to eq false
    end

   it 'should be invalid with missing duration' do
      @params[:duration] = nil
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:duration][:missing] ).to eq true
      expect( result.errors[:duration][:not_integer] ).to eq false
      expect( result.errors[:duration][:not_positive] ).to eq false
      expect( result.errors[:duration][:too_large] ).to eq false
    end

    it 'should be invalid when string' do
      @params[:duration] = 'sfasdfdsa'
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:duration][:missing] ).to eq false
      expect( result.errors[:duration][:not_integer] ).to eq true
      expect( result.errors[:duration][:not_positive] ).to eq false
      expect( result.errors[:duration][:too_large] ).to eq false
    end

    it 'should be invalid when negative' do
      @params[:duration] = -5
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:duration][:missing] ).to eq false
      expect( result.errors[:duration][:not_integer] ).to eq false
      expect( result.errors[:duration][:not_positive] ).to eq true
      expect( result.errors[:duration][:too_large] ).to eq false
    end

    it 'should be invalid when zero' do
      @params[:duration] = 0
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:duration][:missing] ).to eq false
      expect( result.errors[:duration][:not_integer] ).to eq false
      expect( result.errors[:duration][:not_positive] ).to eq true
      expect( result.errors[:duration][:too_large] ).to eq false
    end

    it 'should be invalid when too large' do
      @params[:duration] = 10000000
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:duration][:missing] ).to eq false
      expect( result.errors[:duration][:not_integer] ).to eq false
      expect( result.errors[:duration][:not_positive] ).to eq false
      expect( result.errors[:duration][:too_large] ).to eq true
    end
  end

  describe 'table_number' do
    it 'should be valid when table number is nil' do
      @params[:table_number] = nil
      result = subject.validate
      expect( result.valid ).to be_truthy
      expect( result.errors[:table_number][:not_integer] ).to eq false
      expect( result.errors[:table_number][:not_positive] ).to eq false
      expect( result.errors[:table_number][:table_does_not_exist] ).to eq false
    end
  
    it 'should be valid when table number is 5' do
      @params[:table_number] = 5
      result = subject.validate
      expect( result.valid ).to be_truthy
      expect( result.errors[:table_number][:not_integer] ).to eq false
      expect( result.errors[:table_number][:not_positive] ).to eq false
      expect( result.errors[:table_number][:table_does_not_exist] ).to eq false
    end

    it 'should be invalid when number of tables is not integer' do
      @params[:table_number] = 'asfdasdfads'
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:table_number][:not_integer] ).to eq true
      expect( result.errors[:table_number][:not_positive] ).to eq false
      expect( result.errors[:table_number][:table_does_not_exist] ).to eq false
    end

    it 'should be invalid when number of tables is not integer' do
      @params[:table_number] = 5.2
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:table_number][:not_integer] ).to eq true
      expect( result.errors[:table_number][:not_positive] ).to eq false
      expect( result.errors[:table_number][:table_does_not_exist] ).to eq false
    end

    it 'should be invalid when number of tables is not integer' do
      @params[:table_number] = -53
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:table_number][:not_integer] ).to eq false
      expect( result.errors[:table_number][:not_positive] ).to eq true
      expect( result.errors[:table_number][:table_does_not_exist] ).to eq false
    end

    it 'should be invalid when number of table is greater then number of tables' do
      @params[:table_number] = 21
      @number_of_tables = 20
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:table_number][:not_integer] ).to eq false
      expect( result.errors[:table_number][:not_positive] ).to eq false
      expect( result.errors[:table_number][:table_does_not_exist] ).to eq true
    end
  end

  describe 'players' do
    it 'should be invalid when no player is admin' do
      @admin_id = 10
      @params[:player_1] = 1
      @params[:plyaer_2] = 2
      @params[:player_3] = 3
      @params[:player_4] = 4
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:players][:admin_is_not_player] ).to eq true
    end

  
    it 'should be invalid when type is 1v1 and player_3 is set' do
      @params[:type] = '1v1'
      @params[:player_3] = 3
      @params[:player_4] = nil
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:player_3][:not_nil] ).to eq true
      expect( result.errors[:player_4][:missing] ).to eq false
      expect( result.errors[:player_3][:not_valid_id] ).to eq false
      expect( result.errors[:player_3][:not_in_group] ).to eq false
      expect( result.errors[:player_3][:not_unique] ).to eq false
    end

    it 'should be invalid when type is 1v1 and player_4 is set' do
      @params[:type] = '1v1'
      @params[:player_3] = nil
      @params[:player_4] = 3
      result = subject.validate
      expect( result.valid ).to be_falsey
      expect( result.errors[:player_4][:not_nil] ).to eq true
      expect( result.errors[:player_4][:missing] ).to eq false
      expect( result.errors[:player_4][:not_valid_id] ).to eq false
      expect( result.errors[:player_4][:not_in_group] ).to eq false
      expect( result.errors[:player_4][:not_unique] ).to eq false
    end

    [1, 2, 3, 4].each do |i| 
      describe "player_#{i}" do
        before :each do
          @player_in_group_proc = Proc.new { |id| id <= 5 || id == 10 }
          @key = "player_#{i}".to_sym
          @params[@key] = 5
          @admin_id = @params[:"player_#{i % 4 +1}"]
          @params[:type] = '2v2'
        end

        it 'should be valid' do
          result = subject.validate
          expect( result.valid ).to be_truthy
        end 

        it 'should be valid when admin is playing' do
          @admin_id = 10
          @params[:player_1] = 1
          @params[:plyaer_2] = 2
          @params[:player_3] = 3
          @params[:player_4] = 4
          @params[@key] = 10 
          result = subject.validate
          expect( result.valid ).to be_truthy
        end

        it 'should be valid when nil and table is public' do
          @params[:private] = false
          @params[@key] = nil
          result = subject.validate
          expect( result.valid ).to be_truthy
        end

        it 'should be invalid when nil and table is private' do
          @params[:private] = true
          @params[@key] = nil
          result = subject.validate
          expect( result.valid ).to be_falsey
          expect( result.errors[@key][:not_nil] ).to be_falsey
          expect( result.errors[@key][:not_valid_id] ).to be_falsey
          expect( result.errors[@key][:not_in_group] ).to be_falsey
          expect( result.errors[@key][:not_unique] ).to be_falsey
          expect( result.errors[@key][:missing] ).to be_truthy
        end

        it 'should be invalid' do
          @params[@key] = 6
          result = subject.validate
          expect( result.valid ).to be_falsey
          expect( result.errors[@key][:not_nil] ).to be_falsey
          expect( result.errors[@key][:not_valid_id] ).to be_falsey
          expect( result.errors[@key][:not_in_group] ).to be_truthy
          expect( result.errors[@key][:not_unique] ).to be_falsey
          expect( result.errors[@key][:missing] ).to be_falsey
        end
    
        it 'should be invalid with string' do
          @params[@key] = 'asfasf'
          result = subject.validate
          expect( result.valid ).to be_falsey
          expect( result.errors[@key][:not_nil] ).to be_falsey

          expect( result.errors[@key][:not_valid_id] ).to be_truthy
          expect( result.errors[@key][:not_in_group] ).to be_falsey
          expect( result.errors[@key][:not_unique] ).to be_falsey
          expect( result.errors[@key][:missing] ).to be_falsey
        end

        it 'should be invalid when -5' do
          @params[@key] = -5
          result = subject.validate
          expect( result.valid ).to be_falsey
          expect( result.errors[@key][:not_nil] ).to be_falsey
          expect( result.errors[@key][:not_valid_id] ).to be_truthy
          expect( result.errors[@key][:not_in_group] ).to be_falsey
          expect( result.errors[@key][:not_unique] ).to be_falsey
          expect( result.errors[@key][:missing] ).to be_falsey
        end

        it 'should be invalid when 0' do
          @params[@key] = 0
          result = subject.validate
          expect( result.valid ).to be_falsey
          expect( result.errors[@key][:not_nil] ).to be_falsey
          expect( result.errors[@key][:not_valid_id] ).to be_truthy
          expect( result.errors[@key][:not_in_group] ).to be_falsey
          expect( result.errors[@key][:not_unique] ).to be_falsey
          expect( result.errors[@key][:missing] ).to be_falsey
        end

        it "should be invalid if #{i % 4 + 1} is same as #{i}" do
          @params[@key] = 5
          @params[:"player_#{i % 4 + 1}"] = 5
          result = subject.validate
          expect( result.valid ).to be_falsey
          expect( result.errors[@key][:not_nil] ).to be_falsey
          expect( result.errors[@key][:not_valid_id] ).to be_falsey
          expect( result.errors[@key][:not_in_group] ).to be_falsey
          expect( result.errors[@key][:not_unique] ).to be_truthy
          expect( result.errors[@key][:missing] ).to be_falsey
        end
      end
    end
  end
end
