require 'rails_helper'

describe RegistrationValidator do
  subject{ RegistrationValidator.new @attributes, @dependencies }

  let(:user_repository) { TestRepository::UserRepository.new }

  before :each do
    @dependencies = {
      user_repository: user_repository
    }
    user_repository.load_by_email_return = nil
    @attributes = {
      email: 'test@example.com',
      first_name: 'Mariusz',
      last_name: 'Kowalski',
      password: 'PAssWOrd1234'
    }
  end

  it 'should be valid' do
    subject.validate
    expect( subject.validate ).to be_truthy
  end
  
  it 'should be invalid with not unique email' do
    user_repository.load_by_email_return = User.new
    expect( subject.validate ).to be_falsey
    expect( subject.errors.messages[:email].join ).to match /this email address is already in use/
  end

  [
    [:first_name, 'a', 'is too short'],
    [:first_name, 'a'*1001, 'is too long'],
    [:last_name, 'a', 'is too short'],
    [:last_name, 'a'*1001, 'is too long'],
    [:password, 'a', 'is too short'],
    [:password, 'a'*1001, 'is too long'],
    [:email, 'sfdasfdsa', 'is not valid']
  ].each do |key, value, message|
    it "should be invalid '#{key}' '#{message}'" do
      @attributes[key] = value
      expect( subject.validate ).to be_falsey
      expect( subject.errors.messages[key].join ).to match /#{message}/
    end
  end
end
