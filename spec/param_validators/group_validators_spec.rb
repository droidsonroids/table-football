require 'rails_helper' 

describe GroupParamValidators::GroupValidator, type: :validator do
  subject{ GroupParamValidators::GroupValidator.new @attributes, @dependencies }

  let(:group_repository) { TestRepository::GroupRepository.new true }

  before :each do
    @dependencies = {
      group_repository: group_repository
    }
    @attributes = {
      id: nil,
      name: 'name',
      number_of_tables: 5
    }
  end

  it 'should be valid' do
    expect( subject.validate ).to be_truthy
  end
 
  it 'should be invalid with to many params' do
    @attributes[:test] = 'test'
    expect( subject.validate ).to be_falsey
    expect( subject.errors.full_messages.join ).to match /Keys test should be removed./
  end

  it 'should be invalid with missing param' do
    @attributes.delete :name
    expect( subject.validate ).to be_falsey
    expect( subject.errors.full_messages.join ).to match /Keys name are missing./
  end

  it 'should be invalid with not unique name' do
    @dependencies = {
      group_repository: TestRepository::GroupRepository.new(false)
    }
    expect( subject.validate ).to be_falsey
    expect( subject.errors.full_messages.join ).to match /Name is not unique./
  end
end
