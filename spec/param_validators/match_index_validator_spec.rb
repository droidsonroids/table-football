require 'rails_helper' 

describe MatchIndexValidator, type: :validator do
  subject{ MatchIndexValidator.new @params, @can_access_group_proc }

  before :each do
    @params = { }
    @can_access_group_proc = Proc.new { |_| true }
  end

  it 'should be valid' do
    expect( subject.validate ).to be_empty
  end

  describe 'keys' do
    it 'should be invalid' do
      @params[:test] = 'test'
      expect( subject.validate ).to_not be_empty
    end
  end

  describe 'table_number' do
    it 'should be valid' do
      @params[:table_number] = nil
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:table_number] = '*'
      expect( subject.validate ).to be_empty
    end
 
    it 'should be valid' do
      @params[:table_number] = '1234'
      expect( subject.validate ).to be_empty
    end
 
    it 'should be invalid when not integer' do
      @params[:table_number] = 'safdasfadsf'
      expect( subject.validate ).to_not be_empty
    end
  end

  describe 'group_id' do
    it 'should be valid' do
      @params[:group_id] = nil
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:group_id] = '*'
      expect( subject.validate ).to be_empty
    end
 
 
    it 'should be valid' do
      @params[:group_id] = '1234'
      expect( subject.validate ).to be_empty
    end
 
    it 'should be invalid when not integer' do
      @params[:group_id] = 'safdasfadsf'
      expect( subject.validate ).to_not be_empty
    end

    it 'should be invalid when not integer' do
      @params[:group_id] = '1234'
      @can_access_group_proc = Proc.new { |_| false }
      expect( subject.validate ).to_not be_empty
    end
  end

  describe 'type' do
    it 'should be valid' do
      @params[:type] = nil
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:type] = '1v1'
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:type] = '2v2'
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:type] = 'all'
      expect( subject.validate ).to be_empty
    end

    it 'should be invalid' do
      @params[:type] = 'asdfasfads'
      expect( subject.validate ).to_not be_empty
    end
  end

  describe 'private' do
    it 'should be valid' do
      @params[:private] = 'private'
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:private] = 'public'
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:private] = 'all'
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:private] = nil
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:private] = 'safsafads'
      expect( subject.validate ).to_not be_empty
    end
  end

  describe 'from_time' do
    it 'should be valid' do
      @params[:from_time] = '2000-01-01T12:00:00.00+01:00'
      expect( subject.validate ).to be_empty
    end

    it 'should be invalid' do
      @params[:from_time] = '2000-01sdfds'
      expect( subject.validate ).to_not be_empty
    end

    it 'should be invalid when from time is greater then to time' do
      @params[:from_time] = '2000-01-02T12:00:00.00+01:00'
      @params[:to_time] = '2000-01-01T12:00:00.00+01:00'
      expect( subject.validate ).to_not be_empty
    end
  end

  describe 'to_time' do
    it 'should be valid' do
      @params[:to_time] = '2000-01-01T12:00:00.00+01:00'
      expect( subject.validate ).to be_empty
    end

    it 'should be invalid' do
      @params[:to_time] = '2000-01sdfds'
      expect( subject.validate ).to_not be_empty
    end
  end

  describe 'admin_id' do
    it 'should be valid' do
      @params[:admin_id] = nil
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:admin_id] = '*'
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:admin_id] = '12343242'
      expect( subject.validate ).to be_empty
    end

    it 'should be invalid' do
      @params[:admin_id] = 'sdgfsdg'
      expect( subject.validate ).to_not be_empty
    end
  end

  describe 'player_id' do
    it 'should be valid' do
      @params[:player_id] = nil
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:player_id] = '*'
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:player_id] = '12343242'
      expect( subject.validate ).to be_empty
    end

    it 'should be invalid' do
      @params[:player_id] = 'sdgfsdg'
      expect( subject.validate ).to_not be_empty
    end
  end

  describe 'page' do
    it 'should be valid' do
      @params[:page] = nil
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:page] = '15'
      expect( subject.validate ).to be_empty
    end

    it 'should be invalid' do
      @params[:page] = 'safdsadf'
      expect( subject.validate ).to_not be_empty
    end
  end

  describe 'per_page' do
    it 'should be valid' do
      @params[:per_page] = nil
      expect( subject.validate ).to be_empty
    end

    it 'should be valid' do
      @params[:per_page] = '15'
      expect( subject.validate ).to be_empty
    end

    it 'should be invalid' do
      @params[:per_page] = '101'
      expect( subject.validate ).to_not be_empty
    end

    it 'should be invalid' do
      @params[:per_page] = 'safdsadf'
      expect( subject.validate ).to_not be_empty
    end
  end

end
