require 'rails_helper'

describe ResetPasswordMailer do
  let(:user) { FactoryGirl.create :user, email: 'user@example.com', first_name: 'Maciek', last_name: 'Pietruszka' }

  subject{ ResetPasswordMailer.reset_password user }

  it 'renders the subject' do
    expect( subject.subject ).to eq 'Reset password'
  end

  it 'renders the receiver email' do
    expect( subject.to ).to eq ['user@example.com']
  end

  it 'renders user name' do
    expect( subject.body.encoded ).to match /Hi Maciek Pietruszka,/
  end

  it 'renders link password reset page' do
    expect( subject.body.encoded ).to match %r{http://krzysztofzielonka.pl/reset_password/}
  end
end
