require 'rails_helper'

describe GroupInvitationMailer do
  let(:invitation) { FactoryGirl.create :group_email_invitation, email: 'user@example.com' }
  subject{ GroupInvitationMailer.invitation invitation }

  it 'renders the subject' do
    expect( subject.subject ).to eq 'Group invitation'
  end

  it 'renders the receiver email' do
    expect( subject.to ).to eq [invitation.email]
  end

  it 'renders user name' do
    expect( subject.body.encoded ).to match /Hi user,/
  end

  it 'renders link to invitation page' do
    expect( subject.body.encoded ).to match %r{http://krzysztofzielonka.pl/group_invitations/}
  end
end
