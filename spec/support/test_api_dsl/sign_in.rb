module TestApiDsl
  module SignIn
    def post_auth_sign_in arguments
      body = arguments.to_json
      post '/api/v1/auth/sign_in', body, {'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json'}
      if response.status != 200
        raise Exception.new "sign in error #{response.status} #{response.body}"
      end
      {
        'id' => JSON.parse(response.body)['id'],
        'email' => JSON.parse(response.body)['email'],
        'access-token' => response.headers['access-token'],
        'token-type' => response.headers['token-type'],
        'refresh-token' => response.headers['refresh-token'],
        'client' => response.headers['client'],
        'uid' => response.headers['uid']
      }
    end
  end	
end

RSpec.configure do |c|
  c.include TestApiDsl::SignIn, type: :request
end
