module TestApiDsl
  module ManageGroup
    DEFAULT_ARGUMENTS = {
      name: 'group name', 
      number_of_tables: 5
    }
    def create_group arguments, auth_headers=nil
      arguments = DEFAULT_ARGUMENTS.merge arguments
      headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
      auth_headers ||= post_auth_sign_in({
        email: arguments[:email],
        password: arguments[:password]
      })
      headers.merge! auth_headers
      body = {
        name: arguments[:name],
        number_of_tables: arguments[:number_of_tables]
      }
      post '/api/v1/groups', body.to_json, headers
      JSON.parse response.body
    end

    def add_user_to_group group, user, admin
      headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
      post "/api/v1/groups/#{group['id']}/invite", { emails: [], users: [user['id']] }.to_json, headers.merge(admin)
      post "/api/v1/groups/#{group['id']}/join", {}.to_json, headers.merge(user)
    end
  end	
end

RSpec.configure do |c|
  c.include TestApiDsl::ManageGroup, type: :request
end
