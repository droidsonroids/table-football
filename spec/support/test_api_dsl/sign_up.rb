module TestApiDsl
  module SignUp
    DEFAULT_ARGUMENTS = {
      email: 'test@example.com',
      first_name: 'first name',
      last_name: 'last name',
      password: 'PAssWOrd1234'
    }
 
    def post_auth arguments={}
      arguments = DEFAULT_ARGUMENTS.merge arguments
      body = arguments.to_json
      post '/api/v1/auth/register', body, {'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json'}
      unless response.status == 204
        raise Exception.new "Sign up error: #{response.status} #{response.body}"
      end
    end

    def accept_last_confirmation
      #text = ActionMailer::Base.deliveries.last.body.raw_source
      #token = text.scan(/confirmation_token=([\-A-Za-z0-9]+)/).first.first
      User.last.update_attribute :confirmed_at, Time.now  # TO DO : use bleow method instead
      #get '/auth/confirmation', confing: 'default', confirmation_token: token, redirect_url: 'http://localhost'
    end
  end	
end

RSpec.configure do |c|
  c.include TestApiDsl::SignUp, type: :request
end
