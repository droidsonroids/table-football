module TestApiDsl
  module ManageMatch
    def create_match group_id, arguments, auth_headers=nil
      headers = { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
      headers.merge! auth_headers
  
      post "/api/v1/groups/#{group_id}/matches", arguments.to_json, headers
      if response.status != 200
        raise Exception.new "can not create match #{response.body}"
      end
      JSON.parse response.body
    end
  end
end

RSpec.configure do |c|
  c.include TestApiDsl::ManageMatch, type: :request
end
