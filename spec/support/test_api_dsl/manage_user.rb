module TestApiDsl
  module ManageUser
    DEFAULT_ARGUMENTS = {
      email: 'test@example.com',
      password: 'PAssWOrd1234',
      first_name: 'name',
      last_name: 'name'
    }
    def create_user arguments = {}
      arguments = DEFAULT_ARGUMENTS.merge arguments
      post_auth arguments
      if response.status != 204
        raise TestApiDsl::Errors::ApiError.new response.body
      end
      accept_last_confirmation
      post_auth_sign_in({
        email: arguments[:email],
        password: arguments[:password]
      })
    end
  end	
end

RSpec.configure do |c|
  c.include TestApiDsl::ManageUser, type: :request
end
