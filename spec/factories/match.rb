FactoryGirl.define do
  factory :match do
    name 'match name'
    table_number 1
    start_time Time.now
    duration 15
    private true
    match_type '2v2'
    group
    admin factory: :user
    player_1 factory: :user
    player_2 factory: :user
    player_3 factory: :user
    player_4 factory: :user
    player_1_state 'joined'
    player_2_state 'joined'
    player_3_state 'joined'
    player_4_state 'joined'
    player_1_points_acceptance 'accept'
    player_2_points_acceptance 'accept'
    player_3_points_acceptance 'accept'
    player_4_points_acceptance 'accept'
  end
end
