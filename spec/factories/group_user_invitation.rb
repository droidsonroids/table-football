FactoryGirl.define do
  factory :group_user_invitation do
    group
    user
    created_at Time.now
  end
end
