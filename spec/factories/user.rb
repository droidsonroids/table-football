FactoryGirl.define do
  factory :user do
    provider 'email'
    sequence(:uid) { |i| "user#{i}@example.com" }
    name 'name'
    nickname 'nickname'
    sequence(:email) { |i| "user#{i}@example.com" }
    confirmed_at Time.now
    password 'PAssWOrd1234'
    password_confirmation 'PAssWOrd1234'
  end
end
