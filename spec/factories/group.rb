FactoryGirl.define do
  factory :group do
    sequence(:name) { |i| "Group ##{i}" }
    number_of_tables 5
    admin factory: :user
  end
end
