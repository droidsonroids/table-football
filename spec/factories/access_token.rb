FactoryGirl.define do
  factory :access_token do
    user 
    sequence(:client_id) { |i| "safasd#{i}" }
    sequence(:encrypted_access_token) { |i| BCrypt::Password.create "xxafsdfs#{i}" }
    sequence(:encrypted_refresh_token) { |i| BCrypt::Password.create "xxafsdfs#{i}" }
    expires_at Time.now   
  end
end
