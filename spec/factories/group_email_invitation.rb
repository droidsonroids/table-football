FactoryGirl.define do
  factory :group_email_invitation do
    sequence(:email) { |i| "user#{i}@example.com" }
    sequence(:token) { |i| "token#{i}" }
    last_sent_at Time.now
    group
  end
end
