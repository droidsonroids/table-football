FactoryGirl.define do
  factory :group_membership_request do
    group
    user
    created_at Time.now
  end
end
