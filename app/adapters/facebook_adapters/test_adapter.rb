module FacebookAdapters
  class TestAdapter
    @@raise_auth_error = false
  
    def initialize oauth_access_token
    end

    def get_me
      if @@raise_auth_error
        raise FacebookAdapters::AuthenticationError.new 'test auth error'
      else
        {
          'id' => '100000071744212',
          'email' => 'xxxx@example.com', 
          'first_name' => 'Imie',
          'last_name' => 'Nazwisko'
        } 
      end
    end

    def self.raise_auth_error value = true
      @@raise_auth_error = value
    end
  end
end
