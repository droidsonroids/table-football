module FacebookAdapters
  class KoalaAdapter
    def initialize oauth_access_token
      @graph = Koala::Facebook::API.new(oauth_access_token, @@app_secret)    
    end
 
    def get_me
      @graph.get_object('me', {}, api_version: 'v2.0')
    rescue Koala::Facebook::AuthenticationError => error
      raise FacebookAdapters::AuthenticationError.new error.message
    end

    def self.set_app_secret app_secret
      @@app_secret = app_secret
    end

    private

    def app_secret
      @@app_secret
    end
  end
end
