module GroupInvitationAdapters
  class EmailAdapter
    def send_invitation_email invitation
      GroupInvitationMailer.invitation(invitation).deliver_now! # .deliver_later 
    end
  end
end
