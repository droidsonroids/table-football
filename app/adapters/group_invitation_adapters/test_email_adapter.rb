module GroupInvitationAdapters
  class TestEmailAdapter
    def initialize
      @send_invitation_email_args = []
    end

    attr_reader :send_invitation_email_args

    def send_invitation_email group_email_invitation
      @send_invitation_email_args << group_email_invitation
    end
  end
end
