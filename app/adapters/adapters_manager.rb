require 'singleton'

class AdaptersManager
  include Singleton

  def initialize
    @adapters = {}
  end

  def set key, adapter
    @adapters[key] = adapter
  end

  def get key
    @adapters[key]
  end
end
