module GoogleIdTokenAdapters
  class GoogleAdapter
    def verify token, audience, client_id
      validator = GoogleIDToken::Validator.new
      jwt = validator.check(token, audience, client_id)
      raise AuthError.new "Cannot validate: #{validator.problem}" unless jwt
      jwt
    end
  end
end
