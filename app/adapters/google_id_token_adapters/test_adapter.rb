module GoogleIdTokenAdapters
  class TestAdapter
    def sub 
      "110169484474386276334"
    end

    def verify token, audience, client_id
      {
        "iss": "https://accounts.google.com",
        "sub": sub,
        "azp": "1008719970978-hb24n2dstb40o45d4feuo2ukqmcc6381.apps.googleusercontent.com",
        "email": "billd1600@gmail.com",
        "at_hash": "X_B3Z3Fi4udZ2mf75RWo3w",
        "email_verified": "true",
        "aud": "1008719970978-hb24n2dstb40o45d4feuo2ukqmcc6381.apps.googleusercontent.com",
        "iat": "1433978353",
        "exp": "1433981953"
      }
    end
  end
end
