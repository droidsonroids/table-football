/**
 * @api {get} /api/v1/users/me your profile
 * @apiName GetApiV1UsersMe
 * @apiGroup Users
 * @apiUse AuthHeaders
 *
 * @apiExample {curl} Example usage:
 * curl -X GET \
 *      -H 'Accepts: application/json' \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      http://krzysztofzielonka.pl/api/v1/users/me
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id" : 1,
 *       "first_name" : "Marcin",
 *       "last_name" : "Kowalski",
 *       "email" : "kzielonka@ymail.com",
 *       "groups" : [{
 *           "id" : 1,
 *           "name" : "Droids on Roids",
 *           "number_of_tables" : 1
 *         }, {
 *           "id" : 2,
 *           "name" : "Pub", 
 *           "number_of_tables" : 3
 *         }
 *       ]
 *     }
 */
