/**
 * @api {get} /api/v1/users/unique_email check email uniqueness
 * @apiName GetApiV1UsersUniqueEmail
 * @apiGroup Users
 *
 * @apiParam {String} email email to check
 *
 * @apiExample {curl} Example usage:
 * curl -X GET \
 *      -H 'Content-Type: application/json' \
 *      -H 'Accept: application/json' \
 *      http://krzysztofzielonka.pl/api/v1/users/unique_email?email=test@example.com
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "unique" : true
 *     }
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 400 OK
 *     {
 *       "errors" : ["Email is required."],
 *       "errors_messages" : []
 *     }
 */
