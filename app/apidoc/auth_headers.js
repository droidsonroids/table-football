/**
 * @apiDefine AuthHeaders
 * @apiHeader (Required auth headers) {String} access-token access token
 * @apiHeader (Required auth headers) {String} client client identificator
 * @apiHeader (Required auth headers) {String} token-type type of token (Bearer)
 * @apiHeader (Required auth headers) {String} uid users uid
 */
