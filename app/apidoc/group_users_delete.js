/**
 * @api {delete} /api/v1/groups/:group_id/users/:id remove group member
 * @apiName DeleteApiV1GroupsGroupIdUsersId
 * @apiGroup Groups
 * @apiParam {Integer} group_id group id
 * @apiParam {Integer} id user id
 * @apiUse AuthHeaders
 *
 * @apiExample {curl} Example usage:
 * curl -X DELETE \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      -H 'Content-type: application/json' \
 *      -H 'Accept: application/json' \
 *      http://krzysztofzielonka.pl/api/v1/groups/1/users/1
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 OK
 *
 * @apiSuccessExample Error400-Response:
 *     HTTP/1.1 400 OK
 *     {
 *       error: "You can not remove yourself from the group. You are group admin."
 *     }
 *
 * @apiSuccessExample Error401-Response:
 *     HTTP/1.1 401 OK
 *     {
 *       error: "..."
 *     }
 * 
 * @apiSuccessExample Error403-Response:
 *     HTTP/1.1 403 OK
 *     {
 *       error: "Only group admin can remove user."
 *     }
 *
 * @apiSuccessExample Error404-Response:
 *     HTTP/1.1 404 OK
 *     {
 *       error: "Group doesn't exists."
 *     }
 *
 * @apiSuccessExample Error404-Response:
 *     HTTP/1.1 404 OK
 *     {
 *       error: "User is not member of this group."
 *     }
 */
