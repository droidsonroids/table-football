/**
 * @api {delete} /api/v1/groups/:id/leave leave group/reject invitation
 * @apiName DeleteApiV1GroupsIdLeave
 * @apiGroup Groups
 * @apiUse AuthHeaders
 *
 * @apiExample {curl} Example usage:
 * curl -X DELETE \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      -H 'Accept: application/json' \
 *      http://krzysztofzielonka.pl/api/v1/groups/1/leave
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 OK
 *
 * @apiSuccessExample Error-Response:
 *     HTTP/1.1 400 OK
 *     {
 *       "error" : "You are not member and You are not invited."
 *     }
 *
 * @apiSuccessExample Error-Response:
 *     HTTP/1.1 400 OK
 *     {
 *       "error" : "You are admin of this group."
 *     }
 *
 */
