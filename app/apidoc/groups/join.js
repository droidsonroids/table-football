/**
 * @api {post} /api/v1/groups/:id/join join to group/accept invitation
 * @apiName PostApiV1GroupsIdJoin
 * @apiGroup Groups
 * @apiUse AuthHeaders
 * @apiDescription Use this if user want to join group or user was invited.
 * @apiError (Error401) {String} unauthorized you are not signed in
 *
 * @apiExample {curl} Example usage:
 * curl -X POST \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      -H 'Content-type: application/json' \
 *      -H 'Accept: application/json' \
 *      http://krzysztofzielonka.pl/api/v1/groups/1/join
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 OK
 *
*/
