/**
 * @api {get} /api/v1/groups/invitations invitations
 * @apiName GetApiV1GroupsInvitations
 * @apiGroup Groups
 * @apiUse AuthHeaders
 * @apiError (Error401) {String} unauthorized you are not signed in
 * @apiDescription Returns list of groups to which you have been invited.
 *
 * @apiExample {curl} Example usage:
 * curl -X GET \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      -H 'Content-type: application/json' \
 *      -H 'Accept: application/json' \
 *      http://krzysztofzielonka.pl/api/v1/groups/invitations
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [{
 *       "group_id" : 1,
 *       "group_name" : "Group 1",
 *       "created_at" : "2015-06-22T10:03:59.062Z"
 *     }, {
 *       "group_id" : 2,
 *       "group_name" : "Group 2",
 *       "created_at" : "2015-06-22T10:03:59.062Z"
 *     }]
*/
