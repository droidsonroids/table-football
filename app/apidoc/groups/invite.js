/**
 * @api {post} /api/v1/groups/:id/invite invite users/emails
 * @apiName PostApiV1GroupsIdInvite
 * @apiDescription This api endpoint invites users to group by emails or user ids. It can be also used to accept user request to join group.
 * @apiGroup Groups
 * @apiUse AuthHeaders
 *
 * @apiParam {String[]} emails list of emails
 * @apiParam {Integer[]} users list of users ids
 *
 * @apiExample {curl} Example usage:
 * curl -X POST \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      -H 'Content-type: application/json' \
 *      -H 'Accept: application/json' \
 *      -d '{ "emails" : ["test1@example.com", "test@example.com"], "users" : [1, 2] }' \
 *      http://krzysztofzielonka.pl/api/v1/groups/1/invite
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "emails" : {
 *         "test1@example.com" : {
 *           "status" : "queued_send",
 *         },
 *         "test2@example.com" : {
 *           "status" : "queued_resend"
 *         }
 *       },
 *       "users" : {
 *         1 : { status : "ok" },
 *         2 : { status : "already invited", invited_at: '2015...' },
 *         3 : { status : "user not found" },
 *         4 : { status : "group member" } 
 *       }
 *     }
 *
 * @apiSuccessExample Error-Response (group not found):
 *     HTTP/1.1 404 OK
 *     {
 *       "error" : "Not found"
 *     }
 */
