/**
 * @api {put} /api/v1/groups/:id update group
 * @apiName PutApiV1Groups
 * @apiGroup Groups
 * @apiParam {Integer} id Group id
 * @apiParam {String} name Group name. Must be unique, has length between 3 and 1024.
 * @apiParam {Integer} number_of_tables Number of tables in this group. Must be positive integer including 0.
 * @apiUse AuthHeaders
 * @apiError (Error400) {Array} errors validation errors
 * @apiError (Error401) {String} unauthorized you are not signed in
 * @apiSuccess (Success response) {Integer} id group id
 * @apiSuccess (Success response) {String} name group name
 * @apiSuccess (Success response) {Integer} number_of_tables number of tables in group
 *
 * @apiExample {curl} Example usage:
 * curl -X PUT \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      -H 'Content-type: application/json' \
 *      -H 'Accept: application/json' \
 *      -d '{"name" : "Group A", "number_of_tables": 6}' \
 *      http://krzysztofzielonka.pl/api/v1/groups/1
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *       {
 *         "id" : 123,
 *         "name" : "group name",
 *         "number_of_tables" : 10
 *       }
 * @apiSuccessExample 400-Error-Response:
 *     HTTP/1.1 400 OK
 *       {
 *         "errors" : ["Name invalid."]
 *       }
 */
