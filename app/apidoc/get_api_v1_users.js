/**
 * @api {get} /api/v1/users list of users
 * @apiName GetApiV1Users
 * @apiGroup Users
 * @apiUse AuthHeaders
 *
 * @apiParam {String} query=null Returns only users matching the query. If null returns all users.
 * @apiParam {String} without_me='no' Can be 'yes' or 'no'. If 'yes' excludes signed in user from the results. Default 'no'.
 * @apiParam {Integer} group_id=null Returns only users from group of this group_id. If null returns all users. NOT IMPLEMENTED
 * @apiParam {Integer} page=1 number of page to return
 * @apiParam {Integer} per_page=100 number of users per page
 *
 * @apiExample {curl} Example usage:
 * curl -X GET \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      http://krzysztofzielonka.pl/api/v1/users
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [{
 *       "id" : 1,
 *       "first_name" : "Imie",
 *       "last_name" : "Nazwisko"
 *     },{
 *       "id" : 2,
 *       "first_name" : "Imie 2",
 *       "last_name" : "Nazwisko 2"
 *     }]
 */
