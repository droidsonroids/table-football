/**
 * @api {get} /api/v1/groups/:id get group
 * @apiName GetApiV1GroupsId
 * @apiGroup Groups
 * @apiParam {Integer} id group id
 * @apiUse AuthHeaders
 *
 * @apiExample {curl} Example usage:
 * curl -X GET \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      -H 'Content-type: application/json' \
 *      -H 'Accept: application/json' \
 *      http://krzysztofzielonka.pl/api/v1/groups/1
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id" : 1,
 *       "name" : "gorup name",
 *       "number_of_tables" : 8,
 *       "admin" : {
 *         "id" : 1",
 *         "first_name" : "Imie",
 *         "last_name" : "Nazwisko"
 *       }
 *     }
 */
