/**
 * @api {get} /api/v1/groups/:group_id/requests/:id/reject reject
 * @apiName ReleteApiV1GroupsIdRequestsIdReject
 * @apiGroup Group Requests
 * @apiUse AuthHeaders
 * @apiDescription Group admin can reject the request.
 *
 * @apiError GroupNotFound The <code>id</code> of the Group was not found.
 *
 * @apiExample {curl} Example usage:
 * curl -X DELETE \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      -H 'Accept: application/json' \
 *      http://krzysztofzielonka.pl/api/v1/groups/1/requests/2/reject
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 OK
 *
 */
