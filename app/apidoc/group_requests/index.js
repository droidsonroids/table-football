/**
 * @api {get} /api/v1/groups/:id/requests requests
 * @apiName GetApiV1GroupsIdUsersRequests
 * @apiGroup Group Requests
 * @apiUse AuthHeaders
 * @apiDescription Group admin can check who wants to join to group.
 *
 * @apiError GroupNotFound The <code>id</code> of the Group was not found.
 *
 * @apiExample {curl} Example usage:
 * curl -X GET \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      -H 'Accept: application/json' \
 *      http://krzysztofzielonka.pl/api/v1/groups/1/requests
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *       [{
 *         "id" : 1,
 *         "first_name" : "Kamila",
 *         "last_name" : "Nowakowska"
 *       }]
 *
 * @apiSuccessExample Error-Response:
 *     HTTP/1.1 401 OK
 *     {
 *       "error" : "..."
 *     }
 *
 * @apiSuccessExample Error-Response:
 *     HTTP/1.1 403 OK
 *     {
 *       "error" : "..."
 *     }
 *
 */
