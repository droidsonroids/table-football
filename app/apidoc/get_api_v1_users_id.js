/**
 * @api {get} /api/v1/users/:id get user
 * @apiName GetApiV1UsersId
 * @apiGroup Users
 * @apiParam {Integer} id group id
 * @apiUse AuthHeaders
 *
 * @apiExample {curl} Example usage:
 * curl -X GET \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      http://krzysztofzielonka.pl/api/v1/users/1
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id" : 1,
 *       "first_name" : "Marcin",
 *       "last_name" : "Kowalski",
 *       "groups" : [{
 *           "id" : 1,
 *           "name" : "Droids on Roids",
 *           "number_of_tables" : 1
 *         }, {
 *           "id" : 2,
 *           "name" : "Pub", 
 *           "number_of_tables" : 3
 *         }
 *       ]
 *     }
 */
