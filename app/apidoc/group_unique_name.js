/**
 * @api {get} /api/v1/groups/unique_name check group name uniqueness
 * @apiName PostApiV1GroupsUniqueName
 * @apiGroup Groups
 * @apiParam {String} name Group name to validate.
 * @apiError (Error400) {String} errors validation errors
 * @apiError (Error401) {String} unauthorized you are not signed in
 * @apiSuccess (Success response) {Boolean} unique true if name is unique
 * @apiUse AuthHeaders
 *
 * @apiExample {curl} Example usage:
 * curl -X GET \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      -H 'Content-type: application/json' \
 *      -H 'Accept: application/json' \
 *      -d '{"name" : "group name"}' \
 *      http://krzysztofzielonka.pl/api/v1/groups/unique_name
 *
 * @apiSuccessExample Success-Response (unique name):
 *     HTTP/1.1 200 OK
 *       {
 *         "unique" : true
 *       }
 * @apiSuccessExample Success-Response (not unique name):
 *     HTTP/1.1 200 OK
 *       {
 *         "unique" : false
 *       }
 */
