/**
 * @api {get} /api/v1/omniauth/google/callback sign in/sign up using google
 * @apiName ApiV1OmniauthGoogleCallback
 * @apiGroup Auth
 *
 * @apiParam {String} code code from google
 *
 * @apiExample {curl} Example usage:
 * curl -X GET \
 *      -H 'Content-Type: application/json' \
 *      -H 'Accept: application/json' \
 *      http://krzysztofzielonka.pl/api/v1/omniauth/google?code=safadsfasfdsaaf
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     access-token: wc3nxr9npzbq56eo7sMth
 *     refresh-token: wc3nxr9npzbq56eo7sMth
 *     token-type: Bearer
 *     client: gK7_ZPbiFaOY1lM0kkeLL
 *     expiry: 143497891
 *     uid: test@example.co
 *       {
 *         "id" : 1,
 *         "email" : "test@example.com",
 *         "first_name" : "Mariusz",
 *         "last_name" : "Mariuszewski"
 *        }
 *
 * @apiSuccessExample Error-Response:
 *     HTTP/1.1 401 OK
 */ 
