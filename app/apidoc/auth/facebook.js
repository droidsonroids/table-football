/**
 * @api {post} /api/v1/auth/facebook sign in (facebook)
 * @apiName PostApiV1AuthFacebook
 * @apiGroup Auth
 *
 * @apiParam {String} access_token facebooks oauth access token
 *
 * @apiExample {curl} Example usage:
 * curl -X POST \
 *      -d '{ "access_token" : "asfasfads" }' \
 *      -H 'Content-Type: application/json' \
 *      -H 'Accept: application/json' \
 *      http://krzysztofzielonka.pl/api/v1/auth/facebook
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     access-token: wc3nxr9npzbq56eo7sMt
 *     refresh-token: wc3nxr9npzbq56eo7sMth
 *     token-type: Bearer
 *     client: gK7_ZPbiFaOY1lM0kkeLL
 *     expiry: 143497891
 *     {
 *       "id" : 8,
 *       "email" : "kzielonka@ymail.com",
 *       "first_name" : "Krzysztof",
 *       "last_name" : "Zielonka",
 *       "auth" : {
 *         "access_token" : "ttYhFoXX5jqqhtu5jje6pw",
 *         "refresh_token" : "A8aaoRAipBTv2ztAo30dtQ",
 *         "token_type" : "Bearer",
 *         "client" : "eVdmcrCGJAsEBXh5PuUpug",
 *         "expiry" : "1435834080",
 *         "uid":"kzielonka@ymail.com"
 *       }
 *     }
 *
 * @apiSuccessExample Error-Response-401:
 *     HTTP/1.1 401 OK
 *       {
 *         "error" : ...
 *       }
 */
