/**
 * @api {post} /api/v1/auth/refresh_token refresh token
 * @apiName PostApiV1AuthRefreshToken
 * @apiGroup Auth
 *
 * @apiParam {String} refresh_token refresh token
 *
 * @apiExample {curl} Example usage:
 * curl -X POST \
 *      -d '{"refresh_token" : "sdgsfdgsfdg"}' \
 *      -H 'Content-Type: application/json' \
 *      -H 'Accept: application/json' \
 *      -H 'client: ssafasdfadsfdsf' \
 *      -H 'uid: test@example.com' \
 *      http://krzysztofzielonka.pl/api/v1/auth/refresh_token
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     access-token: wc3nxr9npzbq56eo7sMt
 *     refresh-token: wc3nxr9npzbq56eo7sMth
 *     token-type: Bearer
 *     client: gK7_ZPbiFaOY1lM0kkeLL
 *     expiry: 143497891
 *     {
 *       "id" : 8,
 *       "email" : "kzielonka@ymail.com",
 *       "first_name" : "Krzysztof",
 *       "last_name" : "Zielonka",
 *       "auth" : {
 *         "access_token" : "ttYhFoXX5jqqhtu5jje6pw",
 *         "refresh_token" : "A8aaoRAipBTv2ztAo30dtQ",
 *         "token_type" : "Bearer",
 *         "client" : "eVdmcrCGJAsEBXh5PuUpug",
 *         "expiry" : "1435834080",
 *         "uid":"kzielonka@ymail.com"
 *       }
 *     }
 *
 * @apiSuccessExample Error-Response:
 *     HTTP/1.1 401 OK
 *       {
 *         "message" => "Invalid refresh token."
 *       }
 *
 */
