/**
 * @api {post} /api/v1/auth/register register new user
 * @apiName ApiV1Registration
 * @apiGroup Auth
 *
 * @apiParam {String} email users unique email
 * @apiParam {String} password users password
 * @apiParam {String} first_name users first name
 * @apiParam {String} last_name users last name
 *
 * @apiExample {curl} Example usage:
 * curl -X POST \
 *      -d '{"email" : "test@example.com", "password" : "PAssWOrd1234", "first_name" : "first name", "last_name" : "last name"}' \
 *      -H 'Content-Type: application/json' \
 *      -H 'Accept: application/json' \
 *      http://krzysztofzielonka.pl/api/v1/auth/register
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 OK
 *
 * @apiSuccessExample Error-Response:
 *     HTTP/1.1 400 OK
i*       {
 *         "errors" : "First name is too short (minimum is 2 characters)Last name is too short (minimum is 2 characters)Password is too short (minimum is 8 characters)Email this email is already in use",
 *         "error_messages" : {
 *           "first_name" : ["is too short (minimum is 2 characters)"],
 *           "last_name" : ["is too short (minimum is 2 characters)"],
 *           "password" : ["is too short (minimum is 8 characters)"],
 *           "email" : ["this email is already in use"]
 *         }
 *       }
 */ 
