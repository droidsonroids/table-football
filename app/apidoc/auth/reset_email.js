/**
 * @api {post} /api/v1/auth/reset_password reset password
 * @apiName PostApiV1AuthResetPassword
 * @apiGroup Auth
 *
 * @apiParam {String} email users email
 *
 * @apiExample {curl} Example usage:
 * curl -X POST \
 *      -H 'Content-Type: application/json' \
 *      -d '{"email" : "test@example.com"}' \
 *      http://krzysztofzielonka.pl/api/v1/auth/reset_password
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 OK
 *
 * @apiSuccessExample Error-Response (missing email):
 *     HTTP/1.1 400 OK
 *       {
 *         "error" => "Email is required."
 *       }
 *
 * @apiSuccessExample Error-Response (invalid email):
 *     HTTP/1.1 404 OK
 *       {
 *         "error" => "User not found."
 *       }
 */
