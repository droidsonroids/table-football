/**
 * @api {post} /api/v1/auth/google_id_token sign in (google id token)
 * @apiName PostApiV1AuthGoogleIdToken
 * @apiGroup Auth
 * @apiDescription Sign in user using google id token. If user doesn't exists it creates new one with first name and last name from params.
 *
 * @apiParam {String} id_token id token
 * @apiParam {String} first_name first name
 * @apiParam {String} last_name last name
 *
 * @apiExample {curl} Example usage:
 * curl -X POST \
 *      -d '{"id_token" : "asfasfads", "first_name" : "sfddsafasfdsa", "last_name" : "last name", "email" : "test@example.com" }' \
 *      -H 'Content-Type: application/json' \
 *      -H 'Accept: application/json' \
 *      http://krzysztofzielonka.pl/api/v1/auth/google_id_token
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     access-token: wc3nxr9npzbq56eo7sMt
 *     refresh-token: wc3nxr9npzbq56eo7sMth
 *     token-type: Bearer
 *     client: gK7_ZPbiFaOY1lM0kkeLL
 *     expiry: 143497891
 *     {
 *       "id" : 8,
 *       "email" : "kzielonka@ymail.com",
 *       "first_name" : "Krzysztof",
 *       "last_name" : "Zielonka",
 *       "auth" : {
 *         "access_token" : "ttYhFoXX5jqqhtu5jje6pw",
 *         "refresh_token" : "A8aaoRAipBTv2ztAo30dtQ",
 *         "token_type" : "Bearer",
 *         "client" : "eVdmcrCGJAsEBXh5PuUpug",
 *         "expiry" : "1435834080",
 *         "uid":"kzielonka@ymail.com"
 *       }
 *     }
 *
 * @apiSuccessExample Error-Response-400:
 *     HTTP/1.1 400 OK
 *       {
 *         ...
 *       }
 * 
 * @apiSuccessExample Error-Response-401:
 *     HTTP/1.1 401 OK
 *       {
 *         "message" : "Invalid id token."
 *       }
 *
 *
 */
