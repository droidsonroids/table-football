/**
 * @api {delete} /api/v1/groups/:id deletes gorup
 * @apiName DeleteApiV1Groups
 * @apiGroup Groups
 * @apiParam {Integer} id group id
 * @apiUse AuthHeaders
 *
 * @apiExample {curl} Example usage:
 * curl -X DELETE \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      -H 'Content-type: application/json' \
 *      -H 'Accept: application/json' \
 *      http://krzysztofzielonka.pl/api/v1/groups/2
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 OK
 *
 * @apiSuccessExample Error-Response (group not found):
 *     HTTP/1.1 404 OK
 *       {
 *         "error" : "Not found"
 *       }
 *
 */
