/**
 * @api {post} /api/v1/groups create new group
 * @apiName PostApiV1Groups
 * @apiGroup Groups
 * @apiParam {String} name Group name. Must be unique, has length between 3 and 1024.
 * @apiParam {Integer} number_of_tables Number of tables in this group. Must be positive integer including 0.
 * @apiUse AuthHeaders
 * @apiError (Error400) {Array} errors validation errors
 * @apiError (Error401) {String} unauthorized you are not signed in
 * @apiExample {curl} Example usage:
 * curl -X POST \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      -H 'Content-type: application/json' \
 *      -H 'Accept: application/json' \
 *      -d '{"name" : "Group A", "number_of_tables": 5}' \
 *      http://krzysztofzielonka.pl/api/v1/groups
 *
 * @apiSuccess (Success response) {Integer} id group id
 * @apiSuccess (Success response) {String} name group name
 * @apiSuccess (Success response) {Integer} number_of_tables number of tables in group
 */
