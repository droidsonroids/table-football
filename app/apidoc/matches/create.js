/**
 * @api {post} /api/v1/groups/:id/matches create match
 * @apiName PostApiV1GroupsMathces
 * @apiGroup Matches
 * @apiUse AuthHeaders
 *
 * @apiParam {String} name=null match name, it is not required, if null server will chose name. If have value it must be String between 3 and 100 characters.
 * @apiParam {String} type can be '1v1' or '2v2'
 * @apiParam {Boolean} private is match private
 * @apiParam {Integer} table_number=null number of table, if null serwer will choose first free table
 * @apiParam {Integer} player_1 id of player 1
 * @apiParam {Integer} player_2 id of player 2
 * @apiParam {Integer} player_3 id of player 3
 * @apiParam {Integer} player_4 id of player 4
 * @apiParam {Time} start_time start time
 * @apiParam {Integer} duration duration (in minutes)
 *
 * @apiExample {curl} Example usage:
 * curl -X POST \
 *      -H 'access-token: O39xuj_Hu3VhRFDs4z8VJA' \
 *      -H 'uid: kzielonka@ymail.com' \
 *      -H 'client: RArfZ4CNFtMPoqE35bVq7A' \
 *      -H 'token-type: Bearer' \
 *      -H 'Content-type: application/json' \
 *      -H 'Accept: application/json' \
 *      -d "{\"start_time\" : \"2015-12-15T12:00:00.00+02:00\", \"duration\" : 15, \"private\": false, \"type\": \"2v2\", \"player_1\": 8}" \
 *      http://krzysztofzielonka.pl/api/v1/groups/1/matches
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id" : 1,
 *       "name" : "Match #1",
 *       "group" : {
 *         "id" : 2,
 *         "name" : "Group #2"
 *       },
 *       "admin" : {
 *         "id" : 1,
 *         "first_name" : "name1",
 *         "last_name" : "last1"
 *       },
 *       "player_1" : {
 *         "id" : 1,
 *         "first_name" : "name1",
 *         "last_name" : "last1"
 *       },
 *       "player_2" : {
 *         "id" : 2,
 *         "first_name" : "name2",
 *         "last_name" : "last2"
 *       },
 *       "player_3" : {
 *         "id" : 3,
 *         "first_name" : "name3",
 *         "last_name" : "last3"
 *       },
 *       "player_4" : null,
 *       "type" : "2v2",
 *       "private" : false,
 *       "starts_at" : "2015-...",
 *       "duration" : 15,
 *       "table_number" : 2
 *     }
 */
