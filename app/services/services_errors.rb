module ServicesErrors
  class ValidationError < RuntimeError 
    def initialize validation_errors, json_errors = []
      @validation_errors = validation_errors
      @json_errors = json_errors
      super "Validation error: #{messages}"
    end

    attr_reader :validation_errors, :json_errors

    def messages
      if validation_errors.respond_to? :full_messages
        validation_errors.full_messages.join
      else
        validation_errors.join
      end
    end

    def error_messages
      if validation_errors.respond_to? :messages
        validation_errors.messages
      else
        validation_errors
      end
    end
  end

  class AuthorizationError < RuntimeError; end

  class AccessDeniedError < RuntimeError
    def initialize message = nil
      if message
        super message
      else
        super 'User is not allowed to access that endpoint.'
      end
    end
  end

  class NotFoundError < RuntimeError
    def initialize message='Not found'
      super message
    end
  end
end
