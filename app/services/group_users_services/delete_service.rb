module GroupUsersServices
  class DeleteService
    def initialize user, params, dependencies
      @user = user
      @params = params
      @group_repository = dependencies[:group_repository]
    end

    def delete
      load_group
      authorize
      check_is_member_admin
      load_member
      remove_member
    end

    def load_group
      @group = @group_repository.find @params[:group_id]
    rescue RepositoriesErrors::NotFoundError
      raise ServicesErrors::NotFoundError.new "Group doesn't exist."
    end

    def authorize
      if @group.admin_id != @user.id
        raise ServicesErrors::AccessDeniedError.new "Only gorup admin can remove user."
      end
    end

    def load_member
      @member = @group_repository.load_member @group, @params[:id]
     rescue RepositoriesErrors::NotFoundError
      raise ServicesErrors::NotFoundError.new "User is not member of this group."
    end

    def check_is_member_admin
      if @group.admin_id.to_s == @params[:id]
        raise ServicesErrors::ValidationError.new ['You can not remove yourself from the group. You are group admin.']
      end
    end

    def remove_member
      @group_repository.remove_member @group, @member
    end
  end
end
