module GroupServices
  class MembershipRequestsService < GroupServiceBase
    def initialize user, params, dependencies
      @user = user
      @params = params
      @group_id = @params[:id]
      @dependencies = dependencies
      @group_membership_requests_repository = dependencies[:group_memership_requests_repository]
    end

    def requests
      load_group
      authorize_user!
      @group_membership_requests_repository.load_requests @group
    end
  end
end
