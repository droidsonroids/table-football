module GroupServices
  class LeaveService < GroupServiceBase
    class Error < RuntimeError; end
    class AdminError < RuntimeError; end

    def initialize user, params, dependencies
      @user = user
      @params = params
      @dependencies = dependencies
      @group_repository = dependencies[:group_repository]
      @group_user_invitation_repository = dependencies[:group_user_invitation_repository]
      @group_id = params[:id]
    end
  
    def leave_group
      load_group
      check_is_admin
      removed = remove_invitation
      removed = remove_member unless removed
      raise Error.new 'Member not found' unless removed
    end

    private
  
    def check_is_admin
      raise AdminError.new if @group.admin_id == @user.id
    end

    def remove_member
      @group_repository.load_member @group, @user.id
      @group_repository.remove_member @group, @user
      true
    rescue RepositoriesErrors::NotFoundError
      false
    end

    def remove_invitation
      invitation = @group_user_invitation_repository.load @group, @user.id
      if invitation
        @group_user_invitation_repository.destroy invitation
        true
      else
        false
      end
    end
  end
end
