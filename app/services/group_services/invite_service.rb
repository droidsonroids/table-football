module GroupServices
  class InviteService < GroupServiceBase
    RESULT = Struct.new :emails, :users

    def initialize user, params, dependencies
      @user = user 
      @params = params
      @group_id = @params[:id]
      @dependencies = dependencies
      @group_repository = dependencies[:group_repository]
      @group_email_invitation_repository = dependencies[:group_email_invitation_repository]
      @group_membership_requests_repository = dependencies[:group_membership_requests_repository]
      @user_repository = dependencies[:user_repository]
      @email_adapter = dependencies[:email_adapter]
      @email_response = {}
    end

    def invite
      load_group
      authorize_user!
      @email_response = Hash[emails.map { |email| [email, { status: 'unknown' }] }]
      @users_response = nil
      create_email_invitations
      create_user_invitations
      RESULT.new @email_response, @users_response
    end

    private

    attr_reader :group_repository, :group_email_invitation_repository, :user_repository, :email_adapter,
      :group_membership_requests_repository

    def create_email_invitations
      invitations = group_email_invitation_repository.load @group, emails
      new_invitations = []
      emails.each do |email|
        if invitations.find_index { |invitation| invitation.email == email }.nil?
          new_invitations << group_email_invitation_repository.create(@group, email)
        end
      end 
      invitations.each &send_email(@email_response)
      new_invitations.each &send_email(@email_response, true)
    end
 
    def create_user_invitations
      @users_response = Hash[user_ids.map { |user| [user, { status: nil }] }]

      group_repository.group_members(@group, user_ids).each do |user|
        @users_response[user.id][:status] = 'is member'
      end

      requests = group_membership_requests_repository.load_requests @group, user_ids
      requests_hash = Hash[requests.map { |request| [request.user_id, request] }]

      users = user_repository.load_users user_ids
      users.each do |user|
        next unless @users_response[user.id][:status].nil?
        if request = requests_hash[user.id]
          group_repository.add_member @group, user
          request.destroy
          @users_response[user.id][:status] = 'accepted request'
        else
          begin
            @group.user_invitations.create! user: user
            @users_response[user.id][:status] = 'ok'
          rescue ActiveRecord::RecordNotUnique
            @users_response[user.id][:status] = 'already invited'
          end
        end
      end
 
      @users_response.each do |_, value|
        value[:status] = 'user not found' if value[:status].nil?
      end
    end

    def send_email email_response, force=false
      Proc.new do |invitation|
        if force
          email_adapter.send_invitation_email invitation
          @email_response[invitation.email][:status] = :queued_send
        elsif invitation.resend?
          email_adapter.send_invitation_email invitation
          @email_response[invitation.email][:status] = :queued_resend
        else
	  @email_response[invitation.email][:status] = :not_queued
	  @email_response[invitation.email][:last_sent_at] = invitation.last_sent_at
        end
      end
    end

    def emails
      @params[:emails] || []
    end

    def user_ids
      @params[:users] || []
    end
  end
end
