module GroupServices
  class ShowService < GroupServiceBase
    def initialize user, group_id, dependencies
      @user = user
      @group_id = group_id
      @dependencies = dependencies
    end

    def group
      load_group
      authorize_user!
      @group
    end
  end
end
