module GroupServices
  class RejectRequestService < GroupServiceBase
    def initialize user, params, dependencies
      @user = user
      @params = params
      @group_id = params[:group_id]
      @dependencies = dependencies
    end

    def reject
      load_group
      authorize_user!
      load_request
      @request.destroy
    end

    private

    def load_request
      @request = @dependencies[:group_memership_requests_repository].load_request_for_user(@group, @params[:id])
    rescue RepositoriesErrors::NotFoundError
      raise ServicesErrors::NotFoundError.new "Request not found."
    end
  end
end
