require 'google_api'

module GroupServices
  class AcceptEmailInvitationService
    ResultConfirm = Struct.new :user, :invitation
    ResultAccepted = Struct.new :user, :invitation
    ResultAlreadyInGroup = Struct.new :user, :group
    Result = Struct.new :sign_in_failed, :sign_in_email, :sign_up_email, :sign_up_first_name, :sign_up_last_name, :sign_up_errors, :google_sign_in_url

    def initialize user, params, dependencies
      @user = user
      @params = params
      @dependencies = dependencies
      @user_repository = dependencies[:user_repository]
    end
   
    attr_reader :user_repository

    def accept
      result = Result.new false, nil
      result.google_sign_in_url = GoogleApi.instance.auth_uri('auth/omniauth/google/callback')
      load_invitation
      if @user and @params[:accept] == 'yes'
        return accept_invitation @user
      elsif @user
        return ResultConfirm.new @user, @invitation
      else
        if @params[:commit] == 'Sign in'
          result.sign_in_email = @params[:email]
          user = user_repository.load_by_email @params[:email], only_confirmed: true
          if user and user.valid_password?(@params[:password])
            return accept_invitation user
          else
            result.sign_in_failed = true 
          end
        elsif @params[:commit] == 'Sign up'
          result.sign_up_email = @params[:email]
          result.sign_up_first_name = @params[:first_name]
          result.sign_up_last_name = @params[:last_name]
          validator = RegistrationValidator.new(@params, {
            user_repository: user_repository
          }, { 
            confirm_password: true
          })
          if validator.validate
            user = user_repository.create({
              uid: @params[:email],
              provider: 'email',
              email: @params[:email],
              password: @params[:password],
              first_name: @params[:first_name],
              last_name: @params[:last_name],
              confirmed_at: Time.now
            })
            return accept_invitation user
          else
            result.sign_up_errors = validator.errors
          end
        end
      end
      result
    end

    private 

    def accept_invitation user
      @invitation.destroy
      if @invitation.group.users.exists? user.id
        return ResultAlreadyInGroup.new user, @invitation.group
      else
        @invitation.group.users << user
        return ResultAccepted.new user, @invitation
      end 
    end

    def load_invitation
      @invitation = GroupEmailInvitation.find_by_token @params[:token]
      raise ServicesErrors::NotFoundError.new if @invitation.nil?
    end
  end
end
