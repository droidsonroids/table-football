module GroupServices
  class UpdateService
    include GroupHelper

    def initialize user, group_id, data, dependencies
      @user = user
      @group_id = group_id
      @data = data
      @dependencies = dependencies
      @group_repository = dependencies[:group_repository]
    end

    def update_group
      load_group @group_id
      authorize_user!
      validate_data!
      @group.update_attributes!({
        name: @data[:name],
        number_of_tables: @data[:number_of_tables]
      })
      @group
    end

    private

    attr_reader :group_repository

    def parameters_validator
      @parameters_validator ||= GroupParamValidators::GroupValidator.new @data.merge({id: @group_id}), @dependencies
    end

    def validate_data!
      unless parameters_validator.validate
        errors = parameters_validator.errors
        raise ServicesErrors::ValidationError.new errors
      end
    end

    def authorize_user!
      unless @group.admin == @user
        raise ServicesErrors::AccessDeniedError.new
      end
    end
  end
end
