module GroupServices
  class UniqueNameService
    def initialize data, dependencies
      @data = data
      @dependencies = dependencies
    end

    def unique_name?
      validate_data!
      group_repository.name_unique? @data[:name]
    end

    private

    def group_repository
      @dependencies[:group_repository]
    end

    def parameters_validator
      @parameters_validator ||= GroupParamValidators::UniqueNameValidator.new @data
    end

    def validate_data!
      unless parameters_validator.validate
        errors = parameters_validator.errors
        raise ServicesErrors::ValidationError.new errors
      end
    end
  end
end
