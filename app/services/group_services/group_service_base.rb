module GroupServices
  class GroupServiceBase
    protected

    def load_group
      @group = group_repository.find(@group_id)
    rescue RepositoriesErrors::NotFoundError
      raise ServicesErrors::NotFoundError.new
    end

    def authorize_user!
      unless @group.admin == @user
        raise ServicesErrors::AccessDeniedError.new
      end
    end

    def group_repository
      @dependencies[:group_repository]
    end
  end
end
