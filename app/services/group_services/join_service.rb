module GroupServices
  class JoinService < GroupServiceBase
    class DuplicatedMemberError < RuntimeError; end
 
    def initialize user, params, dependencies
      @user = user
      @params = params
      @group_id = params[:id]
      @dependencies = dependencies
      @group_repository = dependencies[:group_repository]
      @group_user_invitation_repository = dependencies[:group_user_invitation_repository]
      @group_membership_requests_repository = dependencies[:group_membership_requests_repository]
    end

    def join_group
      load_group
      if load_group_user_invitation
        group_repository.add_member @group, @user
        group_user_invitation_repository.destroy @group_user_invitation
        :added
      else
        group_membership_requests_repository.create @group, @user
        :requseted
      end
    rescue RepositoriesErrors::NotUniqueError
      raise DuplicatedMemberError.new
    end

    private

    attr_reader :group_repository, :group_user_invitation_repository, 
      :group_membership_requests_repository

    def load_group_user_invitation
      @group_user_invitation = group_user_invitation_repository.load @group, @user.id
    end
  end
end
