module GroupServices
  class InvitationsService
    def initialize user, params, dependencies
      @user = user
      @params = params
      @dependencies = dependencies
    end
 
    def invitations
      @user.group_invitations.includes(:group)
    end
  end
end
