module GroupServices
  class IndexService
    def initialize dependencies
      @dependencies = dependencies
    end

    def groups
      group_repository.all_with_admins
    end

    private

    def group_repository
      @dependencies[:group_repository]
    end

    def parameters_validator
      @parameters_validator ||= GroupParamValidators::GroupValidator.new @data.merge({id: nil}), @dependencies
    end
  end
end
