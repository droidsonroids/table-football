module GroupServices
  class DestroyService
    def initialize user, group_id, dependencies
      @user = user
      @group_id = group_id
      @dependencies = dependencies
    end

    def destroy_group
      load_group
      authorize_user!
      @group.destroy!
      @group
    end

    private

    def group_repository
      @dependencies[:group_repository]
    end

    def load_group
      @group = group_repository.find(@group_id)
    rescue RepositoriesErrors::NotFoundError
      raise ServicesErrors::NotFoundError.new
    end

    def authorize_user!
      unless @group.admin == @user
        raise ServicesErrors::AccessDeniedError.new
      end
    end
  end
end
