module GroupServices
  class CreateService
    def initialize user, data, dependencies
      @user = user
      @data = data || {}
      @dependencies = dependencies
    end

    def create_group
      validate_data!
      group_repository.create(@user, {
        name: @data[:name],
        number_of_tables: @data[:number_of_tables],
        admin: @user
      })
    end

    private

    def group_repository
      @dependencies[:group_repository]
    end

    def parameters_validator
      @parameters_validator ||= GroupParamValidators::GroupValidator.new @data.merge({id: nil}), @dependencies
    end

    def validate_data!
      unless parameters_validator.validate
        errors = parameters_validator.errors
        raise ServicesErrors::ValidationError.new errors
      end
    end
  end
end
