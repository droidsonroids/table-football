module GroupServices
  module GroupHelper
    def load_group group_id
      @group = group_repository.find group_id
    rescue RepositoriesErrors::NotFoundError
      raise ServicesErrors::NotFoundError.new
    end
  end
end
