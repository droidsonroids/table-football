module AuthServices
  class RefreshTokenService
    Tokens = Struct.new :access_token, :refresh_token, :client_id, :expiries_at
    Result = Struct.new :user, :tokens

    def initialize uid, client_id, params, dependencies
      @uid = uid
      @client_id = client_id
      @params = params
      @access_token_repository = dependencies[:access_token_repository]
      @user_repository = dependencies[:user_repository]
    end

    def refresh
      validate_params
      load_user
      load_access_token_record
      check_refresh_token
      @tokens = renew_tokens
      Result.new @user, @tokens
    end
 
    private

    attr_reader :access_token_repository, :user_repository

    def load_user
      @user = user_repository.load_by_uid_only @uid 
      if @user.nil?
        raise ServicesErrors::AuthorizationError.new 'User does not exist.'
      end
    end

    def load_access_token_record
      @access_token_record = access_token_repository.find @user, @client_id
      if @access_token_record.nil?
        raise ServicesErrors::AuthorizationError.new 'Invalid refresh token.'
      end
    end      

    def check_refresh_token
      unless @access_token_record.valid_refresh_token? @params[:refresh_token]
        raise ServicesErrors::AuthorizationError.new 'Invalid refresh token.'
      end
    end

    def validate_params
      errors = []
     
      if @params.keys.to_set != ['controller', 'action', 'refresh_token'].to_set
        errors.push "Body must contain only 'refresh_token'."
      elsif not @params[:refresh_token].is_a? String
        errors.push "Refresh token must be a string."
      end

      unless errors.empty?
        raise ServicesErrors::ValidationError.new errors
      end
    end

    def renew_tokens
      access_token = @access_token_record.generate_access_token
      refresh_token = @access_token_record.generate_refresh_token
      @access_token_record.expires_at = 1.month.from_now
      @access_token_record.save!
      Tokens.new access_token, refresh_token, @access_token_record.client_id, @access_token_record.expires_at 
    end
  end
end
