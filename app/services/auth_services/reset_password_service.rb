module AuthServices
  class ResetPasswordService
    def initialize params, dependencies
      @params = params
      @user_repository = dependencies[:user_repository]
    end

    def reset_password
      validate_params
      user = @user_repository.load_by_email @params[:email]
      if user.nil?
        raise ServicesErrors::NotFoundError.new 'User not found.'
      else
        ResetPasswordMailer.reset_password(user).deliver_now!
      end
    end

    private

    def validate_params
      if @params[:email].nil?
        raise ServicesErrors::ValidationError.new ['Email is required.']
      end
    end
  end
end
