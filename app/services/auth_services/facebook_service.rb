module AuthServices
  class FacebookService
    include Tokens

    class AuthenticationError < RuntimeError; end

    Result = Struct.new :user, :tokens

    def initialize params, dependencies
      @params = params
      @facebook_adapter = dependencies[:facebook_adapter]
      @user_repository = dependencies[:user_repository]
      @access_token_repository = dependencies[:access_token_repository]
    end
   
    def auth
      auth_facebook
      find_or_create_user
      create_headers
      Result.new @user, @tokens
    end

    private

    def find_or_create_user
      @user = @user_repository.load_by_uid :facebook, @me['id']
      if @user.nil?
        password = SecureRandom.urlsafe_base64(nil, false)
        @user = @user_repository.create({
          provider: 'facebook',
          uid: @me['id'],
          email: @me['email'],
          first_name: @me['first_name'],
          last_name: @me['last_name'],
          confirmed_at: Time.now,
          password: password
        })
      end
    end

    def create_headers
      @tokens = @access_token_repository.create @user, {
        apple_bundle_id: @params[:apple_bundle_id],
        device_token: @params[:device_token]
      }
    end

    def auth_facebook
      @me = @facebook_adapter.new(@params[:access_token]).get_me 
    rescue FacebookAdapters::AuthenticationError => error
      raise AuthenticationError.new error.message
    end
  end
end
