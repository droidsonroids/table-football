module AuthServices
  module Tokens
    def generate_client_id
      SecureRandom.urlsafe_base64(nil, false)
    end

    def generate_tokens client_id
      token = SecureRandom.urlsafe_base64(nil, false)
      refresh_token = SecureRandom.urlsafe_base64(nil, false)
      @user.tokens[client_id] ||= {}
      @user.tokens[client_id]['token'] = BCrypt::Password.create token
      @user.tokens[client_id]['refresh_token'] = BCrypt::Password.create refresh_token
      @user.tokens[client_id]['expiry'] = (Time.now + DeviseTokenAuth.token_lifespan).to_i
      {
        'access-token' =>  token,
        'refresh-token' => refresh_token,
        'type' => 'Bearer',
        'client' => client_id,
        'expiry' => @user.tokens[client_id]['expiry'].to_s,
        'uid' => @user.uid
      }
    end
  end
end
