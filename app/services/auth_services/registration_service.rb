module AuthServices
  class RegistrationService
    class MissingConfirmUrlError < RuntimeError; end
    class AlreadySignedUpError < RuntimeError; end
 
    def initialize data, dependencies
      @data = data
      @dependencies = dependencies
    end

    def register
      #raise AlreadySignedUpError.new if user and user.confirmed?
        
      validate!
      user = @dependencies[:user_repository].load_by_email(@data[:email])
      if user.nil?
        user = @dependencies[:user_repository].create({
          email: @data[:email],
          first_name: @data[:first_name],
          last_name: @data[:last_name],
          password: @data[:password],
          provider: 'email',
          uid: @data['email']
        })
      else
        user.update_attributes({
          first_name: @data[:name],
          last_name: @data[:last_name],
          password: @data[:password],
          uid: @data['uid']
        })
      end
      user.send_confirmation_instructions({
        redirect_url: redirect_url
      })
      user
    end

    private

    def validate!
      validator = RegistrationValidator.new @data, @dependencies
      unless validator.validate
      	raise ServicesErrors::ValidationError.new validator.errors
      end
    end

    def user_email
      if User.case_insensitive_keys.include?(:email)
        @data[:email].downcase
      else
        @data[:email]
      end
    end

    def redirect_url
      redirect_url = DeviseTokenAuth.default_confirm_success_url
    end
  end
end
