module AuthServices
  class AcceptResetPasswordService
    Result = Struct.new :status, :token, :errors

    def initialize params, dependencies
      @params = params
      @dependencies = dependencies
      @user_repository = dependencies[:user_repository]
    end

    def reset
      if load_user
        if @user.reset_password_sent_at.nil? or @user.reset_password_sent_at < Time.now - 1.week
          Result.new :not_found, nil, nil
        elsif @params[:commit] == 'Submit'
          unless validator.valid?
            Result.new :invalid, @params[:token], validator.errors
          else
            reset_password
            Result.new :reset, nil, nil
          end
        else
          Result.new :found, @params[:token], nil
        end
      else
        Result.new :not_found, nil, nil
      end
    end

    private

    def load_user
      @user = @user_repository.load_by_reset_password_token @params[:token]
    end

    def validator
      @validator ||= ResetPasswordValidator.new @params
    end

    def reset_password
      @user.update_attributes!({
        reset_password_token: nil,
        reset_password_sent_at: nil,
        password: @params[:password]
      })
    end
  end
end
