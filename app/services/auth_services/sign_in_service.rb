module AuthServices
  class SignInService
    class AuthorizationError < RuntimeError; end

    Result = Struct.new :user, :headers, :tokens
 
    def initialize params, dependencies
      @params = params
      @access_token_repository = dependencies[:access_token_repository]
    end

    def sign_in
      validate_params
      @user = User.where(provider: 'email', uid: @params[:email]).limit(1).first
      if @user and @user.valid_password?(@params[:password]) and @user.confirmed?
        @user.save!
        tokens = access_token_repository.create @user, {
          apple_bundle_id: @params[:apple_bundle_id],
          device_token: @params[:device_token]
        }
        Result.new @user, {}, tokens
      elsif @user and @user.valid_password?(@params[:password])
        raise AuthorizationError.new 'User is not confirmed.'
      else
        raise AuthorizationError.new 'Email or password are not correct.'
      end
    end

    private
 
    attr_reader :access_token_repository

    def validate_params
      errors = []

      unless [:action, :controller, :sign_in, :email, :password].collect(&:to_s).to_set == (@params.keys - ['apple_bundle_id', 'device_token']).to_set
        errors.push 'Email and password are required.'
      end
 
      unless @params[:email].is_a? String
        errors.push 'Email must be a string.'
      end

      unless @params[:password].is_a? String
        errors.push 'Password must be a string.'
      end

      if @params[:apple_bundle_id]
        if @params[:device_token].nil?
          errors.push 'Device token is required when apple bundle id is present.'
        end

        if not @params[:apple_bundle_id].is_a? String
          errors.push 'Apple bundle id must be a string.'
        elsif @params[:apple_bundle_id].size > 1000
          errors.push 'Apple bundle id can not be longer then 1000.'
        end
      end
 
      if @params[:device_token]
        if @params[:apple_bundle_id].nil?
          errors.push 'Apple bundle id is required when device token is present.'
        end

        if not @params[:device_token].is_a? String
          errors.push 'Device token must be a string.'
        elsif @params[:device_token].size > 1000
          errors.push 'Device token can not be longer then 1000.'
        end
      end
   
      unless errors.empty?
        raise ServicesErrors::ValidationError.new errors
      end
    end
  end
end
