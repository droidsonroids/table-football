module AuthServices
  class GoogleIdKeyService
    class AuthError < RuntimeError; end

    Result = Struct.new :user, :tokens

    @@clients = nil

    def initialize params, dependencies
      @params = params
      @dependencies = dependencies 
      @google_adapter = dependencies[:google_adapter]
      @user_repository = dependencies[:user_repository]
      @access_token_repository = dependencies[:access_token_repository]
    end

    attr_reader :google_adapter

    def authenticate
      jwt = nil
      clients.each do |name, client_id|
        begin
          jwt = @google_adapter.verify @params[:id_token], client_id, nil #"526907992446-5j4slde1ium23i07qe8ab21gh5rg3nji.apps.googleusercontent.com"
        rescue GoogleIdTokenAdapters::AuthError
        end
      end
  
      raise AuthError.new if jwt.nil?

      @user = @user_repository.load_by_uid 'google', jwt_sub(jwt)
      unless @user
        validate!
        @user = @user_repository.create user_attributes(jwt)
      end
      @user.save!
      @tokens = @access_token_repository.create @user, {}
      Result.new @user, @tokens
    end

    private
   
    def validate!
      @validator ||= RegistrationValidator.new @params, @dependencies, skip_password: true, skip_email_uniqueness: true
      unless @validator.validate
        raise ServicesErrors::ValidationError.new @validator.errors
      end
    end

    def jwt_sub jwt
      jwt[:sub] || jwt['sub']
    end

    def user_attributes jwt
      password = SecureRandom.urlsafe_base64(nil, false)
      {
        provider: 'google',
        uid: jwt_sub(jwt),
        email: @params[:email],
        first_name: @params[:first_name],
        last_name: @params[:last_name],
        password: password,
        confirmed_at: Time.now
      }
    end

    def clients
      return @@clients if @@clients
      @@clients = YAML.load_file "#{Rails.root}/config/google_id_token.yml"
    end
  end
end
