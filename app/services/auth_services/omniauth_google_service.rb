require 'google_api'

module AuthServices
  class OmniauthGoogleService
    include Tokens

    PROVIDER_NAME = 'google'

    Result = Struct.new :user, :headers

    class AuthError < RuntimeError; end

    def initialize code, options = {}
      @code = code
      @options = options
    end

    def authorize
      load_google_user
      find_user
      new_user unless @user
      @client_id = generate_client_id
      generate_tokens @client_id
      @user.skip_confirmation!
      headers = generate_tokens(@client_id)
      @user.save!
      Result.new @user, headers
    end

    def load_google_user
      @google_user = GoogleApi.instance.user_data @code, @options[:redirect_path]
    rescue GoogleApi::AuthError => error
      raise AuthError.new error.message
    end

    def find_user
      @user = User.where(provider: PROVIDER_NAME, uid: @google_user.id).limit(1).first
    end

    def new_user
      password = SecureRandom.urlsafe_base64(nil, false)
      @user = User.new({
        provider: PROVIDER_NAME,
        uid: @google_user.id,
        password: password,
        password_confirmation: password,
        email: @google_user.email,
        first_name: @google_user.first_name,
        last_name: @google_user.last_name,
        confirmed_at: Time.now
      })
    end
  end
end
