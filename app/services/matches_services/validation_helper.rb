module MatchesServices
  class ValidationHelper
    TIME_EPSILON = 1.minute
    
    class ValidationErrors < RuntimeError
      def initialize validation_result
        @validation_result = validation_result
        super 'validation error'
      end
   
      attr_reader :validation_result

      def validation_errors
        validation_result.errors
      end

      def errors_list
        validation_result.json_errors
      end
    end

    def initialize match, user, group, params, dependencies
      @match = match
      @user = user
      @group = group
      @params = params
      @group_repository = dependencies[:group_repository]
      @match_repository = dependencies[:match_repository]
    end

    def validate!
      @validator ||= MatchValidator.new(@params, @user.id, @group.number_of_tables, match_started, player_in_group_proc, time_past_proc, time_collision_proc)
      result = @validator.validate
      unless result.valid
        raise ValidationErrors.new result
      end
    end

    private

    attr_reader :group_repository, :match_repository

    def match_started
      false
    end

    def player_in_group_proc
      ids = [@params[:player_1], @params[:player_2], @params[:player_3], @params[:player_4]].compact
      members_ids = group_repository.group_members(@group, ids).collect(&:id)
      Proc.new { |id| members_ids.include? id }
    end

    def time_past_proc
      Proc.new { |time| DateTime.parse(time).to_time + TIME_EPSILON < Time.now }
    end

    def time_collision_proc
      Proc.new do |time|
        start_time = DateTime.rfc3339 time
        match_id = @match ? @match.id : nil
        count = match_repository.occupied_tables_count @group.id, start_time, @params[:duration], table_number: @params[:table_number], match_id: match_id
        if @params[:table_number]
          count > 0
        else
          count >= @group.number_of_tables
        end
      end
    end
  end
end
