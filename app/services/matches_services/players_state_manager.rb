module MatchesServices
  class PlayersStateManager
    def initialize params, match_attrs
      @match = match_attrs
      @params = params
    end

    def player_state player_number
      if @params[:type] == '1v1' and player_number >= 3
        'none'
      elsif @params["player_#{player_number}"].nil?
        'none'
      elsif @match[:admin_id] == @params["player_#{player_number}"]
        'joined'
      elsif @match[:"player_#{player_number}_id"] != @params["player_#{player_number}"]
        'invited'
      else
        @match[:"player_#{player_name}_state"]
      end
    end
  end
end
