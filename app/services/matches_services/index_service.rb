module MatchesServices
  class IndexService
    Result = Struct.new :matches, :total, :page, :per_page

    def initialize user, params, dependencies
      @user = user
      @params = params
      @match_repository = dependencies[:match_repository]
      @user_repository = dependencies[:user_repository]
    end

    def matches
      validate_params
      matches = match_repository.find_matches_paginated @user, group_id, table_number, type, private, from_time, to_time, admin_id, player_id, offset, per_page
      count = match_repository.find_matches_count @user, group_id, table_number, type, private, from_time, to_time, admin_id, player_id
      Result.new matches, count, page, per_page
    end
   
    private
 
    attr_reader :match_repository, :user_repository

    def validate_params
      errors = MatchIndexValidator.new(@params, can_access_group_proc).validate
      unless errors.empty?
        raise ServicesErrors::ValidationError.new errors
      end
    end

    def can_access_group_proc
      Proc.new do |group_id| 
        user_repository.is_group_member? @user, group_id  
      end
    end

    def group_id
      @params[:group_id] ? @params[:group_id].to_i : nil
    end

    def table_number
      return nil if @params[:table_number].nil? or @params[:table_number] == '*'
      @params[:table_number]
    end

    def type
      return nil if @params[:type] == 'all' or @params[:type] == '*'
      @params[:type]
    end

    def private
      return nil if @params[:private] == 'all' or @params[:type] == '*'
      @params[:private]
    end

    def from_time
      @params[:from_time] ? DateTime.parse(@params[:from_time]) : nil
    end
 
    def to_time
      @params[:to_time] ? DateTime.parse(@params[:to_time]) : nil
    end

    def admin_id
      return nil if @params[:admin_id] == '*'
      @params[:admin_id] ? @params[:admin_id].to_i : nil
    end
 
    def player_id
      return nil if @params[:player_id] == '*'
      @params[:player_id]
    end

    def page
      @params[:page] ? @params[:page].to_i : 1
    end

    def offset
      (page - 1) * per_page
    end

    def per_page
      @params[:per_page] ? @params[:per_page].to_i : 50
    end

  end
end
