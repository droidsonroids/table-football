module MatchesServices
  class UpdateService
    Error = Struct.new :code, :title, :details

    def initialize user, params, dependencies
      @user = user
      @params = params
      @dependencies = dependencies
      @match_repository = dependencies[:match_repository]
    end

    def update
      load_match
      authorize
      check_is_not_started
      validate!
      update_match
      @match
    end

    private

    attr_reader :params, :match_repository

    def load_match
      @match = match_repository.load_match params[:id]
    rescue RepositoriesErrors::NotFoundError
      raise ServicesErrors::NotFoundError.new 'match not found'
    end

    def authorize
      unless @match.admin_id == @user.id
        raise ServicesErrors::AccessDeniedError.new 'You are not admin of this match.'
      end
    end

    def check_is_not_started
      if @match.start_time < Time.now
        json_errors = [Error.new('update/after start time', 'after start time', 'You can not update match which start time is in the past.')]
        raise ServicesErrors::ValidationError.new [], json_errors
      end
    end

    def validate!
      ValidationHelper.new(@match, @user, @match.group, @params, @dependencies).validate!
    end

    def update_match
      @match.update_attributes({
        name: @params[:name],
        match_type: @params[:type],
        private: @params[:private],
        table_number: table_number,
        duration: @params[:duration],
        start_time: @params[:start_time],
        player_1_id: @params[:player_1],
        player_2_id: @params[:player_2],
        player_3_id: @params[:player_3],
        player_4_id: @params[:player_4],
        player_1_points_acceptance: 'none',
        player_2_points_acceptance: 'none',
        player_3_points_acceptance: 'none',
        player_4_points_acceptance: 'none'
      })
    end

    def table_number
      @params[:table_number] || @match.table_number
    end
  end
end
