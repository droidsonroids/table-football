module MatchesServices
  class CreateService
      
    def initialize user, params, dependencies
      @user = user
      @params = params
      @dependencies = dependencies
      @group_repository = dependencies[:group_repository]
      @match_repository = dependencies[:match_repository]
    end

    def create
      load_group
      authorize_user!
      validate!
      initialize_players_state_manager
      create_match
      @match
    end

    private
    
    attr_reader :group_repository, :match_repository
 
    def load_group
      @group = group_repository.find @params[:id]
    rescue RepositoriesErrors::NotFoundError
      raise ServicesErrors::NotFoundError
    end
   
    def authorize_user!
       group_repository.load_member @group, @user.id
    rescue RepositoriesErrors::NotFoundError
      raise ServicesErrors::AccessDeniedError.new 'You are not member of this gorup.'
    end

    def validate!
      ValidationHelper.new(nil, @user, @group, @params, @dependencies).validate!
    end

    def initialize_players_state_manager
      @players_state_manager ||= PlayersStateManager.new @params, {
        admin_id: @user.id,
        player_1_id: nil,
        player_2_id: nil,
        player_3_id: nil,
        player_4_id: nil
      }
    end

    def create_match
      @match = match_repository.create @group, {
        name: choose_name,
        match_type: @params[:type],
        private: @params[:private],
        table_number: choose_table_number,
        duration: @params[:duration],
        start_time: @params[:start_time],
        admin_id: @user.id,
        player_1_id: @params[:player_1],
        player_2_id: @params[:player_2],
        player_3_id: @params[:player_3],
        player_4_id: @params[:player_4],
        player_1_state: @players_state_manager.player_state(1),
        player_2_state: @players_state_manager.player_state(2),
        player_3_state: @players_state_manager.player_state(3),
        player_4_state: @players_state_manager.player_state(4),
        player_1_points_acceptance: 'none',
        player_2_points_acceptance: 'none',
        player_3_points_acceptance: 'none',
        player_4_points_acceptance: 'none'
      }
    end
 
    def choose_table_number
      return @params[:table_number] if @params[:table_number]
      start_time = DateTime.rfc3339 @params[:start_time]
      table_numbers = match_repository.occupied_tables @group.id, start_time, @params[:duration]
      i = 1
      while i <= @group.number_of_tables
        break unless table_numbers[i - 1] == i
        i += 1
      end
      i
    end

    def choose_name
      return @params[:name] if @params[:name]
      count = match_repository.count @group
      "Match ##{count + 1}"
    end
  end
end
