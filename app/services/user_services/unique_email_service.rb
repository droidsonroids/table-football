module UserServices
  class UniqueEmailService
    def initialize params, dependencies
      @params = params
      @user_repository = dependencies[:user_repository]
    end

    def check
      validate
      !!!@user_repository.load_by_email(@params[:email], only_confirmed: true)
    end

    private

    def validate
      errors = []
      if @params[:email].nil?
        errors << 'Email is required.'
      elsif not @params[:email].is_a? String
        errors << 'Email must be a string.'
      elsif not @params[:email].match User::EMAIL_REGEXP
        errors << 'Email is not valid.'
      end
 
      unless errors.empty?
        raise ServicesErrors::ValidationError.new errors
      end
    end
  end
end
