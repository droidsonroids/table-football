module UserServices
  class ShowService
    def initialize user_id, dependencies
      @user_id = user_id
      @dependencies = dependencies
    end

    def user
      load_user!
      authorize!
      @user
    end

    private

    def load_user!
      @user = @dependencies[:user_repository].find @user_id
    rescue RepositoriesErrors::NotFoundError
      raise ServicesErrors::NotFoundError.new 
    end

    def authorize!
      unless @user.confirmed?
        raise ServicesErrors::AccessDeniedError.new
      end
    end
  end
end
