module UserServices
  class IndexService
    def initialize user, params, dependencies
      @user = user
      @params = params
      @dependencies = dependencies
    end

    def users
      validate!
      @dependencies[:user_repository].all query, without_id, group_id, offset, per_page
    end

    private

    def query
      @params[:query].gsub('_', '\\_').gsub('%', '\\%').gsub(/\s/, '%') if @params[:query]
    end

    def without_me
      @params[:without_me] == 'yes'
    end

    def without_id
      @user.id if without_me
    end

    def group_id
      @params[:group_id] ? @params[:group_id].to_i : nil
    end

    def page
      @params[:page] ? @params[:page].to_i : 1
    end

    def offset
      (page - 1)*per_page
    end

    def per_page
      @params[:per_page] ? @params[:per_page].to_i : 100
    end

    def validate!
      validator = UserIndexValidator.new @params
      unless validator.validate
        raise ServicesErrors::ValidationError.new validator.errors
      end
    end
  end
end
