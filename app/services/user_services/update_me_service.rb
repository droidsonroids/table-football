module UserServices
  class UpdateMeService
    def initialize user, params, dependencies
      @user = user
      @params = params
      @user_repository = dependencies[:user_repository]
      @dependencies = dependencies
    end

    def update_me
      validate!
      @user.update_attributes!({
        first_name: @params[:first_name],
        last_name: @params[:last_name]
      })
      @user 
    end

    def validate!
      validator = UserValidator.new @params
      unless validator.validate
      	raise ServicesErrors::ValidationError.new validator.errors
      end
    end
  end
end
