module DbRepository
  class GroupUserInvitationRepository
    def load group, user_id
      group.user_invitations.find_by_user_id user_id
    end

    def destroy group_user_invitation
      group_user_invitation.destroy
    end
  end
end
