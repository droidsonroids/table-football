module DbRepository
  class MatchRepository
    def create group, attributes
      group.matches.create! attributes 
    end

    def find_matches user, group_id, table_number, type, private, from_time, to_time, admin_id, player_id
      group_ids = user.groups.select(&:id).collect(&:id) 
      group_ids = [group_id] if group_id
      matches = Match.where group_id: group_ids 
      matches = matches.where(match_type: type) if type and type != 'all'
      if private == 'private'
        matches = matches.where(private: true)
      elsif private == 'public'
        matches = matches.where(private: false)
      end
      matches = matches.where('start_time >= ?', from_time) if from_time
      matches = matches.where('start_time <= ?', to_time) if to_time
      matches = matches.where(admin_id: admin_id) if admin_id
      matches = matches.where(table_number: table_number) if table_number
      matches = matches.where('player_1_id=? OR player_2_id=? OR player_3_id=? OR player_4_id=?', player_id, player_id, player_id, player_id) if player_id
      matches
    end

    def find_matches_paginated user, group_id, table_number, type, private, from_time, to_time, admin_id, player_id, offset, per_page
      find_matches(user, group_id, table_number, type, private, from_time, to_time, admin_id, player_id).offset(offset).limit(per_page).order('start_time DESC')
    end

    def find_matches_count user, group_id, table_number, type, private, from_time, to_time, admin_id, player_id
      find_matches(user, group_id, table_number, type, private, from_time, to_time, admin_id, player_id).count
    end

    def find_intersection group_id, start_time, duration, table_number = nil, match_id = nil
      matches = Match.where(group_id: group_id).
        where(%Q{
          (
            start_time <= ? AND 
            ? <= start_time + (duration || ' minutes')::interval
          ) OR (
            ? <= start_time AND start_time <= ?
          )
        }, start_time, start_time, start_time, start_time + duration.minutes)
      matches = matches.where table_number: table_number if table_number
      matches = matches.where 'id != ?' , match_id if match_id
      matches
    end

    def occupied_tables group_id, start_time, duration
      find_intersection(group_id, start_time, duration, nil, nil).select('DISTINCT table_number').order('table_number ASC').collect(&:table_number)
    end

    def occupied_tables_count group_id, start_time, duration, table_number: nil, match_id: nil
      find_intersection(group_id, start_time, duration, table_number, match_id).count 
    end
  
    def count group
      group.matches.count
    end

    def load_match match_id
      Match.find match_id
    rescue ActiveRecord::RecordNotFound
      raise RepositoriesErrors::NotFoundError.new
    end
  end
end
