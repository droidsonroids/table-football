module DbRepository
  class UserRepository
    def load_by_email email, only_confirmed: false
      users = User.where(email: email, provider: 'email')
      users = users.where('confirmed_at IS NOT NULL') if only_confirmed
      users.limit(1).first
    end

    def load_by_uid provider, uid
      User.where(provider: provider, uid: uid).where('confirmed_at IS NOT NULL').limit(1).first
    end

    def load_by_uid_only uid
      User.find_by_uid uid 
    end

    def load_by_reset_password_token token
      User.find_by_reset_password_token token
    end

    def load_users ids
      User.where(id: ids)
    end

    def is_group_member? user, group_id
      user.groups.exists? group_id
    end
 
    def all query, without_id, group_id, offset, limit
      users = User
      users = users.where('id != ?', without_id) if without_id
      users = users.where('(first_name || last_name) ILIKE ?', "%#{query}%") if query
      users = users.joins(:groups).where(groups: { id: group_id }) if group_id
      users.offset(offset).limit(limit).order('first_name ASC, last_name ASC')
    end
 
    def find id
      User.find id
    rescue ActiveRecord::RecordNotFound
      raise RepositoriesErrors::NotFoundError
    end

    def create attributes
      User.create! attributes
    end
  end
end
