module DbRepository
  class AccessTokenRepository
    def create user, attributes
      AccessToken.create_for_user user, attributes
    end

    def find user, client_id
      user.access_tokens.find_by_client_id client_id
    end
  end
end
