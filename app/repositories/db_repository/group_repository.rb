module DbRepository
  class GroupRepository
    def create user, attributes
      user.groups.create! attributes
    end

    def find id
      Group.find id
    rescue ActiveRecord::RecordNotFound
      raise RepositoriesErrors::NotFoundError.new
    end

    def add_member group, user
      group.users << user
    rescue ActiveRecord::RecordNotUnique
      raise RepositoriesErrors::NotUniqueError.new
    end

    def group_members group, ids = nil
      if ids.nil?
        group.users
      else
        group.users.where id: ids
      end
    end

    def load_member group, member_id
      group.users.find member_id
    rescue ActiveRecord::RecordNotFound
      raise RepositoriesErrors::NotFoundError.new
    end
 
    def remove_member group, member
      group.users.delete member
    end
 
    def all_with_admins
      Group.all.includes(:admin)
    end

    def name_unique? name, expect_id=nil
      if expect_id
        !Group.where("id != #{expect_id}").exists?(name: name)
      else
        !Group.exists?(name: name)
      end
    end
  end
end
