module DbRepository
  class GroupMembershipRequestsRepository
    def create group, user
      group.membership_requests.create! user: user
    end

    def load_requests group, ids = nil
      if ids.nil?
        group.membership_requests
      else
        group.membership_requests.where(user_id: ids)
      end
    end

    def load_request_for_user group, user_id
      request = group.membership_requests.where(user_id: user_id).first
      raise RepositoriesErrors::NotFoundError.new unless request
      request
    end
  end
end
