module DbRepository
  class GroupEmailInvitationRepository
    def load group, emails
      group.email_invitations.where email: emails
    end

    def create group, email
      group.email_invitations.create! email: email
    end
  end
end
