module RepositoriesErrors
  class NotFoundError < RuntimeError; end
  class NotUniqueError < RuntimeError; end
end
