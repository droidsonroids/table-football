module TestRepository
  class GroupEmailInvitationRepository
    def initialize
      @create_args = []
    end 
  
    attr_accessor :load_groups
    attr_reader :last_load_group_group_arg
    attr_reader :last_load_group_emails_arg
    attr_reader :create_args

    def load group, emails
      @last_load_group_group_arg = group
      @last_load_group_emails_arg = emails
      @load_groups
    end

    def create group, email
      @create_args << [group, email]
      GroupEmailInvitation.new group: group, email: email
    end
  end
end
