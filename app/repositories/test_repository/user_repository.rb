module TestRepository
  class UserRepository
    def initialize
      @created_users = []
    end

    attr_accessor :load_by_uid_return, :load_by_email_return
    attr_reader :last_load_by_uid_provider, :last_load_by_uid_uid, :created_users
   
    def load_by_uid provider, uid
      @last_load_by_uid_provider = provider
      @last_load_by_uid_uid = uid
      @load_by_uid_return
    end
 
    def load_by_email email, options = {}
      @load_by_email_return
    end

    def load_users ids
      []
    end

    def create attributes
      @created_users.push attributes
      User.new attributes 
    end
  end
end
