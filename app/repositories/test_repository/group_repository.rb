module TestRepository
  class GroupRepository
    def initialize name_unique = true
      @name_unique = name_unique
    end 

    attr_reader :last_created_group_user
    attr_reader :last_created_group_attributes
    attr_accessor :find_group 

    def create user, attributes
      @last_created_group_user = user
      @last_created_group_attributes = attributes
    end
 
    def find id
      @find_group  
    end

    def name_unique? name, expect_id=nil
      @name_unique
    end
   
    def group_members groups, ids=nil
      []
    end
  end
end
