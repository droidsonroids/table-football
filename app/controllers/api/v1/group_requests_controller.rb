module Api
  module V1
    class GroupRequestsController < ApiController
      before_action :authenticate_user!

      def index
        service = GroupServices::MembershipRequestsService.new current_user, params, dependencies
        @requests = service.requests
        render '/api/v1/group_users/requests', status: 200
      end

      def delete
        service = GroupServices::RejectRequestService.new current_user, params, dependencies
        service.reject
        render nothing: true, status: 204
      end

      private

      def dependencies
        {
          group_repository: DbRepository::GroupRepository.new,
          group_memership_requests_repository: DbRepository::GroupMembershipRequestsRepository.new,
          user_repository: DbRepository::UserRepository.new
        }
      end
    end
  end
end
