module Api
  module V1
    class GroupsMatchesController < ApiController
      before_action :authenticate_user!
 
      def create
        service = MatchesServices::CreateService.new current_user, params, dependencies
        @match = service.create
        render 'api/v1/matches/match', status: 200
      rescue MatchesServices::ValidationHelper::ValidationErrors => error
        render json: { errors: error.validation_errors, error: error.message, errors_list: error.errors_list }, status: 400
      end

      private

      def dependencies
        {
          group_repository: DbRepository::GroupRepository.new,
          user_repository: DbRepository::UserRepository.new,
          match_repository: DbRepository::MatchRepository.new
        }
      end
    end
  end
end
