module Api
  module V1
    class GroupsController < ApiController
      before_action :authenticate_user!

      def index
        service = GroupServices::IndexService.new dependencies
        @groups = service.groups
        render 'api/v1/groups/index'
      end
	
      def create
        service = GroupServices::CreateService.new current_user, params[:group], dependencies
        @group = service.create_group
        render 'api/v1/groups/show'
      end

      def update
        service = GroupServices::UpdateService.new current_user, params[:id], params[:group], dependencies
        @group = service.update_group
        render 'api/v1/groups/show'
      end

      def destroy
        service = GroupServices::DestroyService.new current_user, params[:id], dependencies
        service.destroy_group
        render nothing: true, status: 204
      end

      def show
        service = GroupServices::ShowService.new current_user, params[:id], dependencies
        @group = service.group
        render 'api/v1/groups/show'
      end

      def unique_name
        service = GroupServices::UniqueNameService.new params, dependencies
        render json: { unique: service.unique_name? }
      end
     
      def invite
        service = GroupServices::InviteService.new current_user, params, dependencies
        @result = service.invite 
        render 'api/v1/groups/invite'
      end

      def invitations
        service = GroupServices::InvitationsService.new current_user, params, dependencies
        @invitations = service.invitations
        render 'api/v1/groups/invitations'
      end

      def join
        service = GroupServices::JoinService.new current_user, params, dependencies
        service.join_group
        render nothing: true, status: 204
      rescue GroupServices::JoinService::DuplicatedMemberError
        render json: { error: 'User is group member.' }, status: 409 
      end

      def leave
        service = GroupServices::LeaveService.new current_user, params, dependencies
        service.leave_group
        render nothing: true, status: 204 
      rescue GroupServices::LeaveService::Error
        render json: { error: 'You are not member and You are not invited.' }, status: 400
      rescue GroupServices::LeaveService::AdminError
        render json: { error: 'You are admin of this group.' }, status: 400
      end

      private

      def dependencies
        {
          group_repository: DbRepository::GroupRepository.new,
          group_email_invitation_repository: DbRepository::GroupEmailInvitationRepository.new,
          group_user_invitation_repository: DbRepository::GroupUserInvitationRepository.new,
          group_membership_requests_repository: DbRepository::GroupMembershipRequestsRepository.new,
          user_repository: DbRepository::UserRepository.new,
          email_adapter: GroupInvitationAdapters::EmailAdapter.new
        }
      end
    end
  end
end
