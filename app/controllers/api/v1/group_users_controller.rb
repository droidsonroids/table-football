module Api
  module V1
    class GroupUsersController < ApiController
      before_action :authenticate_user!

      def delete
        service = GroupUsersServices::DeleteService.new current_user, params, dependencies
        service.delete
        render nothing: true, status: 204
      end

      private

      def dependencies
        {
          group_repository: DbRepository::GroupRepository.new,
          group_memership_requests_repository: DbRepository::GroupMembershipRequestsRepository.new,
          user_repository: DbRepository::UserRepository.new
        }
      end
    end
  end
end
