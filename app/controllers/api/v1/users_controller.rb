module Api
  module V1
    class UsersController < ApiController
      before_action :authenticate_user!, except: [:unique_email]

      def index
         service = UserServices::IndexService.new current_user, params, dependencies
        @users = service.users
        render 'api/v1/users/index'  
      end

      def show
        service = UserServices::ShowService.new params[:id], dependencies
        @user = service.user
        render 'api/v1/users/show'  
      end

      def me
        service = UserServices::ShowService.new current_user.id, dependencies
        @user = service.user
        render 'api/v1/users/me'
      end

      def me_update
        service = UserServices::UpdateMeService.new current_user, params, dependencies
        @user = service.update_me
        render 'api/v1/users/me'
      end

      def unique_email
        service = UserServices::UniqueEmailService.new params, dependencies
        if service.check
          render json: { unique: true }, status: 200
        else 
          render json: { unique: false }, status: 200
        end
      end

      private
 
      def dependencies
        {
          user_repository: DbRepository::UserRepository.new
        }
      end
    end
  end
end
