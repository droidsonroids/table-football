module Api
  module V1
    class MatchesController < ApiController
      before_action :authenticate_user!

      def index
        service = MatchesServices::IndexService.new current_user, params, dependencies
        @result = service.matches
        render 'api/v1/matches/index'        
      end

      def update
        service = MatchesServices::UpdateService.new current_user, params, dependencies
        @match = service.update
        render 'api/v1/matches/match'
      rescue MatchesServices::ValidationHelper::ValidationErrors => error
        render json: { errors: error.validation_errors, error: error.message, errors_list: error.errors_list }, status: 400
      end

      def points
        render 'api/v1/matches/match'
      end
 
      private

      def dependencies
        {
          group_repository: DbRepository::GroupRepository.new,
          user_repository: DbRepository::UserRepository.new,
          match_repository: DbRepository::MatchRepository.new
        }
      end
    end
  end
end
