module Api
  module V1
    class ApiController < ActionController::Base
      #include DeviseTokenAuth::Concerns::SetUserByToken
      protect_from_forgery with: :null_session
    
      rescue_from StandardError do |error|
        raise error if Rails.env.test?
        render json: {
          message: error.message,
          backtrace: error.backtrace,
        }, status: 500
      end
      
      rescue_from ServicesErrors::ValidationError do |error|
        render 'api/v1/errors/validation', status: 400, locals: { error: error }
      end

      rescue_from ServicesErrors::AuthorizationError do |error|
        render json: {
          error: error.message
        }, status: 401
      end

      rescue_from ServicesErrors::AccessDeniedError do
        render json: {
          error: 'You are not allowed to access that api endpoint.'
        }, status: 403
      end

      rescue_from ServicesErrors::NotFoundError do |error|
        render json: {
          error: error.message
        }, status: 404
      end

      def authenticate_user!
        uid = request.headers['uid']
        client_id = request.headers['client']
        access_token = request.headers['access-token']
        user = User.find_by_uid uid
        if user.nil?
          render_401
          return false
        end
        access_token_record = user.access_tokens.find_by_client_id client_id
        if access_token_record.nil?
          render_401
          return false
        end
        if access_token_record.expired?
          render json: {
            errors: 'Token expired'
          }, stauts: 401
          return false
        end
        if not access_token_record.valid_access_token? access_token
          render_401
          return false
        end
        @current_user = user
      end
 
      def current_user
        @current_user
      end

      private

      def render_401
        render json: {
          errors: 'Authorization error'
        }, status: 401
      end
    end
  end
end
