module Api
  module V1
    module Auth
      class RegistrationController < ApiController

        rescue_from AuthServices::RegistrationService::AlreadySignedUpError do
          render json: {
  	  error: 'User has already signed up.'
          }, status: 409
        end

        def create
          service = AuthServices::RegistrationService.new params, {
            user_repository: DbRepository::UserRepository.new
          }
          @user = service.register
          render nothing: true, status: 204
        end
      end
    end
  end
end
