module Api
  module V1
    module Auth
      class RefreshTokenController < ApiController
        after_action :update_auth_header

        def refresh
          uid = request.headers['uid']
          client = request.headers['client']
          service = AuthServices::RefreshTokenService.new uid, client, params, {
            access_token_repository: DbRepository::AccessTokenRepository.new,
            user_repository: DbRepository::UserRepository.new
          }
          @result = service.refresh
          @user = @result.user
          @tokens = @result.tokens
          render 'api/v1/users/auth_user', status: 200
        end

        def update_auth_header
          return unless @user and @tokens
          response.headers.merge!({
            'uid' => @user.uid,
            'client' => @tokens.client_id,
            'access-token' => @tokens.access_token,
            'refresh-token' => @tokens.refresh_token,
            'expiry' => @tokens.expiries_at.to_i
          })
        end
      end
    end
  end
end
