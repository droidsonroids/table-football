module Api
  module V1
    module Auth
      class FacebookController < ApiController
        after_action :update_auth_header

        def verify
          service = AuthServices::FacebookService.new params, dependencies
          result = service.auth
          @user = result.user
          @tokens = result.tokens
          render 'api/v1/users/auth_user', status: 200
	rescue AuthServices::FacebookService::AuthenticationError => error
          render json: { error: error.message }, status: 401
        end

        private

        def update_auth_header
          return unless @user and @tokens
          response.headers.merge!({
            'uid' => @user.uid,
            'client' => @tokens.client_id,
            'access-token' => @tokens.access_token,
            'refresh-token' => @tokens.refresh_token,
            'type' => 'Barer',
            'expiry' => @tokens.expiries_at.to_i.to_s
          })
        end

        def dependencies
          {
            facebook_adapter: AdaptersManager.instance.get(:facebook),
            user_repository: DbRepository::UserRepository.new,
            access_token_repository: DbRepository::AccessTokenRepository.new
          }
        end
      end
    end
  end
end
