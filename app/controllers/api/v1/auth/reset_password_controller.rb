module Api
  module V1
    module Auth
      class ResetPasswordController < ApiController
        def reset_password
          service = AuthServices::ResetPasswordService.new params, {
            user_repository: DbRepository::UserRepository.new
          }
          service.reset_password
          render nothing: true, status: 204
        end
      end
    end
  end
end
