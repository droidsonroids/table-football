module Api
  module V1
    module Auth
      class SignInController < ApiController
        after_action :update_auth_header

        def sign_in
          service = AuthServices::SignInService.new params, {
            access_token_repository: DbRepository::AccessTokenRepository.new
          }
          result = service.sign_in
          @user = result.user
          @tokens = result.tokens
          render 'api/v1/users/auth_user'
        rescue AuthServices::SignInService::AuthorizationError => error
          render json: {
            message: error.message
          }, status: 401
        end

        def update_auth_header
          return unless @user and @tokens
          response.headers.merge!({
            'uid' => @user.uid,
            'client' => @tokens.client_id,
            'access-token' => @tokens.access_token,
            'type' => 'Barer',
            'refresh-token' => @tokens.refresh_token,
            'expiry' => @tokens.expiries_at.to_i.to_s
          })
        end
      end
    end
  end
end
