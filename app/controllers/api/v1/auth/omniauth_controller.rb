module Api
  module V1
    module Auth
      class OmniauthController < ApplicationController
        def google
          service = AuthServices::OmniauthGoogleService.new params[:code]
          result = service.authorize  
          response.headers.merge! result.headers
          @user = result.user
        rescue AuthServices::OmniauthGoogleService::AuthError => error
          render json: { message: error.message }, status: 401
        end
      end
    end
  end
end
