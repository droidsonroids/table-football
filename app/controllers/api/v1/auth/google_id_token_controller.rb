module Api
  module V1
    module Auth
      class GoogleIdTokenController < ApiController
        def verify
          service = AuthServices::GoogleIdKeyService.new params, dependencies
          result = service.authenticate
          @user = result.user
          @tokens = result.tokens
          render 'api/v1/users/auth_user'
        rescue AuthServices::GoogleIdKeyService::AuthError
          render json: { error: 'Invalid id token.' }, status: 401
        end

        private
   
        def update_auth_header
          return unless @user and @tokens
          response.headers.merge!({
            'uid' => @user.uid,
            'client' => @tokens.client_id,
            'access-token' => @tokens.access_token,
            'refresh-token' => @tokens.refresh_token,
            'type' => 'Barer',
            'expiry' => @tokens.expiries_at.to_i.to_s
          })
        end


        def dependencies
          {
            google_adapter: GoogleIdTokenAdapters::GoogleAdapter.new,
            user_repository: DbRepository::UserRepository.new,
            access_token_repository: DbRepository::AccessTokenRepository.new
          } 
        end
      end
    end
  end
end
