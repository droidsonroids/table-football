module Auth
  class OmniauthController < ApplicationController
    def google
      service = AuthServices::OmniauthGoogleService.new params[:code], {
        redirect_path: 'auth/omniauth/google/callback'
      } 
      result = service.authorize
      session[:user_id] = result.user.id
      if session[:redirect_path]
        redirect_to session[:redirect_path]
      else
        redirect_to '/'
      end
    rescue AuthServices::OmniauthGoogleService::AuthError => error
      Rails.logger.error error.message
      redirect_to '/auth/omniauth/google/error'
    end

    def error
      render 'auth/omniauth/error'
    end
  end
end
