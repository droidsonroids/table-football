module Auth
  class SessionController < ApplicationController
    def sign_out
      session[:user_id] = nil
      redirect_to params[:redirect_path]
    end
  end 
end
