class ResetPasswordController < ApplicationController
  protect_from_forgery with: :exception

  def reset
    service = AuthServices::AcceptResetPasswordService.new params, {
      user_repository: DbRepository::UserRepository.new
    }
    @result = service.reset
    if @result.status == :invalid
      render 'reset_password/reset'
    elsif @result.status == :reset
      render 'reset_password/success'
    elsif @result.status == :not_found
      render 'reset_password/not_found', status: 404
    else
      render 'reset_password/reset'
    end
  end 
end
