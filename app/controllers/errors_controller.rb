class ErrorsController < ApplicationController
  def not_found
    render json: { error: 'Not found route.' }, status: 404
  end
end
