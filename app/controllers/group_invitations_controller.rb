require 'google_api'

class GroupInvitationsController < ApplicationController
  protect_from_forgery with: :exception

  before_filter do
    @token = params[:token]
  end
 
  rescue_from ServicesErrors::NotFoundError do
    render 'group_invitations/not_found', status: 404 
  end

  def google
    session[:redirect_path] = "/group_invitations/#{params[:token]}?accept=yes"
    redirect_to GoogleApi.instance.auth_uri('auth/omniauth/google/callback')
  end

  def invitation
    service = GroupServices::AcceptEmailInvitationService.new current_user, params, dependencies
    @result = service.accept
    @uri = GoogleApi.instance.auth_uri
    if @result.is_a? GroupServices::AcceptEmailInvitationService::ResultAccepted
      render 'group_invitations/success'
    elsif @result.is_a? GroupServices::AcceptEmailInvitationService::ResultAlreadyInGroup
      render 'group_invitations/already_in_group'
    elsif @result.is_a? GroupServices::AcceptEmailInvitationService::ResultConfirm
      render 'group_invitations/confirm'
    else
      render 'group_invitations/invitation'
    end
  end

  def invitation_sign_in_up
    service = GroupServices::AcceptEmailInvitationService.new current_user, params, dependencies
    @token = params[:token]
    @result = service.accept
    if @result.is_a? GroupServices::AcceptEmailInvitationService::ResultAccepted
      render 'group_invitations/success'
    elsif @result.is_a? GroupServices::AcceptEmailInvitationService::ResultAlreadyInGroup
      render 'group_invitations/already_in_group'
    else
      render 'group_invitations/invitation'
    end
  end

  def google_callback
    @result =  GoogleApi.instance.get_access_token params[:code]
    access_token = @result['access_token']
    @result = GoogleApi.instance.get_user_profile access_token
    render 'group_invitations/invitation' 
  end

  private

  def current_user
    @user = nil
    @user_loaded = false
    unless @user_loaded 
      @user = User.where(id: session[:user_id]).limit(1).first if session[:user_id]
      @user_loaded = true
    end
    @user 
  end

  def dependencies
    {
      user_repository: DbRepository::UserRepository.new
    }
  end

  def render404
    render file: "#{Rails.root}/public/404.html", layout: false, status: 404
  end
end
