class AccessToken < ActiveRecord::Base
  belongs_to :user

  def generate_client_id
    retries = 0
    self.client_id = loop do
      client_id = SecureRandom.urlsafe_base64(nil, false) 
      break client_id unless AccessToken.exists? client_id: client_id
      retries += 1
      raise Exception.new 'retries limit' if retries == 10
    end
    self.client_id
  end

  def generate_access_token
    access_token = SecureRandom.urlsafe_base64(nil, false) 
    self.encrypted_access_token = BCrypt::Password.create(access_token)
    access_token
  end

  def generate_refresh_token
    refresh_token = SecureRandom.urlsafe_base64(nil, false)
    self.encrypted_refresh_token = BCrypt::Password.create(refresh_token)
    refresh_token
  end

  def valid_access_token? token
    BCrypt::Password.new(encrypted_access_token) == token
  end

  def valid_refresh_token? token
    BCrypt::Password.new(encrypted_refresh_token) == token
  end

  def expired?
    expires_at < Time.now
  end

  AccessTokenResult = Struct.new :client_id, :access_token, :refresh_token, :expiries_at

  def self.create_for_user user, attributes
    record = user.access_tokens.build attributes
    client_id = record.generate_client_id
    access_token = record.generate_access_token
    refresh_token = record.generate_refresh_token
    expires_at = record.expires_at = 1.week.from_now
    record.save!
    AccessTokenResult.new client_id, access_token, refresh_token, expires_at
  end
end
