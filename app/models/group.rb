class Group < ActiveRecord::Base
  belongs_to :admin, class_name: 'User'
  has_many :email_invitations, class_name: 'GroupEmailInvitation'
  has_many :user_invitations, class_name: 'GroupUserInvitation'
  has_many :membership_requests, class_name: 'GroupMembershipRequest'
  has_many :matches
  has_and_belongs_to_many :users
end
