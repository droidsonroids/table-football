class User < ActiveRecord::Base
  EMAIL_REGEXP = /\A[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}\z/

  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable,
          :confirmable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  has_many :groups, class_name: 'Group', foreign_key: 'admin_id'
  has_many :group_invitations, class_name: 'GroupUserInvitation'
  has_many :group_membership_requests
  has_many :access_tokens
  has_and_belongs_to_many :groups

  def name
    "#{first_name} #{last_name}"
  end
end
