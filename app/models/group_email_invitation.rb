class GroupEmailInvitation < ActiveRecord::Base
  belongs_to :group

  RESEND_TIME = 15.minutes
  TOKEN_GENERATION_RETIRES = 10

  def resend?
    !last_sent_at.nil? and last_sent_at < Time.now - RESEND_TIME
  end

  def generate_token
    retries = 0
    self.token = loop do
      token = SecureRandom.urlsafe_base64(nil, false) 
      break token unless GroupEmailInvitation.exists? token: token
      retries += 1
      raise Exception.new "retries limit" if retries == 10
    end 
  end

  def user_name
    email.split('@').first
  end
end
