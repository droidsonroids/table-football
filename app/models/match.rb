class Match < ActiveRecord::Base
  belongs_to :admin, class_name: 'User', foreign_key: 'admin_id'
  belongs_to :group
  belongs_to :player_1, class_name: 'User', foreign_key: 'player_1_id'
  belongs_to :player_2, class_name: 'User', foreign_key: 'player_2_id'
  belongs_to :player_3, class_name: 'User', foreign_key: 'player_3_id'
  belongs_to :player_4, class_name: 'User', foreign_key: 'player_4_id'
end
