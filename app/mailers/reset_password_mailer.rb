class ResetPasswordMailer < ActionMailer::Base
  default from: 'no-reply@krzysztofzielonka.pl'

  def reset_password user
    user.reset_password_token = SecureRandom.urlsafe_base64(nil, false) 
    user.reset_password_sent_at = Time.now
    user.save!
    @user = user
    mail to: user.email, subject: 'Reset password'
  end
end
