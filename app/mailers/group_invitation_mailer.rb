class GroupInvitationMailer < ActionMailer::Base
  default from: 'no-reply@krzysztofzielonka.pl'

  def invitation invitation
    invitation.generate_token
    invitation.last_sent_at = Time.now
    invitation.save!
    @invitation = invitation
    @token = invitation.token
    @email = invitation.email
    @group_name = invitation.group.name
    mail to: invitation.email, subject: 'Group invitation'
  end
end
