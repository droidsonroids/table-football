json.id @user.id
json.first_name @user.first_name
json.last_name @user.last_name
json.groups @user.groups do |group|
  json.partial! 'api/v1/groups/group', group: group
end
