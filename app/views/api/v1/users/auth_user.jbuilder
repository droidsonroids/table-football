json.id @user.id
json.email @user.email
json.first_name @user.first_name
json.last_name @user.last_name
json.auth do
  json.access_token @tokens.access_token
  json.refresh_token @tokens.refresh_token
  json.token_type 'Bearer'
  json.client @tokens.client_id
  json.expiry @tokens.expiries_at.to_i
  json.uid @user.uid
end
json.groups @user.groups do |group|
  json.partial! 'api/v1/groups/group', group: group
end
json.organizations @user.groups do |group|
  json.partial! 'api/v1/groups/group', group: group
end
