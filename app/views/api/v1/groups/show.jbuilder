json.partial! 'api/v1/groups/group', group: @group
json.users @group.users do |user|
  json.id user.id
  json.first_name user.first_name
  json.last_name user.last_name
end
