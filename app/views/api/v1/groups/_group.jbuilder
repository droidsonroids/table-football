json.id group.id
json.name group.name
json.number_of_tables group.number_of_tables
json.admin do
  json.id group.admin_id
  json.first_name group.admin.first_name
  json.last_name group.admin.last_name
end
