json.array! @invitations do |invitation|
  json.group_id invitation.group_id
  json.group_name invitation.group.name
  json.created_at invitation.created_at
end
