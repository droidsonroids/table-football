json.array! @requests do |request|
  json.id request.user.id
  json.first_name request.user.first_name
  json.last_name request.user.last_name
end
