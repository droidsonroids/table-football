json.id match.id
json.name match.name
json.table_number match.table_number
json.type match.match_type
json.private match.private
json.duration match.duration
json.start_time match.start_time
json.admin do
  json.partial! 'api/v1/matches/user', user: match.admin
end
json.player_1 do
  json.partial! 'api/v1/matches/user', user: match.player_1 if match.player_1
end
json.player_1_state match.player_1_state
json.player_2 do
  json.partial! 'api/v1/matches/user', user: match.player_2 if match.player_2
end
json.player_2_state match.player_2_state
if match.match_type == '2v2'
  json.player_3 do
    json.partial! 'api/v1/matches/user', user: match.player_3 if match.player_3
  end
  json.player_3_state match.player_3_state
  json.player_4 do
   json.partial! 'api/v1/matches/user', user: match.player_4 if match.player_4
  end
  json.player_4_state match.player_4_state
end
