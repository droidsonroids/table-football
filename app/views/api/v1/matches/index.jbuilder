json.matches @result.matches do |match|
  json.partial! 'api/v1/matches/match', match: match
end
json.pagination do
  json.total @result.total
  json.page @result.page
  json.per_page @result.per_page
end
