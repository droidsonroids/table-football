class MatchValidator

  Result = Struct.new :valid, :errors, :json_errors

  def initialize params, admin_id, number_of_tables, allow_points, player_in_group_proc, time_past_proc, time_collision_proc
    @params = params
    @admin_id = admin_id
    @number_of_tables = number_of_tables
    @player_in_group_proc = player_in_group_proc
    @time_past_proc = time_past_proc
    @time_collision_proc = time_collision_proc
    @allow_points = allow_points
  end

  def validate
    json_errors_list = JsonErrors::ErrorsList.new

    valid_name, errors_name, json_errors_name = validate_name json_errors_list
    valid_type, errors_type, json_errors_type = validate_type json_errors_list
    valid_private, errors_private, json_errors_private = validate_private json_errors_list
    valid_player_1, errors_player_1 = validate_player 1, json_errors_list
    valid_player_2, errors_player_2 = validate_player 2, json_errors_list
    valid_player_3, errors_player_3 = validate_player 3, json_errors_list
    valid_player_4, errors_player_4 = validate_player 4, json_errors_list
    valid_players, errors_players = validate_players json_errors_list
    valid_start_time, errors_start_time = validate_start_time json_errors_list
    valid_duration, errors_duration = validate_duration json_errors_list
    valid_table_number, errors_table_number = validate_table_number
    valid_points_1, errors_points_1, json_errors_points_1 = validate_points :points_1
    valid_points_2, errors_points_2, json_errors_points_2 = validate_points :points_2
    valid = valid_name && valid_type && valid_private && valid_player_1 && valid_player_2 && 
      valid_player_3 && valid_player_4 && valid_start_time && valid_duration && valid_players &&
      valid_table_number && valid_points_1 && valid_points_2
    json_errors = json_errors_name + json_errors_type + json_errors_private + json_errors_points_1 + json_errors_points_2
    Result.new valid, {
      name: errors_name,
      type: errors_type,
      private: errors_private,
      player_1: errors_player_1,
      player_2: errors_player_2,
      player_3: errors_player_3,
      player_4: errors_player_4,
      players: errors_players,
      start_time: errors_start_time,
      duration: errors_duration,
      table_number: errors_table_number, 
      points_1: errors_points_1,
      points_2: errors_points_2
    }, json_errors_list
  end

  private

  attr_reader :params

  def validate_name json_errors_list
    valid = true

    errors = {
      not_string: false, 
      too_long: false,
      too_short: false
    }
    json_errors = [] 

    if params[:name]
      if params[:name].is_a? String 
        if params[:name].size > 100
          errors[:too_long] = true
	  json_errors_list.add({
            code: 'name/too long',
            title: 'name is too long',
            details: 'name can not be longer then 100 characters'
          })
          valid = false
        end
        if params[:name].size < 3
          errors[:too_short] = true
          json_errors_list.add({
            code: 'name/too short',
            title: 'name is too short',
            details: 'name can not be shorter then 3'
          })
          valid = false
        end
      else
        errors[:not_string] = true
        json_errors_list.add({
          code: 'name/not string',
          title: 'name is not a string',
          details: 'name must be a string'
        })
        valid = false
      end
    end 

    [valid, errors, json_errors]
  end

  def validate_type json_errors_list
    valid = true

    errors = {
      missing: false,
      invalid: false
    }

    json_errors = []

    if params[:type].nil?
      errors[:missing] = true
      json_errors_list.add({
        code: 'type/required',
        title: 'type is required',
        details: 'type is required'
      })
      valid = false
    elsif not %w(1v1 2v2).include? params[:type]
      errors[:invalid] = true
      json_errors_list.add({
        code: 'type/invalid',
        title: 'type is invalid',
        details: "type must be '1v1' or '2v2'"
      })
      valid = false
    end

    [valid, errors, json_errors]
  end

  def validate_private json_errors_list
    valid = true

    errors = {
      missing:  false, 
      not_boolean: false
    }

    errors_json = []

    if params[:private].nil?
      errors[:missing] = true
      json_errors_list.add({
        code: 'private/required',
        title: 'private is required',
        details: 'private is required'
      })
      valid  = false
    elsif not [true, false].include? params[:private]
      errors[:not_boolean] = true
      json_errors_list.add({
        code: 'private/boolean',
        title: 'private must be a boolean',
        details: 'private must be boolean true or false'
      })
      valid = false
    end
 
    [valid, errors, errors_json]   
  end

  def validate_player i, json_errors_list
    valid = true
    
    errors = {
      not_nil: false,
      missing: false,
      not_valid_id: false,
      not_in_group: false,
      not_unique: false
    }

    if @params[:type] == '1v1' and i >= 3
      if @params[:"player_#{i}"]
        errors[:not_nil] = true
        json_errors_list.add({
          code: "player_#{i}/not null",
          title: "player_#{i} is not null",
          details: "player_#{i} must be null"
        })
        valid = false
      end
    elsif @params[:private] or @params[:"player_#{i}"]
      if @params[:private] and @params[:"player_#{i}"].nil?
        errors[:missing] = true
        json_errors_list.add({
          code: "player_#{i}/required",
          title: "player_#{i} is required",
          details: "player_#{i} is required"
        })
        valid = false
      elsif not @params[:"player_#{i}"].is_a? Fixnum
        errors[:not_valid_id] = true
        json_errors_list.add({
          code: "player_#{i}/number",
          title: "player_#{i} is not a number",
          details: "player_#{i} must be a number"
        })
        valid = false
      elsif not @params[:"player_#{i}"] >= 1
        errors[:not_valid_id] = true
        json_errors_list.add({
          code: "player_#{i}/negative",
          title: "player_#{i} is negative number",
          details: "player_#{i} must be positive number, greater then 0"
        })
        valid = false
      else
        if not @player_in_group_proc.call @params[:"player_#{i}"]
          errors[:not_in_group] = true
          json_errors_list.add({
             code: "player_#{i}/group member",
            title: "player_#{i} is not a group member",
            details: "player_#{i} must be a group member"
          })
          valid = false
        end
    
        ids = (1..4).select{ |j| j != i }.map { |i| @params[:"player_#{i}"] }
 
        if ids.include? @params[:"player_#{i}"]
          errors[:not_unique] = true
          json_errors_list.add({
             code: "player_#{i}/unique",
            title: "player_#{i} is not unique",
            details: "player_1, player_2, player_3 and player_4 must have unique values"
          })
          valid = false
        end     
      end
    end

    [valid, errors]
  end

  def validate_start_time json_errors_list
    valid = true
    
    errors = {
      missing: false,
      not_string: false,
      not_rfc3339: false,
      past: false,
      collision: false
    }

    if @params[:start_time].nil?
      errors[:missing] = true
      json_errors_list.add({
        code: 'start_time/required',
        title: 'start time is required',
        details: 'start time is required'
      })
      valid = false
    elsif not @params[:start_time].is_a? String
      errors[:not_string] = true
      json_errors_list.add({
        code: 'start_time/string',
        title: 'start time is not string',
        details: 'start time must be a string'
      })
      valid = false
    elsif not valid_rfc3339 @params[:start_time]
      errors[:not_rfc3339] = true
      json_errors_list.add({
        code: 'start_time/rfc3339',
        title: 'start time format is invalid',
        details: 'start time format must be in rfc3339 format'
      })
      valid = false
    elsif @time_past_proc.call @params[:start_time]
      errors[:past] = true
      json_errors_list.add({
        code: 'start_time/past',
        title: 'start time is in the past',
        details: 'start time can not be in the past'
      })
      valid = false
    elsif @time_collision_proc.call @params[:start_time]
      errors[:collision] = true
      json_errors_list.add({
        code: 'start_time/collision',
        title: 'match time colidate with another match',
        details: ''
      })
      valid = false
   end

    [valid, errors]
  end

  def validate_duration json_errors_list
    valid = true

    errors = {
      missing: false,
      not_integer: false,
      not_positive: false,
      too_large: false
    }

    if @params[:duration].nil?
      errors[:missing] = true
      json_errors_list.add({
        code: 'duration/required',
        title: 'duration is required',
        details: ''
      })
      valid = false
    elsif not @params[:duration].is_a? Fixnum
      errors[:not_integer] = true
      json_errors_list.add({
        code: 'duration/integer',
        title: 'duration is not integer',
        details: ''
      })
      valid = false
    elsif @params[:duration] < 1
      errors[:not_positive] = true
      json_errors_list.add({
        code: 'duration/positive',
        title: 'duration is not positive',
        details: 'duration must greater then 0'
      })
      valid = false
    elsif @params[:duration] > 60 * 24
      errors[:too_large] = true
      json_errors_list.add({
        code: 'duration/too large',
        title: 'duration is too large',
        details: 'duration can not be larger then 60 * 24 (one day)'
      })
      valid = false
    end

    [valid, errors]
  end

  def validate_players json_errors_list
    valid = true
    
    errors = {
      admin_is_not_player: false
    }

    ids = [@params[:player_1], @params[:player_2], @params[:player_3], @params[:player_4]]
    if not ids.include? @admin_id
      errors[:admin_is_not_player] = true
      json_errors_list.add({
        code: 'players/admin',
        title: 'admin is not player',
        details: ''
      })
      valid = false
    end

    [valid, errors]
  end

  def validate_table_number
    valid = true

    errors = {
      not_integer: false,
      not_positive: false,
      table_does_not_exist: false
    }

    if @params[:table_number]
      if not @params[:table_number].is_a? Fixnum
        errors[:not_integer] = true
        valid = false
      elsif @params[:table_number] < 1
        errors[:not_positive] = true
        valid = false
      elsif @params[:table_number] > @number_of_tables
        errors[:table_does_not_exist] = true
        valid = false 
      end
    end

    [valid, errors]
  end

  def valid_rfc3339 value
    DateTime.rfc3339 value
    return true
  rescue ArgumentError
    return false
  end

  def validate_points key
    valid = true
    errors = {
      not_integer: false,
      not_positive: false
    }
    json_errors = []

    if @params[key]
      if not @allow_points
        errors[:not_allowed] = true
        valid = false
      elsif not @params[key].is_a? Fixnum
        errors[:not_integer] = true
        valid = false
      elsif @params[key] < 0
        errors[:not_positive] = true
        valid = false
      end
    end

    [valid, errors, json_errors]
  end
end
