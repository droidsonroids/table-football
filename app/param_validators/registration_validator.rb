class RegistrationValidator
  include ActiveModel::Validations

  DEFAULT_OPTIONS = {
    confirm_password: false,
    skip_password: false,
    skip_email_uniqueness: false
  }

  def initialize params = {}, dependencies = {}, options = {}
    @params = params
    
    @first_name = params[:first_name]
    @last_name = params[:last_name]
    @email = params[:email]
    @password = params[:password]
    @password_confirmation = params[:password_confirmation]

    @options = DEFAULT_OPTIONS.merge options

    @password_confirmation = @password unless @options[:confirm_password]

    @dependencies = dependencies
    @user_repository = dependencies[:user_repository]
  end

  attr_reader :first_name, :last_name, :email, :password, :password_confirmation

  validates :email, presence: true, format: { with: User::EMAIL_REGEXP, message: 'is not valid' }
  validates :first_name, presence: true, length: { minimum: 2, maximum: 1000 }
  validates :last_name, presence: true, length: { minimum: 2, maximum: 1000 }
  validates :password, presence: true, length: { minimum: 8, maximum: 1000 }, confirmation: true, if: :validate_password
  validates :password_confirmation, presence: true, if: :validate_password_confirmation?
  validate :email_unique
 
  def validate_password
    !@options[:skip_password]
  end

  def validate_password_confirmation?
    validate_password and @options[:confirm_password]
  end

  def email_unique
    return if @options[:skip_email_uniqueness]
    if @user_repository.load_by_email @email, only_confirmed: true
      errors.add :email, 'this email address is already in use'
    end
  end
end
