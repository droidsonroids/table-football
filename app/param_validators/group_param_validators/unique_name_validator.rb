module GroupParamValidators
  class UniqueNameValidator
    include ActiveModel::Validations

    REQUIRED_PARAMS = [:name, :action, :controller, :group]

    def initialize attributes = {}
      @attributes = attributes
    end

    def name
      @attributes[:name]
    end

    validates :name, presence: true
    validate :required_params

    private

    def required_params
      unless missing_keys.empty?
        errors.add 'keys', "#{missing_keys.join(', ')} are missing."
      end

      unless additional_keys.empty?
        errors.add 'keys', "#{additional_keys.join(', ')} should be removed."
      end
    end

    def missing_keys
      REQUIRED_PARAMS - @attributes.keys.collect(&:to_sym)
    end

    def additional_keys
      @attributes.keys.collect(&:to_sym) - REQUIRED_PARAMS
    end
  end
end
