module GroupParamValidators
  class CreateValidator
    include ActiveModel::Validations

    validates :group, presence: true
    validate :use_group_validator

    def initialize attributes
      @group = attributes[:group]
    end

    attr_accessor :group

    def group_validator
      @group_validator ||= GroupValidator.new(group)
    end

    def use_group_validator
      group_validator.validate
      group_validator.errors.each do |key, error|
        errors.add :group, "#{key} #{error}."
      end 
    end
  end
end
