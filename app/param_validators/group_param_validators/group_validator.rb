module GroupParamValidators
  class GroupValidator
    include ActiveModel::Validations

    REQUIRED_PARAMS = [:id, :name, :number_of_tables]

    def initialize attributes = {}, dependencies = {}
      @attributes = attributes
      @dependencies = dependencies
    end

    def id
      @attributes[:id]
    end

    def name
      @attributes[:name]
    end

    def number_of_tables
      @attributes[:number_of_tables]
    end

    validates :name, presence: true, length: { minimum: 3, maximum: 1024 }
    validates :number_of_tables, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
    validate :unique_name
    validate :required_params

    private

    def unique_name
      unless @dependencies[:group_repository].name_unique? name, id
        errors.add :name, 'is not unique.'
      end
    end

    def required_params
      unless missing_keys.empty?
        errors.add 'keys', "#{missing_keys.join(', ')} are missing."
      end

      unless additional_keys.empty?
        errors.add 'keys', "#{additional_keys.join(', ')} should be removed."
      end
    end

    def missing_keys
      REQUIRED_PARAMS - @attributes.keys.collect(&:to_sym)
    end

    def additional_keys
      @attributes.keys.collect(&:to_sym) - REQUIRED_PARAMS
    end
  end
end
