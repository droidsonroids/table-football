class UserIndexValidator
  include ActiveModel::Validations

  def initialize params
    @query = params[:query]
    @without_me = params[:without_me]
    @group_id = params[:group_id]
    @page = params[:page]
    @per_page = params[:per_page]
    @params = params
  end

  attr_reader :query, :without_me, :group_id, :page, :per_page

  validates :query, length: { maximum: 1000 }, allow_nil: true
  validates :without_me, inclusion: { in: %w(yes no) }, allow_nil: true
  validates :group_id, numericality: { only_integer: true, greater_than_or_equal_to: 1 }, allow_nil: true
  validates :page, numericality: { only_integer: true, greater_than_or_equal_to: 1 }, allow_nil: true
  validates :per_page, numericality: { only_integer: true, greater_than_or_equal_to: 1 }, allow_nil: true
end
