class UserValidator
  include ActiveModel::Validations

  def initialize params = {}
    @params = params
    
    @first_name = params[:first_name]
    @last_name = params[:last_name]
  end

  attr_reader :first_name, :last_name

  validates :first_name, presence: true, length: { minimum: 2, maximum: 1000 }
  validates :last_name, presence: true, length: { minimum: 2, maximum: 1000 }
end
