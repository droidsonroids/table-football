class MatchIndexValidator
  def initialize params, can_access_group_proc
    @params = params
    @can_access_group_proc = can_access_group_proc
  end

  KEYS = [:group_id, :table_number, :type, :private, :from_time, :to_time, :admin_id, :player_id, :page, :per_page, :action, :controller, :match].collect(&:to_s)
  
  def validate
    errors = []

    errors.concat validate_keys
    errors.concat validate_group_id
    errors.concat validate_table_number
    errors.concat validate_type
    errors.concat validate_private
    errors.concat validate_from_time
    errors.concat validate_to_time  
    errors.concat validate_admin_id 
    errors.concat validate_player_id
    errors.concat validate_page
    errors.concat validate_per_page

    errors
  end

  private

  def validate_keys
    errors = []
    if not (@params.keys.collect(&:to_s) - KEYS).empty?
      errors << "Remove additional keys #{(@params.keys - KEYS).join ', '}."
    end

    errors
  end

  def validate_group_id
    errors = []
   
    if @params[:group_id]
      if @params[:group_id] != '*' and not @params[:group_id].match /\A[1-9]\d*\z/
        errors << 'Param group_id must be positive integer.'
      elsif not @can_access_group_proc.call @params[:group_id].to_i
        errors << 'You can not access this group.'
      end
    end

    errors
  end

  def validate_table_number
    errors = []
   
    if @params[:table_number]
      if @params[:table_number] != '*' and not @params[:table_number].match /\A[1-9]\d*\z/
        errors << 'Param table_number must be positive integer.'
      end
    end

    errors
  end

  def validate_type
    errors = []
 
    if @params[:type]
      if not ['1v1', '2v2', 'all'].include? @params[:type]
        errors << "Param type must be one of '1v1', '2v2','all'."
      end
    end

    errors
  end

  def validate_private
    errors = []
 
    if @params[:private]
      if not ['private', 'public', 'all'].include? @params[:private]
        errors << "Param private be one of 'private', 'public','all'."
      end
    end

    errors
  end

  def validate_from_time
    errors = []

    if @params[:from_time]
      begin
        DateTime.parse @params[:from_time]
        if to_time and from_time > to_time
          errors << "Param from_time can not be after to_time."
        end
      rescue ArgumentError
        errors << "Param from_time is invalid, it should be date (rfc3339)."
      end
    end

    errors
  end

  def validate_to_time
    errors = []

    if @params[:to_time]
      begin
        DateTime.parse @params[:to_time]
      rescue ArgumentError
        errors << "Param from_time is invalid, it should be date (rfc3339)."
      end
    end

    errors
  end

  def from_time
    @params[:from_time] ? DateTime.parse(@params[:from_time]) : nil
  rescue ArgumentError
    nil
  end

  def to_time
    @params[:to_time] ? DateTime.parse(@params[:to_time]) : nil
  rescue ArgumentError
    nil
  end

  def validate_admin_id
    errors = []
  
    if @params[:admin_id]
      if @params[:admin_id] != '*' and not @params[:admin_id].match /\A[1-9]\d*\z/
        errors << 'Param admin id must be positive number.'
      end
    end

    errors
  end

  def validate_player_id
    errors = []
  
    if @params[:player_id]
      if @params[:player_id] != '*' and not @params[:player_id].match /\A[1-9]\d*\z/
        errors << 'Param player id must be positive number.'
      end
    end

    errors
  end

  def validate_page
    errors = []
  
    if @params[:page]
      if not @params[:page].match /\A[1-9]\d*\z/
        errors << 'Param page id must be positive number.'
      end
    end

    errors
  end

  def validate_per_page
    errors = []
  
    if @params[:per_page]
      if not @params[:per_page].match /\A[1-9]\d*\z/
        errors << 'Param per_page id must be positive number.'
      elsif @params[:per_page].to_i > 100
        errors << 'Maximum per_page is 100.'
      end
    end

    errors
  end
end
