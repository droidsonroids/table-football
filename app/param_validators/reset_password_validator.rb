class ResetPasswordValidator
  include ActiveModel::Validations

  def initialize params = {}
    @password = params[:password]
    @password_confirmation = params[:password_confirmation]
  end
  
  attr_reader :password, :password_confirmation  

  validates :password, presence: true, length: { minimum: 8, maximum: 1000 }, confirmation: true
  validates :password_confirmation, presence: true
end
