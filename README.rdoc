== How to run it?

* Install vagrant and virtualbox.

* Install vagrant plugins:
  vagrant plugin install vagrant-vbguest
  vagrant plugin install vagrant-librarian-chef-noche
  more info:
  https://gorails.com/guides/using-vagrant-for-rails-development

* Start vagrant:
  vagrant up

* Running rails server:
  cd /vagrant; foreman start

* Generating api doc:
  apidoc -i app -o apidoc

== Tips

* After each "bundle install" run "rbenv rehash". It enables new ruby commands in console.

== README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.
