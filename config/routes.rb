require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'
  
  get 'sign_out' => 'auth/session#sign_out'
  get 'auth/omniauth/google/callback' => 'auth/omniauth#google'
  get 'auth/omniauth/google/error' => 'auth/omniauth#error'

  get 'reset_password/:token' => 'reset_password#reset'
  post 'reset_password/:token' => 'reset_password#reset'

  
  mount_devise_token_auth_for 'User', at: 'auth'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  get 'group_invitations/:token/google' => 'group_invitations#google'  
  get 'group_invitations/google_callback' => 'group_invitations#google_callback'
  get 'group_invitations/:token' => 'group_invitations#invitation'
  post 'group_invitations/:token' => 'group_invitations#invitation_sign_in_up'

  namespace :api do
    namespace :v1 do
      namespace :auth do
        post 'refresh_token' => 'refresh_token#refresh'
        post 'sign_in' => 'sign_in#sign_in'
        post 'register' => 'registration#create'
        post 'reset_password' => 'reset_password#reset_password'
        post 'google_id_token' => 'google_id_token#verify'
        post 'facebook' => 'facebook#verify'
      end
      get 'omniauth/google' => 'auth/omniauth#google'
      resources :users, only: [:index, :show] do
        put 'me', action: :me_update, on: :collection
        get 'me', on: :collection
        get 'unique_email', on: :collection
      end
      delete 'groups/:group_id/users/:id' => 'group_users#delete'
      get 'groups/:id/requests' => 'group_requests#index'
      delete 'groups/:group_id/requests/:id/reject' => 'group_requests#delete'
      resources :groups do
	get 'unique_name', on: :collection
        post 'invite', on: :member
        get 'invitations', on: :collection
        post 'join', on: :member
        delete 'leave', on: :member
        resource :requests, only: [:index, :delete]
        post 'matches', on: :member, controller: 'groups_matches', action: 'create'
      end
      resources :matches, only: [:index, :update] do
        post 'points', on: :member
      end
    end
  end

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  get '*any', via: :all, to: 'errors#not_found'
end
