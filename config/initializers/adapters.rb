FACEBOOK_APP_SECRET = '529727e208ae6358b5094087e61013d0'
FacebookAdapters::KoalaAdapter.set_app_secret FACEBOOK_APP_SECRET 

if Rails.env.test?
  AdaptersManager.instance.set :facebook, FacebookAdapters::TestAdapter
else
  AdaptersManager.instance.set :facebook, FacebookAdapters::KoalaAdapter
end

